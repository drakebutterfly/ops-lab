运维知识体系

（1）操作系统和Linux、计算、存储、网络

（2）虚拟化：KVM/XEN、CPU虚拟化、内存虚拟化、网络虚拟化、I/O虚拟化

（3）SDN软件定义网络：VXLAN、SDN控制器、OpenFlow、虚拟网卡、虚机交换机

（4）云计算：IaaS PaaS SaaS

（5）容器化：namespace隔离、cgroups限制、rootfs文件系统

（6）云原生：容器、微服务、服务网格、不可变基础设施、声明式API

（7）DevOps：shell自动化、python自动化、ansible自动化



网络

TCP/IP

虚拟网络

容器网络

SDN



虚拟网络4种模型：

- 桥接(Bridge Adapter)：虚机之间二层网络互通，虚拟交换机桥接物理网卡与外部网络打通
- NAT：NAT（虚机之间三层隔离，通过NAT与虚拟交换机和物理网卡共二层）和NAT网络（虚机之间二层打通，虚拟交换机与物理网卡通过NAT打通）
- 主机(Host-only Adapter)：虚机之间共二层私有网络，通过虚拟网卡和物理网卡共享与外部网络打通
- 内部网络：仅虚机之间二层互通

![virtual_net](.\virtual_net.png)



Linux虚拟网络设备：TAP/TUN/VETH/Bridge

OVS：一个成熟的开源分布式虚机交换机



Linux性能分析：

（1）CPU：lscpu、 /proc/cpuinfo、top、ps、vmstat

（2）内存：lsmem、/proc/meminfo、free -h、top、ps、vmstat

（3）IO：df -h、iostat -d
