<h1 align="center">Docker基础学习</h1>

# 1 Docker 概述

Docker使用的是C/S系统架构，详细[架构](https://docs.docker.com/get-started/overview/#docker-architecture)组成如下：

![architecture](https://gitee.com/drakebutterfly/ops-lab/raw/master/docker/images/architecture.png)

整个工作流程是，client客户端发布命令行指令，server服务端daemon守护进程接受指令并运行。一般情况下， docker客户端和服务端运行在同一台机器上。

- 镜像(image)：一个分层的容器模板，可以通过这个模板创建多个内容相同的容器服务。
- 容器(container)：通过镜像启动，容器之间运行环境互相隔离。
- 仓库(registry)：镜像仓库，分为共有仓库和私有仓库，共有仓库为[Docker Hub][https://hub.docker.com/]。



## 1.1 什么是容器

容器是与Host其它进程隔离的一个进程。利用Linux的`namespaces`和`cgroups`技术实现隔离。

- namespaces：隔离工作环境中应用程序的视野：包括进程树、网络、用户和文件系统。
- cgroups：提供物理资源限制，包括CPU、内存、I/O和网络资源。



## 1.2 容器与虚拟机区别

原理上不同。容器是将操作系统层虚拟化，共用Host操作系统Kernel；虚拟机利用Hypervisor虚拟化硬件。

![docker and vm](https://gitee.com/drakebutterfly/ops-lab/raw/master/docker/images/docker%20and%20vm.png)

### 1.2.1 优势

1. 启动快

   容器启动属于秒级，虚拟机是分钟级。启动容器是启动本机一个进程；而虚拟机是直接启动一个操作系统。

2. 资源占用少

   容器应用本质是Host一个进程，只占用需要的资源；虚拟机前提要占用一个完整操作系统的安装和运行资源。

3. 体积小

   容器没有自己的Kernel，共用Host的Kernel，只包含需要用到的库和应用组件。

4. 快速交付和部署



### 1.2.2 劣势

1. 隔离性和安全性

   容器属于进程级别的隔离，cgroups只能限制资源使用上限，不能防止其他应用占用资源；虚拟机是系统级别隔离。



# 2 Docker安装

Docker的安装/删除直接查看官方文档即可。笔者在centos 7环境上，参照[安装文档](https://docs.docker.com/engine/install/centos/)做了验证，步骤如下：

- 卸载旧的版本

  ```shell
  yum remove docker \
                    docker-client \
                    docker-client-latest \
                    docker-common \
                    docker-latest \
                    docker-latest-logrotate \
                    docker-logrotate \
                    docker-engine
  ```

  

- 安装`yum-utils`（提供yum-config-manager），通过`yum-config-manager`配置docker的yum源镜像仓库

  ```shell
  yum install -y yum-utils
  
  yum-config-manager \
      --add-repo \
      https://download.docker.com/linux/centos/docker-ce.repo
  ```

  

- 安装docker engine，涉及yum包有`docker-ce`、`docker-ce-cli`和`containerd.io`

  ```shell
  yum install docker-ce docker-ce-cli containerd.io
  ```

  

- 启动docker

  ```shell
  systemctl start docker
  ```

  

- 验证

  ```shell
  docker version
  
  docker run hello-world
  ```

  

## 2.1 注意事项

（1）使用VMWare虚拟机镜像安装的时候，需要配置DNS服务器，地址是对应的NAT模式下的网关。

```shell
# 1.临时DNS配置方法
echo "nameserver 192.168.190.2" >> /etc/resolv.conf

# 2.永久DNS配置方法
echo "DNS1=192.168.190.2" >> /etc/sysconfig/network-scripts/ifcfg-ens33
```

（2）需要注意操作系统的版本，centos只支持7及以上的版本，可以到[仓库](https://download.docker.com/linux/centos/)查看对应的操作系统版本支持情况。



# 3 Dokcer配置文件

新版的Docker(1.12以后)，推荐用户通过自行创建`/etc/docker/daemon.js`文件来修改docker配置。常用的配置修改有：

- docker根目录
- 仓库镜像地址

样例：

```json
{
    "data-root": "/var/lib/docker",
    "registry-mirrors": [
        "https://index.docker.io/v1/"
    ]
}
```



# 4 Docker常用命令

docker功能很强大，涉及的命令很多，主要可以分为：系统命令、远程仓库命令、镜像命令和容器命令。

![docker commands diagram](https://gitee.com/drakebutterfly/ops-lab/raw/master/docker/images/docker%20commands%20diagram.png)

此处只分析常用命令的日常用法，详细的使用可以在需要的时候查看[官方命令帮助手册](https://docs.docker.com/engine/reference/commandline/cli/)。



## 4.1 系统命令

### help

帮助命令，对所有docker命令都有效。

```shell
docker [command] --help

# 例子
[root@localhost ~]# docker info --help
[root@localhost ~]# docker version --help
```



### docker version

显示docker客户端和服务端的版本信息。还包含有：Go版本、Git commit和操作系统架构。

```shell
Usage:  docker version [OPTIONS]

Show the Docker version information

Options:
  -f, --format string       Format the output using the given Go template
      --kubeconfig string   Kubernetes config file

# 例子
[root@localhost ~]# docker version
Client: Docker Engine - Community
 Version:           20.10.8
 API version:       1.41
 Go version:        go1.16.6
 Git commit:        3967b7d
 Built:             Fri Jul 30 19:55:49 2021
 OS/Arch:           linux/amd64
 Context:           default
 Experimental:      true

Server: Docker Engine - Community
 Engine:
  Version:          20.10.8
  API version:      1.41 (minimum version 1.12)
  Go version:       go1.16.6
  Git commit:       75249d8
  Built:            Fri Jul 30 19:54:13 2021
  OS/Arch:          linux/amd64
  Experimental:     false
 containerd:
  Version:          1.4.10
  GitCommit:        8848fdb7c4ae3815afcc990a8a99d663dda1b590
 runc:
  Version:          1.0.2
  GitCommit:        v1.0.2-0-g52b36a2
 docker-init:
  Version:          0.19.0
  GitCommit:        de40ad0
```



### docker info

主要关注容器和镜像的总体情况，docker的根目录与仓库信息。

```shell
Usage:  docker info [OPTIONS]

Display system-wide information

Options:
  -f, --format string   Format the output using the given Go template

# 例子
[root@localhost ~]# docker info
Client:
 Context:    default
 Debug Mode: false
 Plugins:
  app: Docker App (Docker Inc., v0.9.1-beta3)
  buildx: Build with BuildKit (Docker Inc., v0.6.1-docker)
  scan: Docker Scan (Docker Inc., v0.8.0)

Server:
 Containers: 2
  Running: 1
  Paused: 0
  Stopped: 1
 Images: 2
 Server Version: 20.10.8
 Storage Driver: overlay2
  Backing Filesystem: xfs
  Supports d_type: true
  Native Overlay Diff: false
  userxattr: false
 Logging Driver: json-file
 Cgroup Driver: cgroupfs
 Cgroup Version: 1
 Plugins:
  Volume: local
  Network: bridge host ipvlan macvlan null overlay
  Log: awslogs fluentd gcplogs gelf journald json-file local logentries splunk syslog
 Swarm: inactive
 Runtimes: io.containerd.runc.v2 io.containerd.runtime.v1.linux runc
 Default Runtime: runc
 Init Binary: docker-init
 containerd version: 8848fdb7c4ae3815afcc990a8a99d663dda1b590
 runc version: v1.0.2-0-g52b36a2
 init version: de40ad0
 Security Options:
  seccomp
   Profile: default
 Kernel Version: 3.10.0-514.el7.x86_64
 Operating System: CentOS Linux 7 (Core)
 OSType: linux
 Architecture: x86_64
 CPUs: 1
 Total Memory: 976.5MiB
 Name: 192.168.190.107
 ID: 3ETG:MZW4:RFGB:5WAE:DDIH:L5GK:6PEU:UGIU:TRWZ:FNKH:NBXS:GJQJ
 Docker Root Dir: /var/lib/docker
 Debug Mode: false
 Registry: https://index.docker.io/v1/
 Labels:
 Experimental: false
 Insecure Registries:
  127.0.0.0/8
 Live Restore Enabled: false
```



## 4.2 远程仓库指令

### docker login

登录仓库。获取镜像的时候可以不需要登录，当需要推送本地镜像到远程仓库的时候，需要先登录才行。

```shell
Usage:  docker login [OPTIONS] [SERVER]

Log in to a Docker registry.
If no server is specified, the default is defined by the daemon.

Options:
  -p, --password string   Password
      --password-stdin    Take the password from stdin
  -u, --username string   Username

# 例子
# 1、不推荐通过-p的方式登录
# 2、通过此方法登录会导致用户名和密码明文保存在/root/.docker/config.json中，使用完要及时推出
[root@localhost ~]# docker login -u username -p password
WARNING! Using --password via the CLI is insecure. Use --password-stdin.
WARNING! Your password will be stored unencrypted in /root/.docker/config.json.
```



### docker logout

退出登录

```shell
Usage:  docker logout [SERVER]

# 例子
# 退出后，会将/root/.docker/config.json中的认证信息清楚
[root@localhost ~]# docker logout
Removing login credentials for https://index.docker.io/v1/
```



### docker search

搜索远程仓库中的镜像信息

```shell
Usage:  docker search [OPTIONS] TERM

Search the Docker Hub for images

Options:
  -f, --filter filter   Filter output based on conditions provided
      --format string   Pretty-print search using a Go template
      --limit int       Max number of search results (default 25)
      --no-trunc        Don't truncate output

# 例子
[root@localhost ~]# docker search nginx
NAME           DESCRIPTION                STARS     OFFICIAL   AUTOMATED
nginx          Official build of Nginx.   15558     [OK]
mailu/nginx    Mailu nginx frontend       9                    [OK]
....省略....

# 筛选官方镜像
[root@localhost ~]# docker search nginx -f is-official=true
NAME      DESCRIPTION                STARS     OFFICIAL   AUTOMATED
nginx     Official build of Nginx.   15558     [OK]
```



## 4.3 镜像命令

### docker pull

获取镜像

```shell
Usage:  docker pull [OPTIONS] NAME[:TAG|@DIGEST]

Pull an image or a repository from a registry

Options:
  -a, --all-tags                Download all tagged images in the repository
      --disable-content-trust   Skip image verification (default true)
      --platform string         Set platform if server is multi-platform capable
  -q, --quiet                   Suppress verbose output

# sample
[root@192 ~]# docker pull hello-world
Using default tag: latest
latest: Pulling from library/hello-world
Digest: sha256:9ade9cc2e26189a19c2e8854b9c8f1e14829b51c55a630ee675a5a9540ef6ccf
Status: Image is up to date for hello-world:latest
docker.io/library/hello-world:latest

[root@192 ~]# docker pull -q hello-world
docker.io/library/hello-world:latest
```



### docker tag

公有仓库的镜像名，组成：项目名（docker ID）/镜像名:标签，如：drake/nginx:latest

私有仓库的标准镜像名由四部分组成：仓库地址/项目名/镜像名:标签，如：daocloud.io/drake/nginx:latest

```shell
Usage:  docker tag SOURCE_IMAGE[:TAG] TARGET_IMAGE[:TAG]

Create a tag TARGET_IMAGE that refers to SOURCE_IMAGE

[root@192 ~]# docker tag hello-world:latest  drakebutterfly/hello-world:latest
```



### docker push

推送镜像到远程仓库，需要先登录。

```shell
Usage:  docker push [OPTIONS] NAME[:TAG]

Push an image or a repository to a registry

Options:
  -a, --all-tags                Push all tagged images in the repository
      --disable-content-trust   Skip image signing (default true)
  -q, --quiet                   Suppress verbose output

[root@192 ~]# docker push drakebutterfly/hello-world:latest
The push refers to repository [docker.io/drakebutterfly/hello-world]
e07ee1baac5f: Mounted from library/hello-world
latest: digest: sha256:f54a58bc1aac5ea1a25d796ae155dc228b3f0e11d046ae276b39c4bf2f13d8c4 size: 525
```



### docker images

查看本地镜像信息。

```shell
Usage:  docker images [OPTIONS] [REPOSITORY[:TAG]]

List images

Options:
  -a, --all             Show all images (default hides intermediate images)
      --digests         Show digests
  -f, --filter filter   Filter output based on conditions provided
      --format string   Pretty-print images using a Go template
      --no-trunc        Don't truncate output
  -q, --quiet           Only show image IDs

[root@192 ~]# docker images
REPOSITORY                   TAG       IMAGE ID       CREATED      SIZE
nginx                        latest    f8f4ffc8092c   4 days ago   133MB
hello-world                  latest    feb5d9fea6a5   8 days ago   13.3kB

[root@192 ~]# docker images -q
f8f4ffc8092c
feb5d9fea6a5
```



### docker rmi

删除本地镜像。

```shell
Usage:  docker rmi [OPTIONS] IMAGE [IMAGE...]

Remove one or more images

Options:
  -f, --force      Force removal of the image
      --no-prune   Do not delete untagged parents

# 使用ID删除
[root@192 ~]# docker rmi d23bdf5b1b1b
Untagged: java:latest
Untagged: java@sha256:c1ff613e8ba25833d2e1940da0940c3824f03f802c449f3d1815a66b7f8c0e9d
Deleted: sha256:d23bdf5b1b1b1afce5f1d0fd33e7ed8afbc084b594b9ccf742a5b27080d8a4a8
Deleted: sha256:0132aeca1bc9ac49d397635d34675915693a8727b103639ddee3cc5438e0f60a
Deleted: sha256:c011315277e16e6c88687a6c683e388e2879f9a195113129a2ca12f782d9fcf9
Deleted: sha256:3181aa7c07970b525de9d3bd15c4c3710a2ab49fd5927df41e5586d9b89b1480
Deleted: sha256:b0053647bc72f97b7a9709a505a20a7a74a556c6aa025979e36532ff3df7cb8d
Deleted: sha256:0877f4904e80b44741cc07706b19c6d415724b20128f4b26ee59faec9a859416
Deleted: sha256:dbf7b16cf5d32dfec3058391a92361a09745421deb2491545964f8ba99b37fc2
Deleted: sha256:4cbc0ad7007fe8c2dfcf2cdc82fdb04f35070f0e2a04d5fa35093977a3cc1693
Deleted: sha256:a2ae92ffcd29f7ededa0320f4a4fd709a723beae9a4e681696874932db7aee2c

# 使用全量镜像名删除
[root@192 ~]# docker rmi hello-world:latest
Untagged: hello-world:latest
Untagged: hello-world@sha256:9ade9cc2e26189a19c2e8854b9c8f1e14829b51c55a630ee675a5a9540ef6ccf
```



### docker save

保存镜像为离线文件。

```shell
Usage:  docker save [OPTIONS] IMAGE [IMAGE...]

Save one or more images to a tar archive (streamed to STDOUT by default)

Options:
  -o, --output string   Write to a file, instead of STDOUT

[root@192 ~]# docker save drakebutterfly/hello-world:latest > hello.tar

# 等价上例
[root@192 ~]# docker save -o hello.tar drakebutterfly/hello-world:latest

# 使用gzip压缩
[root@192 ~]# docker save drakebutterfly/hello-world:latest | gzip > hello.tar.gz
```



### docker load

加载镜像，从tar包（即使使用gzip、bzip2或xz压缩）

```shell
Usage:  docker load [OPTIONS]

Load an image from a tar archive or STDIN

Options:
  -i, --input string   Read from tar archive file, instead of STDIN
  -q, --quiet          Suppress the load output

[root@192 ~]# docker load < hello.tar

# 等价上例
[root@192 ~]# docker load -i hello.tar

[root@192 ~]# docker load < hello.tar.gz
```



### docker history

查看镜像分层历史。

```shell
Usage:  docker history [OPTIONS] IMAGE

Show the history of an image

Options:
      --format string   Pretty-print images using a Go template
  -H, --human           Print sizes and dates in human readable format (default true)
      --no-trunc        Don't truncate output
  -q, --quiet           Only show image IDs

[root@192 ~]# docker history nginx:latest
IMAGE          CREATED      CREATED BY                                      SIZE      COMMENT
f8f4ffc8092c   4 days ago   /bin/sh -c #(nop)  CMD ["nginx" "-g" "daemon…   0B
<missing>      4 days ago   /bin/sh -c #(nop)  STOPSIGNAL SIGQUIT           0B
<missing>      4 days ago   /bin/sh -c #(nop)  EXPOSE 80                    0B
<missing>      4 days ago   /bin/sh -c #(nop)  ENTRYPOINT ["/docker-entr…   0B
<missing>      4 days ago   /bin/sh -c #(nop) COPY file:09a214a3e07c919a…   4.61kB
<missing>      4 days ago   /bin/sh -c #(nop) COPY file:0fd5fca330dcd6a7…   1.04kB
<missing>      4 days ago   /bin/sh -c #(nop) COPY file:0b866ff3fc1ef5b0…   1.96kB
<missing>      4 days ago   /bin/sh -c #(nop) COPY file:65504f71f5855ca0…   1.2kB
<missing>      4 days ago   /bin/sh -c set -x     && addgroup --system -…   64MB
<missing>      4 days ago   /bin/sh -c #(nop)  ENV PKG_RELEASE=1~buster     0B
<missing>      4 days ago   /bin/sh -c #(nop)  ENV NJS_VERSION=0.6.2        0B
<missing>      4 days ago   /bin/sh -c #(nop)  ENV NGINX_VERSION=1.21.3     0B
<missing>      4 days ago   /bin/sh -c #(nop)  LABEL maintainer=NGINX Do…   0B
<missing>      4 days ago   /bin/sh -c #(nop)  CMD ["bash"]                 0B
<missing>      4 days ago   /bin/sh -c #(nop) ADD file:99db7cfe7952a1c7a…   69.2MB
```



### docker build

通过dockerfile构建镜像。

```shell
Usage:  docker build [OPTIONS] PATH | URL | -

Build an image from a Dockerfile

Options:
  -f, --file string             Name of the Dockerfile (Default is 'PATH/Dockerfile')
  -t, --tag list                Name and optionally a tag in the 'name:tag' format

# dockerfile可以是URL，如git地址
# ./表示镜像构建上下文路径，-f默认dockerfile从上下文路径取
[root@192 ~]# docker build -f dockerfile -t test:latest ./
```



## 4.4 容器命令

### docker run

运行镜像，新建并启动容器。

```shell
Usage:  docker run [OPTIONS] IMAGE [COMMAND] [ARG...]

Run a command in a new container

Options:
   -d                         Run container in background and print container ID
   -p                         Publish a container's port(s) to the host
   -v                         Bind mount a volume
   --name string              Assign a name to the container

# 后端运行一个名称为nginx01的容器
# 映射容器80端口到Host8080端口
# 外挂容器/usr/share/nginx/html目录到Host/test目录中，且在容器内部无法修改
[root@192 ~]# docker run -d -p 8080:80 -v /test:/usr/share/nginx/html:ro --name nginx01 nginx:latest
```



### docker ps

列出容器。

```shell
Usage:  docker ps [OPTIONS]

List containers

Options:
  -a, --all             Show all containers (default shows just running)
  -f, --filter filter   Filter output based on conditions provided
      --format string   Pretty-print containers using a Go template
  -n, --last int        Show n last created containers (includes all states) (default -1)
  -l, --latest          Show the latest created container (includes all states)
      --no-trunc        Don't truncate output
  -q, --quiet           Only display container IDs
  -s, --size            Display total file sizes

# 只列出运行中的容器，如果需要列出其它状态的容器，需要使用-a选项
[root@192 ~]# docker ps
CONTAINER ID   IMAGE          COMMAND                  CREATED         STATUS         PORTS                                   NAMES
b98408c0e2a1   nginx:latest   "/docker-entrypoint.…"   7 minutes ago   Up 7 minutes   0.0.0.0:8080->80/tcp, :::8080->80/tcp   nginx01
```



### docker rm

删除容器。

```shell
Usage:  docker rm [OPTIONS] CONTAINER [CONTAINER...]

Remove one or more containers

Options:
  -f, --force     Force the removal of a running container (uses SIGKILL)
  -l, --link      Remove the specified link
  -v, --volumes   Remove anonymous volumes associated with the container

# 使用容器ID删除
[root@192 ~]# docker rm 5c1a90a74cde
5c1a90a74cde

# 使用容器名称删除
[root@192 ~]# docker rm agitated_carson
agitated_carson
```



### docker create/start/stop/restart/kill

容器的制作/启动/停止/重启/强制停止

```shell
docker start 容器ID/容器名称     # 配合docker create使用
docker stop 容器ID/容器名称
docker restart 容器ID/容器名称
docker kill 容器ID/容器名称      # 强制停止容器
```



### docker logs

查看容器日志。

```shell
Usage:  docker logs [OPTIONS] CONTAINER

Fetch the logs of a container

Options:
      --details        Show extra details provided to logs
  -f, --follow         Follow log output
      --since string   Show logs since timestamp (e.g. 2013-01-02T13:23:37Z) or relative (e.g. 42m for 42 minutes)
  -n, --tail string    Number of lines to show from the end of the logs (default "all")
  -t, --timestamps     Show timestamps
      --until string   Show logs before a timestamp (e.g. 2013-01-02T13:23:37Z) or relative (e.g. 42m for 42 minutes)

[root@192 ~]# docker logs -f nginx01

[root@192 ~]# docker logs -f b98408c0e2a1
```



### docker top

查看容器内部进程信息。类比Linux系统的top命令，选项也是一致的。

```shell
Usage:  docker top CONTAINER [ps OPTIONS]

Display the running processes of a container

[root@192 ~]# docker top b98408c0e2a1
UID                 PID                 PPID                C                   STIME               TTY                 TIME                CMD
root                4553                4533                0                   17:25               pts/0               00:00:00            nginx: master process nginx -g daemon off;
101                 4602                4553                0                   17:26               pts/0               00:00:00            nginx: worker process
```



### docker inspect

查看容器（镜像）元数据。

获取容器的详细低阶信息，包括：容器ID、创建时间、容器状态、镜像、Host日志路径、Host配置（挂载卷和端口映射等）和网络配置等。

如果是查看镜像的详细信息，包括：镜像ID、创建时间、容器配置（分层命令）和RootFS信息等。

```shell
Usage:  docker inspect [OPTIONS] NAME|ID [NAME|ID...]

Return low-level information on Docker objects

Options:
  -f, --format string   Format the output using the given Go template
  -s, --size            Display total file sizes if the type is container
      --type string     Return JSON for specified type
```



### docker exec

进入正在运行的容器。

```shell
Usage:  docker exec [OPTIONS] CONTAINER COMMAND [ARG...]

Run a command in a running container

Options:
  -d, --detach               Detached mode: run command in the background
      --detach-keys string   Override the key sequence for detaching a container
  -e, --env list             Set environment variables
      --env-file list        Read in a file of environment variables
  -i, --interactive          Keep STDIN open even if not attached
      --privileged           Give extended privileges to the command
  -t, --tty                  Allocate a pseudo-TTY
  -u, --user string          Username or UID (format: <name|uid>[:<group|gid>])
  -w, --workdir string       Working directory inside the container

# 进入容器并开启bash进程
[root@192 ~]# docker exec -it b98408c0e2a1 bash
```



### docker cp

容器和Host之间相互拷贝。

```shell
Usage:  docker cp [OPTIONS] CONTAINER:SRC_PATH DEST_PATH|-
        docker cp [OPTIONS] SRC_PATH|- CONTAINER:DEST_PATH

Copy files/folders between a container and the local filesystem

# 将容器中的文件拷贝到Host
[root@192 ~]# docker cp b98408c0e2a1:/docker-entrypoint.sh ./

# 将Host文件拷贝到容器中
[root@192 ~]# docker cp hello.tar b98408c0e2a1:/
```



# 5 Docker镜像

镜像是一种轻量级、可执行的独立软件包，用来打包软件运行环境和基于运行环境开发的软件，它包含运行某个软件所需要的所有内容，包含：代码、运行库、环境变量和配置文件。镜像使用UnionFS（联合文件系统）技术，镜像本质是通过一层一层的文件系统组成的，开发者可以基于基础镜像[scratch](https://hub.docker.com/_/scratch/)添加自己的一层或多层文件系统，制作自己的应用镜像。

![image layer](https://gitee.com/drakebutterfly/ops-lab/raw/master/docker/images/image%20layer.jpg)

容器只能使用Host的Kernel，并且不能修改。所有容器共有内核。如果容器对Kernel版本有要求（比如应用只能在某个kernel版本下运行），不建议使用容器，虚拟机更适合。企业场景可以更换对应版本Kernel，附带操作系统镜像，毕竟容器化优势很明显。

- bootfs：主要使用bootloader和Kernel，bootloader主要是引导加载Kernel，当我们加载镜像的时候，会通过bootloader加载Kernel，Docker镜像最底层是bootfs，当bootfs加载完成后，整个Kernel内核都在内存中了，bootfs也就可以卸载。通过`uname -r`可以查到所有镜像的Kernel和Host的是一样的。
- rootfs：可以理解是各种不同操作系统发行版的基础镜像层，如Centos，RedHat等。包含了标准的Linux系统的命令、工具、程序库和文件系统。



## 5.1 镜像分层与容器

镜像就是一个模板，通过`docker run`运行镜像生成容器，本质就是生成一个容器层（动态的可读写）在镜像的顶部，容器层以下都叫镜像层，镜像层是静态不可变的。

![image container](https://gitee.com/drakebutterfly/ops-lab/raw/master/docker/images/image%20container.jpg)

- 镜像层：只读

  dockerfile每一条命令对应一个镜像层。每一个镜像层的操作不会影响上一层，即使有删除操作也只是在本层中起作用，而上一层创建的文件依然保留。有上下依赖关系的命令，尽可能放到同一条命令中，通过`&&`符号连接。每一层镜像都是可以被共享的，在Docker本地都是唯一的，节省磁盘资源。

- 容器层：可读写

  只有当需要修改时才复制一份数据，这种特性被称作Copy-on-Write。容器层保存的是镜像变化的部分，不会对镜像本身进行任何修改。

| 文件操作 | 说明 |
| :--- | :---- |
| 文件读取 | 在容器中读取某个文件时，Docker会从上往下依次在各镜像层中查找此文件。一旦找到，打开并读入内存。 |
| 添加文件 | 在容器中创建文件时，新文件被添加到容器层中。 |
| 修改文件 | 在容器中修改已存在的文件时，Docker会从上往下依次在各镜像层中查找此文件。一旦找到，立即将其复制到容器层，然后修改之。 |
| 删除文件 | 在容器中删除文件时，Docker会从上玩下依次在各镜像层中查找此文件。一旦找到，会在容器层中记录此删除操作。 |



## 5.2 容器与镜像的转换

容器运行过程中，文件系统的所有变更都是在容器层上面的，容器转换成镜像本质就是将容器层转换成一镜像层。反过来，镜像转换层容器，本质就是生成一个容器层。



### 5.2.1 容器转换层镜像

通过`docker commit`命令实现。

```shell
Usage:  docker commit [OPTIONS] CONTAINER [REPOSITORY[:TAG]]

Create a new image from a container's changes

Options:
  -a, --author string    Author (e.g., "John Hannibal Smith <hannibal@a-team.com>")
  -c, --change list      Apply Dockerfile instruction to the created image
  -m, --message string   Commit message
  -p, --pause            Pause container during commit (default true)

[root@192 ~]# docker commit -a drake -m "nginx with expose port 8080" b98408c0e2a1 drakebutterfly/nginx:latest
sha256:c70b840d12b36c708b8c6d0834ede355bdbc2fbd130cb561c2773a5e358cbac8
```



### 5.2.2 镜像转换成容器

通过`docker run`命令实现，[上面](#docker run)有详细的使用介绍。



# 6 Docker数据卷

数据卷设计的目的，在于数据的**持久化**和容器集群之间的**数据共享**。数据卷具备以下特点：

1. 数据卷独立于容器的生命周期，即使容器删除了，外挂在Host的数据卷也不会受到影响。所以当使用匿名挂载方式时，注意删除容器后需要手动删除数据卷，否则会存在垃圾数据。
2. 数据卷在容器启动时初始化，如果容器使用的镜像在挂载点有数据，这些数据会拷贝更新到新初始化的数据卷中。
3. 数据卷可以在容器之间共享复用。
4. 可以对数据卷内的内容直接修改，容器能够感知到数据的变化。



## 6.1 数据卷常用命令

数据卷的命令，就三个功能，分别是：创建、删除、查看。

```shell
Usage:  docker volume COMMAND

Manage volumes

Commands:
  create      Create a volume
  inspect     Display detailed information on one or more volumes
  ls          List volumes
  prune       Remove all unused local volumes
  rm          Remove one or more volumes
```



### 6.1.1 创建

通过`docker volume create`命令实现。

```shell
Usage:  docker volume create [OPTIONS] [VOLUME]

Create a volume

Options:
  -d, --driver string   Specify volume driver name (default "local")
      --label list      Set metadata for a volume
  -o, --opt map         Set driver specific options (default map[])

[root@192 ~]# docker volume create v1
v1
```



### 6.1.2 查看

通过`docker volume ls`命令可以查看数据卷列表。

```shell
Usage:  docker volume ls [OPTIONS]

List volumes

Aliases:
  ls, list

Options:
  -f, --filter filter   Provide filter values (e.g. 'dangling=true')
      --format string   Pretty-print volumes using a Go template
  -q, --quiet           Only display volume names

[root@192 ~]# docker volume ls
DRIVER    VOLUME NAME
local     v1
```

通过`docker volume inspect`命令可以查看数据卷详细元数据，包括：创建时间、挂载点和数据卷名称等。

```shell
[root@192 ~]# docker volume inspect v1
[
    {
        "CreatedAt": "2021-10-03T09:49:10+08:00",
        "Driver": "local",
        "Labels": {},
        "Mountpoint": "/var/lib/docker/volumes/v1/_data",
        "Name": "v1",
        "Options": {},
        "Scope": "local"
    }
]
```



### 6.1.3 删除

通过`docker volume rm`删除数据卷。

```shell
Usage:  docker volume rm [OPTIONS] VOLUME [VOLUME...]

Remove one or more volumes. You cannot remove a volume that is in use by a container.

Aliases:
  rm, remove

Examples:

$ docker volume rm hello
hello


Options:
  -f, --force   Force the removal of one or more volumes
```

通过`docker volume prune`删除所有没有正在用的数据卷。

```shell
Usage:  docker volume prune [OPTIONS]

Remove all unused local volumes

Options:
      --filter filter   Provide filter values (e.g. 'label=<label>')
  -f, --force           Do not prompt for confirmation

[root@192 ~]# docker volume prune
WARNING! This will remove all local volumes not used by at least one container.
Are you sure you want to continue? [y/N] y
Deleted Volumes:
v1
1ccfcc23229324509fb31f7e72c2d7cde84815529af9de4d36228955fca34819
v2
v3
v4

Total reclaimed space: 13.35kB
```

**注意：匿名/具名挂载，删除容器后，数据卷不会自动删除，仍然占用磁盘资源，需要手动删除。**



## 6.2 数据卷挂载方式

在使用`docker run`创建容器的时候，通过`-v`选项，实现容器的数据卷挂载，有三种方式：

```shell
-v Host目录:容器目录      # 指定路径挂载，此方式通过docker volume ls是查不到的
-v 容器目录              # 匿名挂载
-v 数据卷名称:容器目录     # 具名挂载
```



### 6.2.1 指定路径挂载

通过此方法指定路径，如果路径不存在会在Host中自动创建，但是Host目录的内容会覆盖掉容器中的内容。例如：如果把容器内配置文件通过此方式挂载出来，因为Host目录为空，会导致容器启动失败。

```shell
[root@192 ~]# docker run -d -p 8080:80 -v /root/test:/etc/nginx --name nginx01 nginx:latest
```



### 6.2.2 匿名挂载

会自动生成一个volume，容器中的内容会同步显示在volume中。

```shell
[root@192 ~]# docker run -d -p 8080:80 -v /etc/nginx --name nginx02 nginx:latest

# 通过 docker inspect 容器ID 的方式来查看挂载情况
[root@192 ~]# docker inspect 96a45bfb3dd8 -f "{{json .Mounts}}"
[{"Type":"volume","Name":"45fff24748a46bccb6e6795ca6d0f7c1ac37c12b32f1bfdf4733f08f07675d5d","Source":"/var/lib/docker/volumes/45fff24748a46bccb6e6795ca6d0f7c1ac37c12b32f1bfdf4733f08f07675d5d/_data","Destination":"/etc/nginx","Driver":"local","Mode":"","RW":true,"Propagation":""}]
```



### 6.2.3 具名挂载

需要先创建一个volume，容器中的内容会同步显示在volume中。

```shell
[root@192 ~]# docker run -d -p 8080:80 -v name-nginx:/etc/nginx --name nginx03 nginx:latest

[root@192 ~]# docker inspect ad7776dd02d1 -f "{{json .Mounts}}"
[{"Type":"volume","Name":"name-nginx","Source":"/var/lib/docker/volumes/name-nginx/_data","Destination":"/etc/nginx","Driver":"local","Mode":"z","RW":true,"Propagation":""}]
```



## 6.3 数据卷操作权限

可以通过`-v 容器内路径:ro/rw`控制容器对数据卷内数据的操作权限。

- ro：容器只有读取权限，只有Host才有写权限。
- rw：默认，容器拥有读写权限。

```shell
[root@192 ~]# docker run -d -p 8080:80 -v name-nginx:/etc/nginx:ro --name nginx04 nginx:latest

[root@192 ~]# docker inspect abad755ccbe0 -f "{{json .Mounts}}"
[{"Type":"volume","Name":"name-nginx","Source":"/var/lib/docker/volumes/name-nginx/_data","Destination":"/etc/nginx","Driver":"local","Mode":"ro","RW":false,"Propagation":""}]
```



## 6.4 数据卷容器

实现不同容器之间的数据共享，可以通过`--volumes-from`的方式挂载某个容器实现，被挂载的容器也称为数据卷容器。

```shell
[root@192 ~]# docker run -d -p 8080:80 --volumes-from nginx04 --name nginx05 nginx:latest
[root@192 ~]# docker inspect 03866701a6a6 -f "{{json .Mounts}}"
[{"Type":"volume","Name":"name-nginx","Source":"/var/lib/docker/volumes/name-nginx/_data","Destination":"/etc/nginx","Driver":"local","Mode":"","RW":false,"Propagation":""}]
```

注意：

1. 使用`--volume-from`参数所挂载数据卷的容器本身并不需要保持运行状态。
2. 如果删除了数据卷容器，数据卷并不会被自动删除。



# 7 Dockerfile

Dockerfile是一个文本文件，通过一条条指令描述镜像每一层是如何构建的。



## 7.1 语法

Dockerfile语法，本质就是dockerfile指令的使用，具有以下几个基本特点：

1. 每个指令必须是大写字母
2. 指令按从上到下的顺序执行
3. 使用`#`标识注释
4. 每个指令都会创建一个镜像层



## 7.2 使用场景

dockerfile端到端的使用场景，包括dockerfile开发、镜像的构建和镜像的发布，详细的流程为：

1. 编写dockerfile文件
2. 使用dockerfile，通过`docker build`命令构建一个镜像
3. 使用`docker run`运行并验证所构建的镜像的正确性
4. 通过`docker push`将镜像发布到远程仓库



[hello-world](https://github.com/docker-library/hello-world)的dockerfile样例：

```dockerfile
FROM scratch
COPY hello /
CMD ["/hello"]
```

获取dockerfile和hello文件（需要注意hello文件的使用权限问题），复现构建过程：

```shell
[root@192 hello-world]# docker build -f Dockerfile -t my-hello-world:latest ./
Sending build context to Docker daemon  15.87kB
Step 1/3 : FROM scratch
 --->
Step 2/3 : COPY hello /
 ---> 0e95a48b1a3a
Step 3/3 : CMD ["/hello"]
 ---> Running in 27ee24d625a5
Removing intermediate container 27ee24d625a5
 ---> f47f5cd8c4f6
Successfully built f47f5cd8c4f6
Successfully tagged my-hello-world:latest
```

注意观察，每一个指令对应生成一层镜像。`docker build`的使用，[上面](#docker build)有详细介绍。需要注意的是命令最后的一个参数`./`，`./`表示当前目录，`-f`后面跟的Dockerfile文件默认就是在当前目录。如果想要指定某个文件作为dockerfile，可以使用`-f 绝对路径/相对路径`的方式。



## 7.3 镜像构建上下文

`docker build`命令最后一个参数`./`就是镜像**构建上下文**，这是docker很重要的一个特性设计。`docker build`命令构建镜像，实际上是在服务端镜像构建的，并非在本地客户端。那么在这种C/S架构中，如何才能让服务端获得本地文件呢？这就引入了上下文的概念。构建过程如下：

1. 通过`docker build`命令指定构建镜像上下文路径；
2. 客户端会将上下文路径下面的所有内容打包，然后上传给服务端；
3. 服务器收到这个上下文包，就获得构建镜像所需要的一切文件，或者说构建镜像所需的一切文件必须在构建上下文中。之后，完成镜像的构建即可。

观察`docker build`的输出，很容易发现发送上下文的过程：

```shell
[root@192 hello-world]# docker build -f Dockerfile -t my-hello-world:latest ./
Sending build context to Docker daemon  15.87kB
```

之所以发送的上下文路径占用磁盘很小，是因为上下文中只有一个Dockerfile和hello小执行文件。

一般来说，上下文路径上只应包含镜像所需的文件。如果是在根目录下执行上诉命令，会发现发送很大的东西到docker服务器，导致整个构建过程及其缓慢。如果目录下有些东西不希望构建的时候传给Docker服务器，可以用`.gitignore`一样的语法写一个`.dockerignore`文件，会在发送上下文的时候剔除相关内容。

**实际使用中，建议新建一个构建目录，作为构建的上下文**。



## 7.4 指令使用

dockerfile提供了十多个指令，供开发者们玩耍，全量指令列表（Version20.10）如下：

| 指令        | 说明                                                         |
| :---------- | :----------------------------------------------------------- |
| FROM        | 启动新的镜像构建，并指定dockerfile的基础镜像                 |
| RUN         | 在当前镜像层上面，执行任何Shell命令                          |
| CMD         | 一个dockerfile文件只能有一条CMD命令，用来设置启动容器时默认执行的命令 |
| LABEL       | 以键值对的形式，给镜像添加一些元数据                         |
| MAINTAINER  | deprecated，标识镜像的作者，使用LABEL指令来代替              |
| EXPOSE      | 声明容器内对外开放的端口                                     |
| ENV         | 设置容器运行的环境变量                                       |
| ADD         | 往镜像中添加文件，如果是gzip，bzip2或xz形式压缩文件，会自动解压缩 |
| COPY        | 往镜像中复制文件                                             |
| ENTRYPOINT  | 与CMD指令类似，区别在于：运行容器时添加在镜像后面的命令/参数，对ENTRYPOINT是拼接，对CMD是覆盖 |
| VOLUME      | 在镜像内创建挂载点，容器运行时，自动执行匿名挂载             |
| USER        | 为容器的运行及接下来RUN、CMD、ENTRYPOINT等指令的运行指定用户或UID |
| WORKDIR     | 为接下来执行的指令指定一个工作目录                           |
| ARG         | 设置构建过程的参数变量，可以在构建过程中指定输入             |
| ONBUILD     | 当前镜像构建并不会被执行。只有以当前镜像为基础镜像，在FROM指令后立即执行 |
| STOPSIGNAL  | 可以设置容器停止信号，默认的stop-signal是SIGTERM             |
| HEALTHCHECK | 告诉Docker应该如何进行判断容器的状态是否正常                 |
| SHELL       | 指定RUN、ENTRYPOINT和CMD指令的shell                          |

详细的指令使用，查看[官方Dockerfile说明](https://docs.docker.com/engine/reference/builder/#from)即可。接下来，介绍一下常用指令的常用场景。



### FROM

指定基础镜像。一般来说`FROM`指令是Dockerfile的第一条指令，除了`ARG`这个特殊情况。Dockerfile赋予了开发者定制镜像的能力，定制镜像需要以一个镜像为基础。Docker提供了一个空白镜像`scratch`，类似java中`Object`的存在，一般操作系统层级的基础镜像，都是以`scratch`作为基础镜像的。

```dockerfile
ARG BASE_VERSION=latest
FROM base:${BASE_VERSION}
ENV MY_NAME=drake
CMD /base/run
```



### ARG

设置构建参数。唯一一个可以在`FROM`之前的指令。定义镜像**构建过程**的参数，以及定义其默认值。使用`docker build`命令时，可以通过使用`--build-arg <varname>=<value>`来替换默认值。

```shell
[root@192 base]# docker build --build-arg BASE_VERSION=alpha -t base:alpha ./
```



### ENV

设置容器**运行**的环境变量，且在镜像构建过程中有效。使用`docker run`命令时，可以通过`--env <key>=<value>`来替换默认值。

```shell
[root@192 base]# docker run -d --env MY_NAME=leo base:alpha
```



### LABEL

设置元数据，用来声明镜像的作者、文档地址等。

```shell
# 语法
LABEL <key>=<value> <key>=<value> <key>=<value> ...

# 例子
LABEL author="drake" modify.time="20211010"

[root@192 base]# docker inspect base:alpha -f "{{json .ContainerConfig.Labels}}"
{"author":"drake","modify.time"="20211010"}
```



### WORKDIR

为Dockerfile中`WORKDIR`指令接下来需要执行的`RUN`，`CMD`，`ENTRYPOINT`，`COPY`和`ADD`指令，指定工作目录，如果不存在则创建一个。

```dockerfile
# 会在/test/test.txt文件中写入hello world
WORKDIR /test
RUN echo "hello world" > test.txt
```



### COPY

将构建上下文目录中的文件/目录复制到新的一层镜像内。

```dockerfile
COPY [--chown=<user>:<group>] <src>... <dest>
COPY [--chown=<user>:<group>] ["<src>",... "<dest>"]

# 例子1
WORKDIR /test
COPY test.jar /test/

# 例子2
# 可以通过--chown修改文件所属用户和组
COPY --chown=paas:paas test.jar /test/
```



### ADD

具备自动解压的复制文件。

```dockerfile
WORKDIR /test
ADD test.tar.gz /test/
```



### USER

指定用户。为容器的运行及接下来`RUN`，`CMD`和`ENTRYPOINT`等指令的运行指定用户。

```dockerfile
# 语法
USER <user>[:<group>]
USER <UID>[:<GID>]

# 例子
FROM microsoft/windowsservercore
# Create Windows user in the container
RUN net user /add patrick
# Set it for subsequent commands
USER patrick
```



### RUN

`RUN`指令，可以在当前镜像层运行任何指令。在使用`run`指令时，切忌不要每一个shell命令使用一个`RUN`指令，这样会导致镜像层数过多，镜像的层数是有限制的。可以使用`;`或者`&&`等shell命令的方式来将有依赖关系的命令串联起来。

Dockerfile支持Shell类的行尾添加`\`的命令换行方式。

```dockerfile
# 语法
RUN <command>
RUN ["executable", "param1", "param2"]

# 例子1，简单场景
RUN /bin/bash -c 'echo hello'
RUN ["/bin/bash", "-c", "echo hello"]

# 例子2，复杂多命令场景
RUN set -x \
    && addgroup --system --gid 101 nginx \
    && adduser --system --disabled-login --ingroup nginx --no-create-home --home /nonexistent --gecos "nginx user" --shell /bin/false --uid 101 nginx \
    && apt-get update \
    && apt-get install --no-install-recommends --no-install-suggests -y gnupg1 ca-certificates \
    && \
    NGINX_GPGKEY=573BFD6B3D8FBC641079A6ABABF5BD827BD9BF62; \
    found=''
```



### VOLUME

在镜像内创建挂载点，容器运行时，自动执行匿名挂载。使用`docker run`命令时，可以通过`-v`覆盖这个匿名挂载设置。

```dockerfile
# 语法
VOLUME ["<path1>", "<path2>"...]
VOLUME <path>

# 例子
VOLUME /myvol
VOLUME ["v1", "v2"]  # 默认会在容器根目录创建名为v1和v2两个目录
```



### EXPOSE

仅**声明**容器内服务对外提供服务的端口，没有配置映射，起到帮助镜像使用者配置端口映射的作用。运行需要通过`-P`或者`-p`来使端口映射生效。

```dockerfile
# 语法
EXPOSE <port> [<port>/<protocol>...]

# 例子
EXPOSE 4369 5671 5672
EXPOSE 80/tcp 80/udp

# 会将EXPOSE的端口，随机映射到Host
[root@192 base]# docker run -d -P base:alpha

[root@192 base]# docker run -d -p 80:80/tcp -p 80:80/udp base:alpha
```



### CMD

一个dockerfile文件只能有一条CMD指令，用来设置启动容器时默认执行的命令。如果有多条指令，只有最后一条生效。在运行时，可以通过指定新的Shell命令来替换镜像中设置的默认命令。如果base:alpha的CMD指令为`CMD /bin/bash`，这时我们通过`docker run -it base:alpha`来运行，会进入`bash`界面。如果通过`docker run -it base:alpha echo "hello"`运行，会输出hello，然后因为容器内部没有活动进程，容器会关闭。

```dockerfile
# 语法
CMD ["executable","param1","param2"]
CMD ["param1","param2"]
CMD command param1 param2

# 例子
CMD echo "This is a test." | wc -
CMD ["/usr/bin/wc","--help"]
```



### ENTRYPOINT

相对`CMD`指令，`docker run`时，`CMD`指令会被外部输入的替换掉，而`ENTRYPOINT`是拼接在内部命令的后面。

```dockerfile
# 语法
ENTRYPOINT ["executable", "param1", "param2"]
ENTRYPOINT command param1 param2

# 例子1，替换CMD
ENTRYPOINT ["/bin/bash"]

# 例子2，配合CMD使用，CMD作为ENTRYPOINT运行脚本的默认参数
ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["rabbitmq-server"]
```



# 8 参考

1. [Docker官方使用说明](https://docs.docker.com/reference/)
2. [Docker入门教程](http://www.ruanyifeng.com/blog/2018/02/docker-tutorial.html)
3. [Docker的启动文件和配置文件](http://docs.lvrui.io/2017/02/19/docker%E7%9A%84%E5%90%AF%E5%8A%A8%E6%96%87%E4%BB%B6%E5%92%8C%E9%85%8D%E7%BD%AE%E6%96%87%E4%BB%B6/)
4. [Docker的镜像结构原理](https://blog.51cto.com/liuleis/2070461)
5. [Docker数据卷](https://yeasy.gitbook.io/docker_practice/data_management/volume)
6. [使用 Dockerfile 定制镜像 - Docker —— 从入门到实践](https://yeasy.gitbook.io/docker_practice/image/build)

