<h1 align="center">Docker进阶学习</h1>

# 1 Docker资源





# 2 Docker网络

当安装完docker后，会自动创建三个网络。

```shell
[root@192 ~]# docker network ls
NETWORK ID     NAME      DRIVER    SCOPE
faf5f0260257   bridge    bridge    local
3cce13e74b21   host      host      local
e231fa1a4dda   none      null      local
```

当使用`docker run`运行容器时，可以使用`--network`选项来标识选择哪个网络。当不显式指定时，默认使用桥接模式`--network bridge`。



## 2.1 四种网络模式

docker默认支持四种不同的网络模式。

| 网络模式  | 配置                           | 说明                                                         |
| --------- | ------------------------------ | ------------------------------------------------------------ |
| bridge    | --network bridge               | 默认模式。                                                   |
| host      | --network host                 | 容器不会虚拟出自己的网卡，直接使用Host的IP和端口。           |
| none      | --network none                 | 该模式关闭了容器的网络功能。                                 |
| container | --network container:Name_or_ID | 容器不会虚拟出自己的网卡，而是和一个指定的容器共享IP和端口。 |



## 2.2 桥接模式

Docker安装完后，会在Host创建一个名为docker0的虚拟网桥，分配IP地址`172.17.0.1/16`给docker0，此IP地址作为容器的网关。容器与Host的网络通信，借助veth-pair技术实现。

![docker network](https://gitee.com/drakebutterfly/ops-lab/raw/master/docker/images/docker%20network.PNG)

桥接模式下运行一个容器时，docker完成上图网络配置的大致过程如下：

（1）在Host上随机创建一对虚拟网卡veth，分别配置在Host和容器。下图中表示的是，Host对应的虚拟网卡是if5，容器对应的虚拟网卡是if6 。**注意：当容器停止或者删除时，这一对网卡会自动消失**。

```shell
[root@192 ~]# ip add
3: docker0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP
    link/ether 02:42:be:df:5d:c3 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
       valid_lft forever preferred_lft forever
    inet6 fe80::42:beff:fedf:5dc3/64 scope link
6: veth3fc3540@if5: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master docker0 state UP
    link/ether d2:c2:6b:c8:82:69 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet6 fe80::d0c2:6bff:fec8:8269/64 scope link
       valid_lft forever preferred_lft forever
```

（2）从docker0所在的子网中，分配一个IP给容器，并设置docker0的IP地址为容器的默认网关。

```shell
[root@192 ~]# docker ps
CONTAINER ID   IMAGE          COMMAND                  CREATED        STATUS         PORTS                                   NAMES
03866701a6a6   nginx:latest   "/docker-entrypoint.…"   24 hours ago   Up 3 minutes   0.0.0.0:8080->80/tcp, :::8080->80/tcp   nginx05

[root@192 ~]# docker inspect 03866701a6a6
"Networks": {
                "bridge": {
                    "IPAMConfig": null,
                    "Links": null,
                    "Aliases": null,
                    "NetworkID": "faf5f0260257a21868852fb814bb1fff4681f9e06def8dd66f2180c749460ce2",
                    "EndpointID": "022e7a432d835555649dda503e1cca46b3f10a86c763fe12416b90d4b9584d96",
                    "Gateway": "172.17.0.1",
                    "IPAddress": "172.17.0.2",
                    "IPPrefixLen": 16,
                    "IPv6Gateway": "",
                    "GlobalIPv6Address": "",
                    "GlobalIPv6PrefixLen": 0,
                    "MacAddress": "02:42:ac:11:00:02",
                    "DriverOpts": null
                }
```



## 2.3 自定义网络

docker允许用户自定义网络，通过`docker network create`命令即可实现。通过自定义网络，可以做到集群网络隔离。

```shell
[root@192 ~]# docker network create -d bridge --subnet 172.18.0.0/16 --gateway 172.18.0.1 mynet

[root@192 ~]# docker network ls
NETWORK ID     NAME      DRIVER    SCOPE
faf5f0260257   bridge    bridge    local
3cce13e74b21   host      host      local
9e288e92b098   mynet     bridge    local
e231fa1a4dda   none      null      local

[root@192 ~]# docker run -d -p 8081:80 --network mynet --name mynet-nginx nginx:latest
71b3d459c0a5962854c140ef8a66f5080a493e8c5e2fcb9dc55baa1dfc5b3b99

[root@192 ~]# docker inspect 71b3d459c0a5
"Networks": {
                "mynet": {
                    "IPAMConfig": null,
                    "Links": null,
                    "Aliases": [
                        "71b3d459c0a5"
                    ],
                    "NetworkID": "9e288e92b098b5e4eab77ea9a5b73303e0e5909b638f853cbe3fdedd98b6b5d1",
                    "EndpointID": "0463cafd2af900e4e9b4f813df977ec53ae1845ae51af88867736a6ebbe617a3",
                    "Gateway": "172.18.0.1",
                    "IPAddress": "172.18.0.2",
                    "IPPrefixLen": 16,
                    "IPv6Gateway": "",
                    "GlobalIPv6Address": "",
                    "GlobalIPv6PrefixLen": 0,
                    "MacAddress": "02:42:ac:12:00:02",
                    "DriverOpts": null
                }
```



### 2.3.1 跨网络互通

使用`docker network connect`来实现容器的跨网络互通，本质的原理就是一个容器多网卡。

```shell
[root@192 ~]# docker ps
CONTAINER ID   IMAGE          COMMAND                  CREATED         STATUS          PORTS                                   NAMES
71b3d459c0a5   nginx:latest   "/docker-entrypoint.…"   8 minutes ago   Up 8 minutes    0.0.0.0:8081->80/tcp, :::8081->80/tcp   mynet-nginx
03866701a6a6   nginx:latest   "/docker-entrypoint.…"   25 hours ago    Up 26 minutes   0.0.0.0:8080->80/tcp, :::8080->80/tcp   nginx05
[root@192 ~]# docker network connect mynet 03866701a6a6
[root@192 ~]# docker network inspect mynet
[
    {
        "Name": "mynet",
        "Id": "9e288e92b098b5e4eab77ea9a5b73303e0e5909b638f853cbe3fdedd98b6b5d1",
        "Created": "2021-10-04T11:07:48.998487342+08:00",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": {},
            "Config": [
                {
                    "Subnet": "172.18.0.0/16",
                    "Gateway": "172.18.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {
            "03866701a6a63617de37b7f8fdb7ced6c258f48f1dff68e0c2322cb55d6db1a2": {
                "Name": "nginx05",
                "EndpointID": "163e7951300e6387a28908697854055e8007c1a1b356bc8b1858b2525eaea4b2",
                "MacAddress": "02:42:ac:12:00:03",
                "IPv4Address": "172.18.0.3/16",
                "IPv6Address": ""
            },
            "71b3d459c0a5962854c140ef8a66f5080a493e8c5e2fcb9dc55baa1dfc5b3b99": {
                "Name": "mynet-nginx",
                "EndpointID": "0463cafd2af900e4e9b4f813df977ec53ae1845ae51af88867736a6ebbe617a3",
                "MacAddress": "02:42:ac:12:00:02",
                "IPv4Address": "172.18.0.2/16",
                "IPv6Address": ""
            }
        },
        "Options": {},
        "Labels": {}
    }
]
```



# 3 镜像分层存储结构

Docker镜像是分层的，上层镜像修改了底层资源的属性，会将所有整个修改属性后的底层资源保存。这样会导致制作出来的镜像很大。为了解决镜像下载和分发的大小问题，Docker镜像是分层存储的，其中一些特性能解决这些问题：

1、通过docker pull下载镜像的时候，对于的分层镜像如果在本地已经存在，不会重复下载。

2、通过docker import导入镜像的时候，对于本地已经存在的分层镜像，不会重复导入，docker会通过镜像内的配置文件在本地将各分层镜像组合成一个完整的镜像。

3、利用相同镜像不会重复导入的特性，导出镜像的时候，可以只保留用户dockerfile制作那几层镜像的layer.tar

2、通过docker import导入镜像的时候，对于本地已经存在的分层镜像，不会重复导入，docker会通过镜像内的配置文件在本地将各分层镜像组合成一个完整的镜像。



通过导出文件结构来分析一下镜像分层存储结构。

```shell
[root@192 ~]# tar -xf hello.tar -C hello
[root@192 ~]# tree hello
hello
├── c28b9c2faac407005d4d657e49f372fb3579a47dd4e4d87d13e29edd1c912d5c
│   ├── json
│   ├── layer.tar
│   └── VERSION
├── feb5d9fea6a5e9606aa995e879d862b825965ba48de054caab5ef356dc6b3412.json
├── manifest.json
└── repositories

[root@192 ~]# cat hello/repositories
{"drakebutterfly/hello-world":{"latest":"c28b9c2faac407005d4d657e49f372fb3579a47dd4e4d87d13e29edd1c912d5c"}}
[root@192 ~]# cat hello/manifest.json
[{"Config":"feb5d9fea6a5e9606aa995e879d862b825965ba48de054caab5ef356dc6b3412.json","RepoTags":["drakebutterfly/hello-world:latest"],"Layers":["c28b9c2faac407005d4d657e49f372fb3579a47dd4e4d87d13e29edd1c912d5c/layer.tar"]}]
[root@192 ~]# cat hello/feb5d9fea6a5e9606aa995e879d862b825965ba48de054caab5ef356dc6b3412.json
{"architecture":"amd64","config":{"Hostname":"","Domainname":"","User":"","AttachStdin":false,"AttachStdout":false,"AttachStderr":false,"Tty":false,"OpenStdin":false,"StdinOnce":false,"Env":["PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"],"Cmd":["/hello"],"Image":"sha256:b9935d4e8431fb1a7f0989304ec86b3329a99a25f5efdc7f09f3f8c41434ca6d","Volumes":null,"WorkingDir":"","Entrypoint":null,"OnBuild":null,"Labels":null},"container":"8746661ca3c2f215da94e6d3f7dfdcafaff5ec0b21c9aff6af3dc379a82fbc72","container_config":{"Hostname":"8746661ca3c2","Domainname":"","User":"","AttachStdin":false,"AttachStdout":false,"AttachStderr":false,"Tty":false,"OpenStdin":false,"StdinOnce":false,"Env":["PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"],"Cmd":["/bin/sh","-c","#(nop) ","CMD [\"/hello\"]"],"Image":"sha256:b9935d4e8431fb1a7f0989304ec86b3329a99a25f5efdc7f09f3f8c41434ca6d","Volumes":null,"WorkingDir":"","Entrypoint":null,"OnBuild":null,"Labels":{}},"created":"2021-09-23T23:47:57.442225064Z","docker_version":"20.10.7","history":[{"created":"2021-09-23T23:47:57.098990892Z","created_by":"/bin/sh -c #(nop) COPY file:50563a97010fd7ce1ceebd1fa4f4891ac3decdf428333fb2683696f4358af6c2 in / "},{"created":"2021-09-23T23:47:57.442225064Z","created_by":"/bin/sh -c #(nop)  CMD [\"/hello\"]","empty_layer":true}],"os":"linux","rootfs":{"type":"layers","diff_ids":["sha256:e07ee1baac5fae6a26f30cabfe54a36d3402f96afda318fe0a96cec4ca393359"]}}[root@192 ~]#
```

分层结构分析：

- repositories：镜像tag和分层结构
- manifest.json：镜像文件结构清单信息
- [sha256].json：镜像的详细配置信息
- xxx/layer.tar：每一层镜像的全量tar包



```shell
[root@192 ~]# docker inspect feb5d9fe | grep "   \"sha256:"
                "sha256:e07ee1baac5fae6a26f30cabfe54a36d3402f96afda318fe0a96cec4ca393359"
[root@192 ~]# sha256sum hello/c28b9c2faac407005d4d657e49f372fb3579a47dd4e4d87d13e29edd1c912d5c/layer.tar
e07ee1baac5fae6a26f30cabfe54a36d3402f96afda318fe0a96cec4ca393359  hello/c28b9c2faac407005d4d657e49f372fb3579a47dd4e4d87d13e29edd1c912d5c/layer.tar
[root@192 ~]# sha256sum hello/feb5d9fea6a5e9606aa995e879d862b825965ba48de054caab5ef356dc6b3412.json
feb5d9fea6a5e9606aa995e879d862b825965ba48de054caab5ef356dc6b3412  hello/feb5d9fea6a5e9606aa995e879d862b825965ba48de054caab5ef356dc6b3412.json
```

通过上面的实验，可以总结出一些特性：

1、镜像的ID和详细配置文件[sha256].json文件的名称是一致的，同时也是这个文件自己的sha256码

2、每一分层镜像的layer.tar包的sha256码和docker inspect查到的是一致的