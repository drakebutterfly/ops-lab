<h1 align=center>YAML语法</h1>

## 1 简介

YAML是"YAML Ain't a Markup Language"（YAML不是一种标记语言）的缩写。在开发的这种语言时，*YAML* 的意思其实是："Yet Another Markup Language"（仍是一种标记语言），但为了强调这种语言以**数据**做为中心，而不是以标记语言为重点，而用反向缩略语重命名。

YAML使用**空白字符缩进**和**键值对**的方式，能比较清晰的表示层次结构的数据结构，一般用来作为配置文件使用。



## 2 YAML基本语法

1. 大小写敏感

 2. 每一组元素，都是以键值对的形式存在，使用**冒号（:）**加**空白字符**来分开键值和内容

 3. 使用空白字符缩进来表示层级结构，不能使用Tab键

 4. 缩进的空白字符数量不重要，只要相同层级结构的元素左侧对齐即可

5. 注释由井字符（#）开始，可以出现在一行中的任何位置，且范围只有一行



## 3 YAML数据结构

YAML支持的数据结构有三种。

- 对象：键值对的集合，又称为映射（mapping）/ 哈希（hashes） / 字典（dictionary）

- 数组：一组按次序排列的值，又称为序列（sequence） / 列表（list）

- 纯量（scalars）：单个的、不可再分的值，分为字符串、整数、浮点数、布尔值、时间、日期和Null



### 3.1 YAML对象

```yaml
people:
    name: drake
    age: 10

phone: {weigth: 10, branch: iphone, isNew: true}
```

转为JSON

```json
{
  "people": {
    "name": "drake",
    "age": 10
  },
  "phone": {
    "weigth": 10,
    "branch": "iphone",
    "isNew": true
  }
}
```



### 3.2 YAML数组

```yaml
fruits:
    - apple
    - orange
    - banana

animals: [dog, cat, horse, fox]
```

转为JSON格式

```json
{
  "fruits": [
    "apple",
    "orange",
    "banana"
  ],
  "animals": [
    "dog",
    "cat",
    "horse",
    "fox"
  ]
}
```



### 3.3 YAML复合结构

```yaml
language:
    - name: yaml
      usage: config
    - name: json
      usage: datastore
    - name: xml
      usage: config
```

转为JSON格式

```json
{
  "language": [
    {
      "name": "yaml",
      "usage": "config"
    },
    {
      "name": "json",
      "usage": "datastore"
    },
    {
      "name": "xml",
      "usage": "config"
    }
  ]
}
```



### 3.4 YAML纯量

```yaml
strings:
    - hello
    - 'hello\nworld' # 会对特殊字符转义
    - "hello\nworld" # 不会对特殊字符转义

int: 12

float: 12.30

boolean:
    - false
    - true

null:
    parent: null
    children: ~   # 使用~表示null

time: 2018-02-17T15:02:31+08:00 #时间使用ISO 8601格式，时间和日期之间使用T连接，最后使用+代表时区

date: 2018-02-17  #日期必须使用ISO 8601格式，即yyyy-MM-dd
```

转为JSON

```json
{
  "strings": [
    "hello",
    "hello\\nworld",
    "hello\nworld"
  ],
  "int": 12,
  "float": 12.3,
  "boolean": [
    false,
    true
  ],
  "null": {
    "parent": null,
    "children": null
  },
  "time": "2018-02-17T07:02:31.000Z",
  "date": "2018-02-17T00:00:00.000Z"
}
```



### 3.5 区块字符串

两种语法可以书写多行文字（multi-line strings），一种为保留换行（使用`|`字符），另一种为折叠换行（使用`>`字符）。两种语法在其特殊字符之后都必须接着换行

```yaml
data: |
    hello world
    welcome to china
       suprise
   
data2: >
   hello
   world
```

转为JSON

```json
{
  "data": "hello world\nwelcome to china\n   suprise\n",
  "data2": "hello world\n"
}
```



`+`表示保留文字块末尾的换行，`-`表示删除字符串末尾的换行。

```yaml
data1: |
    hello world


data2: |+
    hello world
    
    
data3: |-
    hello world
    
    
```

转为JSON

```json
{
  "data1": "hello world\n",
  "data2": "hello world\n\n\n",
  "data3": "hello world"
}
```



## 4 参考

[1] [YAML wiki](https://zh.wikipedia.org/wiki/YAML)

[2] [YAML 语言教程](https://www.ruanyifeng.com/blog/2016/07/yaml.html)

[3] [JSON转YAML](https://oktools.net/json2yaml)