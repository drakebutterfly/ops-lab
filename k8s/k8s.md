Pob

容器共享

同一个Pod中的多个容器共享Pod级别的存储卷Volume。

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: pod-volume
spec:
  containers:
  - name: nginx
    image: nginx:latest
    ports:
    - containerPort: 80
    volumeMounts:
    - name: nginx-conf
      mountPath: /etc/nginx
  - name: confreader
    image: centos:latest
    command: ["sh", "-c", "cat /opt/logs/nginx.conf"]
    volumeMounts:
    - name: nginx-conf
      mountPath: /opt/logs
  volumes:
  - name: nginx-conf
    emptyDir: {}
```



配置管理





健康检查





调度





升级与回滚



扩容与缩容





Service