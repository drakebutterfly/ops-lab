<h2 align=center>Linux常用命令</h2>

### 1 命令的提示符

```shell
[root@localhost ~]#
```

- []：分割符，无特殊含义
- root：当前登录的用户
- @：分割符，无特殊含义
- localhost：主机名（简写）
- ~：当前用户所在的目录（简写）
- #：命令提示符。超级用户
- $：命令提示符。普通用户



### 2 命令的基本格式

```shell
[root@localhost ~]# 命令 [选项] [参数]
```



### 3 目录操作命令

#### 3.1 ls命令

```shell
ls -li

ls -lh

ls -al
```



#### 3.2 cd命令

```shell
cd ~ # 进入家目录
cd - # 进入上次所在目录
cd . # 当前目录
cd .. # 上级目录
```



#### 3.3 mkdir命令

创建目录

```shell
mkdir -p # 递归创建
```



#### 3.4 rmdir命令

只能删除空白目录，基本不用，一般用`rm -r`代替



### 4 文件操作命令

#### 4.1 touch命令

创建空文件或修改文件时间



#### 4.2 stat命令

显示文件或文件系统的详细信息，包括权限、最近访问时间（access time）、最近更改时间（modify time）、最近改动时间（change time）

| 简名  | 全名        | 中文     | 作用                                                       |
| ----- | ----------- | -------- | ---------------------------------------------------------- |
| atime | Access Time | 访问时间 | 最后一次访问文件（读取或执行）的时间                       |
| ctime | Change Time | 变化时间 | 最后一次改变文件（属性或权限）或者目录（属性或权限）的时间 |
| mtime | Modify Time | 修改时间 | 最后一次修改文件（内容）或者目录（内容）的时间             |



#### 4.3 cat命令

用来查看文件内容

```shell
-A # 相当于-vET选项的整合，用于列出所有隐藏符号
-E # 把每行结尾的回车符$显示出来
-T # 把Tab键用^I显示出来
-n # 显示行号
-v # 列出特殊字符
```



#### 4.4 more命令

分屏显示文件，命令会打开一个交互界面，常用交互命令如下：

- 空格键：向下翻页
- b：向上翻页
- 回车键：向下滚动一行
- /字符串：搜索指定的字符串
- q：退出



#### 4.5 less命令

分行显示文件，通过上下键一行行移动查看。



#### 4.6 head命令

显示文件头的内容



#### 4.7 tail命令

显示文件尾的内容

```shell
-n N # 显示后N行
-f # 监听文件新增内容，按ctrl+c强制退出
```



#### 4.8 ln命令

在文件之间建立链接



硬链接

```shell
ln bcd /tmp/bcd_hard # 硬链接，inode号相同，引用计数增加
```

硬链接标记不清，不建议使用，不能链接目录，不能跨分区链接。



软链接

一定要写绝对路径

```shell
ln -s /root/bcd /tmp/bcd_soft # 软链接，inode号不相同，引用计数不变
```

删除源文件，软链接不能使用。软链接标记清晰，建议使用，能链接目录，能跨区链接。



### 5 目录和文件命令

#### 5.1 rm命令

```shell
rm -rf 文件或目录 # 递归强制删除，无法恢复
```



#### 5.2 cp命令

```shell
# 复制文件
cp def /tmp/

# 改名复制
cp def /tmp/n_def

# 复制目录
cp -r dir /tmp/

# 被复制的目录和复制后的目录的所有属性完全一致
cp -a dir /tmp/
```



#### 5.3 mv命令

移动/重命名

```shell
mv file /tmp/ # 移动file到/tmp/目录下

mv file_old file_new # 将file_old重命名为file_new
```



### 6 用户和权限管理

#### 6.1 用户管理

##### 6.1.1 用户相关文件

（1）/etc/passwd文件

```shell
[root@localhost ~]# cat /etc/passwd
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
```

第一列：用户名。

第二列：用户密码标识。

第三列：用户ID。0代表超级用户；1-499代表系统用户，预留给系统用户的；500-65535代表普通用户。

第四列：组ID。如果添加用户时，不指定用户所属组，会建立和用户名相同的组。

第五列：用户说明。

第六列：用户家目录。对于不可登录的用户，家目录没有实际的意义。

第七列：登录shell。

> Linux的所有超级用户的UID都为0



（2）/etc/shadow文件

```shell
[root@localhost ~]# cat /etc/shadow
root:$6$QdnABUsozt2DS641$VvBUSdVv42Gjec7/iOGbsZDxIwCPyd/::0:99999:7:::
bin:*:17110:0:99999:7:::
```

第一列：用户名

第二列：加密密码。可以在密码前认为加入一个特殊符号如`!`来让密码暂时失效，使得这个用户无法登录。所有伪用户的密码都是`!!`或`*`代表没有密码是不能登录。当然，如果我们新建的用户不设置密码，它的密码项也是`!!`，代表这个用户也不能登录。

第三列：密码最近修改的时间，天级别。通过`echo $(($(date --date="2022/07/15" +%s)/86400+1))`可以将日期换算成日期时间戳。同理，也可以通过`date -d "1970-01-01 19188 days"`来将时间戳转换成日期。

第四列：两次密码的修改间隔时间。

第五列：密码的有效时间。

第六列：密码到期前的警告天数。

第七列：密码到期后的宽限天数。默认值是-1，代表过期也不会影响登录。

第八列：密码失效时间。

第九列：保留。



（3）/etc/group文件

```shell
[root@localhost ~]# cat /etc/group
root:x:0:
bin:x:1:
```

第一列：组名

第二列：组密码位。对应的实际密码，配置在/etc/gshadow文件中。

第三列：组ID。

第四列：此组中支持的其他用户。初始组，和用户名相同的组；附加组，每个用户可以属于多个附加组，一般用附加组来管理一群用户的权限。



（4）/etc/gshadow



（5）用户家目录，如/home/user1，/root



（6）用户邮箱目录，在/var/spool/mail/user1



##### 6.1.2 添加用户

```shell
[root@localhost ~]# useradd [options] username
Options:
  -c, --comment COMMENT         GECOS field of the new account
  -d, --home-dir HOME_DIR       home directory of the new account
  -g, --gid GROUP               name or ID of the primary group of the new account
  -G, --groups GROUPS           list of supplementary groups of the new
  -s, --shell SHELL             login shell of the new account
  -u, --uid UID                 user ID of the new account

# Example
[root@localhost ~]# useradd -u 2009 -c "test user" -d /home/test -s /bin/bash test
```

添加用户时参考的默认值文件主要有两个，分别是`/etc/default/useradd`和`/etc/login.defs`。一般情况下，直接使用`useradd username`直接创建用户，其他的选项按默认值来即可。



##### 6.1.3 设置密码

```shell
# 普通用户直接输入passwd，并先后输入旧密码和新密码
[root@localhost ~]# passwd


[root@localhost ~]# passwd [OPTION] username
OPTION:
	-l         锁定用户。仅root用户可用
	-u         解锁用户。仅root用户可用
	--stdin    可以通过管道符输出的数据作为用户的密码

# 一行命令修改密码，注意使用history -c清空历史命令记录
[root@localhost ~]# echo "newpasswd" | passwd --stdin username
[root@localhost ~]# (echo "newpasswd"; sleep 1; echo "newpasswd") | passwd username

# 通过命令，将密码修改时间归零（shadow第3字段），强制用户登录就要修改密码
[root@localhost ~]# chage -d 0 user1
```



##### 6.1.4 修改用户信息

usermod命令修改已经添加的用户信息。

```shell
[root@localhost ~]# usermod [OPTION] username
OPTION:
	-G 组名:    修改用户附加组，其实就是把用户加入其他用户组    
```

一般情况就-G比较常用，其它的查找使用即可。



##### 6.1.5 删除用户

```shell
[root@localhost ~]# userdel [OPTION] username
OPTION:
	-r:    在删除用户的同时删除用户的家目录
```



##### 6.1.6 切换用户身份

```shell
[root@localhost ~]# su [OPTION] username
OPTION:
	-:           代表连带用户的环境变量一起切换
	-c 命令:      仅执行一次命令，而不切换用户身份
```

`-`不能省略，它代表切换用户身份时，用户的环境变量也要切换成新用户的环境变量。



##### 6.1.7 组管理命令

```shell
# 创建组
[root@localhost ~]# groupadd tg

# 删除组
[root@localhost ~]# groupdel tg

# 将用户加入组或者删除
[root@localhost ~]# gpasswd [选项] 组名
选项：
	-a 用户名:		把用户加入附加组
	-d 用户名:     把用户从附加组中删除
[root@localhost ~]# gpasswd -a user1 group1
[root@localhost ~]# gpasswd -d user1 group1
```



#### 6.2 权限管理

文件权限

```shell
-rwxrwxrwx
```

目录权限

```shell
drwxrwxrwx
```

权限含义解析：r :  4，w :  2，x :  1

读、写、执行权限对文件和目录的作用是不同的。

| 权限 | 对文件的作用                                                 | 对目录的作用                                                 |
| ---- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 读   | 文件中的数据可读。                                           | 代表可以查看目录下的内容，也就是可以查看目录下有哪些子文件和子目录。 |
| 写   | 可以修改文件中的数据，但不能删除文件本身。**如果想要删除文件，需要对文件的上级目录拥有写权限**。 | 可以修改目录下的数据，也就是可以在目录中新建、删除、复制、剪切子文件或子目录。**写权限是目录最高权限**。 |
| 执行 | 对文件拥有执行权限。**执行权限算是文件的最高权限**。         | 代表可以进入目录。                                           |



##### 6.2.1 chmod命令

```shell
[root@localhost ~]# chmod [OPTION] MODE FILE
OPTION:
	-R, --recursive        change files and directories recursively

# Example
# 1 给目录test设置权限755
[root@localhost ~]# chmod 755 test
# 2 递归地将目录test及其以下的所有文件和目录设置权限750
[root@localhost ~]# chmod -R 750 test
# 3 将coreutils文件去除所属用户执行权限，并给其他用户添加读权限
[root@localhost ~]# chmod u-x,o+r coreutils-8.22-18.el7.x86_64.rpm
# 4 去除world文件的所有执行权限
[root@localhost ~]# chmod a-x world.bz2
```

用户模式除了755这种数字模式，比较常见到的还有`[augo][+-=][rwx]`这种模式，其中u代表所属用户、g代表所属组、o代表其他用户、a代表所有。



##### 6.2.2 chown命令

```shell
[root@localhost ~]# chown [OPTION] [OWNER][:[GROUP]] FILE
OPTION:
	-R, --recursive        change files and directories recursively

# Example
[root@localhost ~]# chmod user1:user1 test
```



##### 6.2.3 chgrp命令

修改文件或目录的所属组。

```sh
[root@localhost ~]# chgrp [OPTION] GROUP FILE
OPTION:
	-R, --recursive        change files and directories recursively

# Example
[root@localhost ~]# chgrp user1 test2
[root@localhost ~]# ll
-rwxr-x---. 1 root  user1   0 Jul 12 22:46 test2
```



##### 6.2.4 umask默认权限

umask默认值为022，umask默认权限需要使用二进制进行**逻辑与**和**逻辑非**联合运算，得到正确的新建文件和目录的默认权限。

- 文件默认权限的最大666，而umask是022

  "-rw-rw-rw-"减去"----w--w-"等于"-rw-r--r--"

- 文件默认权限的最大666，而umask是033

  "-rw-rw-rw-"减去"----wx-wx"等于"-rw-r--r--"

- 目录默认权限最大可以是777，而umask是022

  "drwxrwxrwx"减去"----w--w-"等于"drwxr-xr-x"

```shell
# 临时修改
umask 033

# 永久修改
修改/etc/profile文件的umask值
```



#### 6.4 ACL 权限

当简单的所属主和所属组权限管理没办法满足权限管理的要求，需要使用ACL。

##### 6.4.1 ACL权限开启

```shell
# 1 查询ACL是否开启，此方式只适用于ext文件系统，xfs默认开启acl
[root@localhost ~]# dumpe2fs -h /dev/sda1 | grep "Default mount options:"
Default mount options:                 user_xattr    acl

# 2 如果没有开启，可以手工开启分区的ACL权限，临时开启
[root@localhost ~]# mount -o remount,acl /
# 也可以通过修改/etc/fstab文件，永久开启ACL权限
[root@localhost ~]# vim /etc/fstab
UUID=e1ef45bd-eea0-4359-93ea-b722c5c05eee  /  ext4  defaults,acl   0 0
[root@localhost ~]# mount -o remount /
```



##### 6.4.2 ACL基本命令

（1）添加和查询

```shell
[root@localhost ~]# setfacl  选项  文件名
[root@localhost ~]# getfacl  文件名

#Example
[root@localhost ~]# setfacl -m u:用户名:权限 文件名
[root@localhost ~]# setfacl -m g:组名:权限 文件名

[root@localhost ~]# setfacl -m u:user1:rwx test/
[root@localhost ~]# setfacl -m u:user1:5 archive.tar.gz
[root@localhost ~]# ll
-rwxr-x---+ 1 root  root  123 Jul 12 23:08 archive.tar.gz
[root@localhost ~]# getfacl archive.tar.gz
# file: archive.tar.gz
# owner: root
# group: root
user::rwx
user:user1:r-x
group::r-x
mask::r-x
other::---

# 递归，并对已经存在的文件生效
[root@localhost ~]# setfacl -m u:user1:rx -R test/
# 递归，并对以后新建的文件生效
[root@localhost ~]# setfacl -m d:u:user1:rx -R test/
```



（2）删除

```shell
# 删除文件的所有ACL权限
[root@localhost ~]# setfacl -b test/

# 删除指定用户user1的ACL权限
[root@localhost ~]# setfacl -x u:user1 test/
```



#### 6.5  命令权限

一般情况下，不太会关注命令的执行权限问题。在用户和权限管理的时候，会发现有一些命令只有超级用户可以执行。同时，也知道在`/sbin`或者`/usr/sbin`目录下的命令就只有超级用户执行，但是直接去目录下查不太方便。以下命令可以辅助判断：

```shell
# 命令所在路径和帮助文档
[root@localhost test]# whereis useradd
useradd: /usr/sbin/useradd /usr/share/man/man8/useradd.8.gz

# 命令全路径
[root@localhost test]# which useradd
/usr/sbin/useradd

# 命令简介，并标明命令的数字，8代表只有超级用户有权限
[root@localhost test]# whatis useradd
useradd (8)          - create a new user or update default new user information
```

通过`man man`命令，可以查看对应数字的含义：

```shell
1   Executable programs or shell commands
2   System calls (functions provided by the kernel)
3   Library calls (functions within program libraries)
4   Special files (usually found in /dev)
5   File formats and conventions eg /etc/passwd
6   Games
7   Miscellaneous  (including  macro packages and conventions), e.g. man(7),groff(7)
8   System administration commands (usually only for root)
9   Kernel routines [Non standard]
```



##### 6.5.1 sudo授权

如果普通用户想要执行超级用户才能使用的命令，需要使用sudo授权，对应的配置文件为`/etc/sudoers`。

```shell
[root@localhost ~]# cat /etc/sudoers | grep -v "^#"
root    ALL=(ALL)       ALL
#用户名  被管理主机的地址=(可使用的身份)  授权命令(绝对路径)

%wheel  ALL=(ALL)       ALL
#%组名  被管理主机的地址=(可使用的身份)  授权命令(绝对路径)

# Example1
[root@localhost ~]# visodu
user1 ALL=/usr/sbin/useradd
user1 ALL=/usr/bin/passwd [A-Za-z]*, !/usr/bin/passwd "", !/usr/bin/passwd root
[root@localhost ~]# su - user1
[user1@localhost ~]# sudo /usr/sbin/useradd user2

# 输入密码后，可以查看当前user1用户具备哪一些超级用户命令的使用权限
[user1@localhost ~]# sudo -l
....
User user1 may run the following commands on this host:
    (root) /usr/sbin/useradd
```



#### 6.6 文件特殊权限

SetUID： 只针对执行文件

SetGID： 既可以针对执行文件，也可以针对目录

Sticky BIT： 只针对目录

##### 6.6.1 SetUID

（1）SUID的功能要求如下：

- 只有可执行的**二进制程序**才能设置SUID权限
- 命令执行者需要对**该程序**拥有x（执行）权限

（2）SUID的执行原理：

- 命令执行者在执行**该程序**时获得该程序文件属主的身份
- SUID权限只在该程序执行过程中有效

（3）样例

```shell
# Example
[root@localhost ~]# ll /etc/shadow
----------. 1 root root  739 Jul 23 18:39 /etc/shadow
```

我们会发现普通用户是没有权限去修改shadow文件的，但是实际上普通用户是可以通过`/usr/bin/passwd`命令来修改自己的密码的。

```shell
[root@localhost ~]# ll /usr/bin/passwd
-rwsr-xr-x. 1 root root 27832 Jun 10  2014 /usr/bin/passwd
```

对比SUID的功能要求，发现passwd命令所属主是root超级用户，它是一个拥有x权限的可执行的文件。此外，其他用户拥有x权限。在这两个前提下，普通用户在执行passwd命令的时候，会使用所属主root的权限来执行。

（4）建议

- SUID权限很危险，不要随意给命令赋予SUID权限，一般使用系统自带的即可
- 对系统默认应该具有SUID权限的文件做一列表，定时检查有没有除此之外的文件被设置了SUID权限。命令`find / -perm -4000 -o -perm -2000 > /tmp/setuid.check`。



##### 6.6.2 SetGID

（1）针对文件的作用

针对文件的时候，SGID的作用和SUID类似，只是所拥有的权限从所属主变成所属组。SGID的含义如下：

- 只有可执行的文件可以设置SGID权限
- 命令执行者要对该文件拥有x权限
- 命令执行者在执行该文件的时候，组身份升级为该文件的所属组
- SGID权限只有在该程序执行过程中有效

```shell
# Example
[root@localhost ~]# ll /var/lib/mlocate/mlocate.db
-rw-r-----. 1 root slocate 902241 Jul 24 12:49 /var/lib/mlocate/mlocate.db
[root@localhost ~]# ll /usr/bin/locate
-rwx--s--x. 1 root slocate 40520 Apr 11  2018 /usr/bin/locate
```

`/var/lib/mlocate/mlocate.db`普通用户是没有查找权限的，但是普通用户可以使用locate命令来查找文件，本质的原因是locate的命令有SGID权限。这样普通用户在执行locate命令的时候，会暂时拥有slocate所属组的r权限。



（2）针对目录的作用

针对目录的时候，含义如下：

- 普通用户必须对此目录拥有r和x权限，才能进入此目录
- 普通用户在此目录中的有效组会变成此目录的属组
- 若普通用户对此目录拥有w权限是，**新建的文件的默认所属组是这个目录的属组**

```shell
[root@localhost tmp]# mkdir testguid
[root@localhost tmp]# chmod 777 testguid/
[root@localhost tmp]# chmod g+s testguid/
[root@localhost tmp]# su - user1
[user1@localhost ~]$ cd /tmp/testguid/
[user1@localhost testguid]$ ls
[user1@localhost testguid]$ touch test
[user1@localhost testguid]$ ll
total 0
-rw-rw-r--. 1 user1 root 0 Jul 24 13:30 test
```

如果想要目录的SGID起作用，需要有777权限。



##### 6.6.3 Sticky BIT

粘着位，简称SBIT。SBIT仅对目录有效，它的作用如下：

- 普通用户对该目录拥有w和x权限
- 如果没有SBIT权限，因为普通用户拥有目录w权限，即普通用户可以删除此目录下的所有文件，包括其他用户建立的文件。一旦赋予了SBIT权限，除了root用户可以删除所有文件，普通用户就只能删除自己建立的权限。

```shell
[root@localhost ~]# ll / | grep tmp
drwxrwxrwt.  14 root root 4096 Jul 24 13:30 tmp
```



##### 6.6.4 设置文件特殊权限

特殊权限表示方式如下：

- 4代表SUID
- 2代表SGID
- 1代表SBIT

```shell
[root@localhost ~]# chmod 4755 ftest
[root@localhost ~]# chmod 2777 dtest
[root@localhost ~]# chmod 1777 dtmp
```



#### 6.7 文件系统属性chattr权限

（1）设置属性

```shell
[root@localhost ~]# chattr [+-=] [选项] 文件或目录
选项：
	+：    增加权限
	-：    删除权限
	=：    等于某权限
	i：    如果对文件设置i属性，那么不允许对文件进行删除、改名，也不能添加和修改数据；		  如果对目录设置i属性，那么只能修改目录下文件的数据，但不允许建立和删除文件。
	a：    如果对文件设置a属性，那么只能在文件中增加数据，但是不能删除也不能修改数据；
	      如果对目录设置a属性，那么只允许在目录中建立和修改文件，但不允许删除。
	e：   Linux中绝大多数的文件都默认拥有e属性。表示该文件是使用ext文件系统进行存储             的，而且不能使用“chattr e”命令取消e属性。

# Example
[root@localhost ~]# chattr +i test
[root@localhost ~]# chattr +a test  # 只能通过echo追加
```

（2）查看文件系统属性

```shell
[root@localhost ~]# lsattr [选项] 文件或目录
选项：
	-a    显示所有文件和目录
	-d    若目标是目录，仅列出目录本身的属性，而不是子文件的

# Example
[root@localhost ~]# lsattr test
----i--------e- test
[root@localhost ~]# lsattr -d dirtest
----i--------e- dirtest
```





### 7 帮助命令

#### 7.1 man命令

```shell
[root@localhost ~]# man [选项] 命令
-f : 查看命令拥有那个级别的帮助
-k : 查看和命令相关的所有帮助  # 一般没啥意义
```

`/`从上往下搜索

`?`从下往上搜索



#### 7.2 info命令

一整套资料，每个单独命令的帮助信息只是这套完整资料中的某一个小章节。太全了，不常用。



#### 7.3 help命令

help只能获取Shell内置命令的帮助。可以通过type命令来区分内置命令和外置命令。

```shell
[root@localhost ~]# type cd 
cd is a shell builtin
[root@localhost ~]# help cd
cd: cd [-L|[-P [-e]]] [dir]
....
[root@localhost ~]# cd --help
-bash: cd: --: invalid option
cd: usage: cd [-L|[-P [-e]]] [dir]
```



#### 7.4 --help命令

绝大多数命令都可以此来查看帮助，输出的帮助信息基本上是man命令的信息简要版。



### 8 搜索命令

#### 8.1 whereis命令

搜索系统命令，不能搜索普通文件，只能搜索系统命令。能查找二进制命令、源文件和帮助文档。

```shell
[root@localhost ~]# whereis ls
ls: /usr/bin/ls /usr/share/man/man1/ls.1.gz
```



#### 8.2 which命令

列出命令所在的路径，如果命令有别名，会同时列出别名。

```shell
[root@localhost ~]# which ls
alias ls='ls --color=auto'
        /usr/bin/ls
```



#### 8.3 locate命令

**centos7无此命令，一般使用find命令**。可以按照文件名搜索普通文件。对应的配置文件为`/etc/updatedb.conf`

- 优点：按照数据库搜索，搜索速度快，消耗资源小，数据库位置`/var/lib/mlocate/mlocate.db`
- 缺点：只能按照文件名来搜索文件，而不能执行更复杂的搜索，比如按照权限、大小等。数据库不是实时更新的，新建的文件可能会找不到，需要`updatedb`更新数据库先。



#### 8.4 find命令

具备按照文件/目录名称、大小、时间、权限进行搜索。可以通过`-type f`和`-type d`来筛选文件和目录。

find命令非常强大，具体使用的时候，可以通过`man find`来查找使用详情，可以通过选项搜索过滤，如搜索`-size`。

##### 8.4.1 按文件名搜索

```shell
[root@localhost ~]# find 搜索路径 [选项] 名称
选项：
	-name:   按照文件名搜索
	-iname:  按照文件名搜索，不区分文件名大小写
	-inum:   按照inode号搜索，可以搜索出来所有硬连接
```



##### 8.4.2 按照文件大小搜索

```shell
[root@localhost ~]# find 搜索路径 [选项] 名称
选项：
	-size [+|-]:   按照文件大小搜索

[root@localhost ~]# find . -iname hello -size +10
./hello-world/hello
[root@localhost ~]# find . -iname hello
./hello-world/hello
./hello
./HELLO
[root@localhost ~]# find . -iname hello -size -10
```



##### 8.4.3 按修改时间搜索

Linux文件由访问时间、数据修改时间、状态修改时间这三个时间，可以按照时间来搜索文件。

```shell
[root@localhost ~]# find 搜索路径 [选项] 名称
选项：
	-atime [+|-]:   按照文件访问时间搜索
	-mtime [+|-]:   按照文件数据修改时间搜索
	-ctime [+|-]:   按照文件状态修改时间搜索
```

`[+|-]`时间的含义：

- -5：代表5天内修改
- 5：代表前5~6天那一天修改的文件
- +5：代表6天前修改的文件



##### 8.4.4 按权限搜索

```shell
[root@localhost ~]# find 搜索路径 [选项] 名称
选项：
	-perm  权限模式:   文件权限刚好等于“权限模式”
	-perm -权限模式:   文件权限全部（属主/群组/其他）包含“权限模式”
	-perm /权限模式:   文件权限的属主、所属组、其他任意一个包含“权限模式”

[root@localhost hello-world]# ll -a
total 24
drwxr-xr-x. 2 root root    52 Jul  9 18:22 .
dr-xr-x---. 6 root root  4096 Jul  9 11:37 ..
-rw-r--r--. 1 root root    41 Oct  3  2021 Dockerfile
-rwxr-xr-x. 1 root root 13256 Oct  3  2021 hello
-rw-------. 1 root root     0 Jul  9 18:22 test600
[root@localhost hello-world]# find . -perm 644
./Dockerfile
[root@localhost hello-world]# find . -perm /644
.
./hello
./Dockerfile
./test600
[root@localhost hello-world]# find . -perm -744
.
./hello
```



##### 8.4.5 按所有者和所属组搜索

```shell
[root@localhost ~]# find 搜索路径 [选项] 名称
选项：
	-uid    用户ID： 按照用户ID查找所有者是指定用户的文件
	-gid    组ID：  按照用户组ID查找所有者是指定用户组的文件
	-user   用户名： 按照用户名查找所有者是指定用户的文件
	-group  组名：   按照组名查找所属组是指定用户组的文件
	-nouser         查找没有所有者的文件
```

`-nouser`选项比较常用，主要用于查找垃圾文件。



##### 8.4.6 按照文件类型搜索

```shell
[root@localhost ~]# find 搜索路径 [选项] 名称
选项：
	-type d:   查找目录
	-type f:   查找普通文件
	-type l:   查找软链接文件
```



##### 8.4.7 逻辑运算符

```shell
[root@localhost ~]# find 搜索路径 [选项] 名称
选项：
	-a:     and逻辑与
	-o:     or逻辑或
	-not:   not逻辑非

[root@localhost hello-world]# ll
total 20
-rw-r--r--. 1 root root    41 Oct  3  2021 Dockerfile
-rwxr-xr-x. 1 root root 13256 Oct  3  2021 hello
-rw-------. 1 root root     0 Jul  9 18:22 test600
[root@localhost hello-world]# find . -perm /644
.
./hello
./Dockerfile
./test600
[root@localhost hello-world]# find . -perm /644 -a -type f
./hello
./Dockerfile
./test600
[root@localhost hello-world]# find . -not -name hello
.
./Dockerfile
./test600
```

 

##### 8.4.8 其他选项

```shell
[root@localhost ~]# find 搜索路径 [选项] 名称 -exec 命令1 {} \;
```

将find命令的结果交给-exec调用命令1来处理。`{} \;`是固定格式，代表find命令的查找结果。如果需要删除之前执行询问，可以用`-ok`代替`-exec`即可。



#### 8.5 grep命令

在文件中提取和匹配符合条件的字符串行。

```shell
[root@localhost ~]# grep [选项] "搜索内容" 文件名称
选项：
	-i:   忽略大小写
	-n:   显示行号
	-v:   反向查找
	--color=auto： 搜索出的关键字用颜色显示
```



### 9 管道符

将**命令1**正确执行的结果**文本流**，作为**命令2**的输入。支持多个管道符场景。

```shell
[root@localhost ~]# 命令1 | 命令2
```



### 10 别名和快捷键

#### 10.1 命令的别名

命令的别名，主要用于照顾管理员的使用习惯，或者将一些常用的长命令简化。

```shell
# 查看命令别名
[root@localhost ~]# alias
alias cp='cp -i'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias grep='grep --color=auto'
alias l.='ls -d .* --color=auto'
alias ll='ls -l --color=auto'
alias ls='ls --color=auto'
alias mv='mv -i'
alias rm='rm -i'
alias which='alias | /usr/bin/which --tty-only --read-alias --show-dot --show-tilde'
[root@localhost ~]# alias 别名='原命令'
```

用alias设置的命令别名，是临时生效的，如果需要长期生效，需要写入环境变量配置文件`~/.bashrc`，仅对某个用户生效。



#### 10.2 常用快捷键

| 快捷键 | 作用                     |
| ------ | ------------------------ |
| Tab键  | 命令或文件补全           |
| ctrl+A | 将光标移动到命令行开头   |
| ctrl+E | 将光标移动到命令行结尾   |
| ctrl+C | 强制终止当前命令         |
| ctrl+L | 清屏，相当于clear命令    |
| ctrl+U | 删除或剪切光标之前的命令 |
| ctrl+Y | 粘贴ctrl+U剪切的内容     |



### 11 压缩与解压缩命令

Linux中常见的压缩格式有，`.zip`、`.gz`、`.bz2`、`.tar`、`.tar.gz`和`.tar.bz2`等。

#### 11.1 zip格式

```shell
[root@localhost ~]# zip [选项] 压缩包名.zip 源文件或源目录
选项：
	-r: 递归压缩目录
	-q: 安静操作，没有过程回显
[root@localhost ~]# zip -r test.zip test/
  adding: test/ (stored 0%)
  adding: test/hello/ (stored 0%)

[root@localhost ~]# unzip [选项] 压缩包名.zip
选项：
	-d: 解压到指定目录
	-q: 安静操作，没有过程回显
[root@localhost ~]# unzip hello.zip -d test/
Archive:  hello.zip
   creating: test/hello/
```



#### 11.2 gz格式

只用于文件，对目录不会打包，就算使用`-r`选项，也是把目录内部的每个文件压缩

```shell
[root@localhost ~]# gzip [OPTION]... [FILE]...
OPTION:
  -c, --stdout       write on standard output, keep original files                            unchanged
  -d, --decompress   decompress
  -q, --quiet        suppress all warnings
  -r, --recursive    operate recursively on directories

[root@localhost test]# ls
hello  welcome  world
[root@localhost test]# gzip world
[root@localhost test]# gzip -c welcome > welcome.gz
[root@localhost test]# ls
hello  welcome  welcome.gz  world.gz
[root@localhost test]# gzip -d world.gz
[root@localhost test]# ls
hello  welcome  welcome.gz  world
[root@localhost hello]# cd ..
[root@localhost test]# gzip -r hello/
[root@localhost test]# ls
hello  welcome  welcome.gz  world
[root@localhost test]# ls hello/
test2.gz  test.gz
```



#### 11.3 bz2格式

功能和gzip类似，但不支持压缩目录。

```shell
[root@localhost ~]# bzip2 [OPTION]... [FILE]...
OPTION:
   -d --decompress     force decompression
   -c --stdout         output to standard out
   -k --keep           keep (don't delete) input files
   -q --quiet          suppress noncritical error messages
   -v --verbose        be verbose (a 2nd -v gives more)
```



#### 11.4 tar格式

打包，不压缩。需要配合`-z`或者`-j`选项来打包后，分别压缩成gz和bz2格式。

```shell
[root@localhost ~]# tar [OPTION...] [FILE]...
OPTION:
  -c, --create               create a new archive
  -v, --verbose              verbosely list files processed
  -x, --extract, --get       extract files from an archive
  -f, --file=ARCHIVE         use archive file or device ARCHIVE
  -t, --list                 list the contents of an archive
  -z, --gzip, --gunzip, --ungzip   filter the archive through gzip
  -j, --bzip2                filter the archive through bzip2
  -C, --directory=DIR        change to directory DIR
Examples:
  tar -cf archive.tar foo bar  # Create archive.tar from files foo and bar.
  tar -tvf archive.tar         # List all files in archive.tar verbosely.
  tar -xf archive.tar          # Extract all files from archive.tar.

[root@localhost ~]# tar -zcvf archive.tar.gz foo bar
[root@localhost ~]# tar -zxvf archive.tar.gz
[root@localhost ~]# tar -zxvf archive.tar.gz -C tmp

[root@localhost ~]# tar -jcvf archive.tar.bz2 foo bar
[root@localhost ~]# tar -jxvf archive.tar.bz2
[root@localhost ~]# tar -jxvf archive.tar.bz2 -C tmp
```



### 12 关机和重启命令

#### 12.1 sync数据同步

刷新数据到磁盘。



#### 12.2 shotdown命令

```shell
[root@localhost ~]# shutdown [OPTIONS...] [TIME] [WALL...]
OPTION:
  -r --reboot    Reboot the machine
  -h             Equivalent to --poweroff, overridden by --halt
[root@localhost ~]# shutdown -h now
```



### 13 网络命令

#### 13.1 配置IP地址

修改`/etc/sysconfig/network-scripts/ifcfg-*`网卡配置文件。一台计算机可以配置多个网卡。

```shell
echo "\
DEVICE=ens33
TYPE=Ethernet
BOOTPROTO=static
IPADDR=192.168.190.120
NETMASK=255.255.255.0
GATEWAY=192.168.190.2
DNS1=192.168.190.2
DEFROUTE=yes
ONBOOT=yes\
" > /etc/sysconfig/network-scripts/ifcfg-ens33
```



##### 13.1.1重启网络服务

```shell
# 重启单张网卡
[root@localhost ~]# ifdown eth0
[root@localhost ~]# ifup eth0

# 重启网络服务1
[root@localhost ~]# service network restart
# 重启网络服务2
[root@localhost ~]# systemctl restart network
```



##### 13.1.2 复制镜像需要重置UUID

```shell
[root@localhost ~]# vi /etc/sysconfig/network-scripts/ifcfg-ens33
# 删除MAC地址行

[root@localhost ~]# rm -rf /etc/udev/rules.d/70-persistent-net.rules
# 删除MAC地址和UUID绑定文件

[root@localhost ~]# shutdown -r now
```



#### 13.2 ifconfig命令

查看网络配置。

```shell
[root@localhost ~]# ifconfig
ens33: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.1.200  netmask 255.255.255.0  broadcast 192.168.1.255
        inet6 fe80::20c:29ff:fee8:8157  prefixlen 64  scopeid 0x20<link>
        ether 00:0c:29:e8:81:57  txqueuelen 1000  (Ethernet)
        RX packets 176  bytes 22929 (22.3 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 150  bytes 34566 (33.7 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

```



#### 13.3 ping命令

可以跟IP或者域名。

```shell
[root@localhost ~]# ping [OPTIONS...] destination
OPTION:
  -c 次数: 指定ping的次数
  -b:     后面加广播地址，用于对整个网络进行探测
```



#### 13.4 netstat命令

网络状态查看命令，既可以查看本机开启的端口，也可以查看哪些客户端连接。

```shell
[root@localhost ~]# netstat [OPTIONS...]
OPTIONS:
  -p, --programs           display PID/Program name for sockets
  -a, --all                display all sockets (default: connected)
  -n, --numeric            don't resolve names
  -t                       tcp
  -u                       udp
  -l, --listening          display listening server sockets

[root@localhost ~]# netstat -pantu
Active Internet connections (servers and established)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      1344/sshd
tcp        0      0 127.0.0.1:25            0.0.0.0:*               LISTEN      2125/master
tcp        0      0 192.168.1.200:22        192.168.1.6:60327       ESTABLISHED 2439/sshd: root@pts
tcp6       0      0 :::22                   :::*                    LISTEN      1344/sshd
tcp6       0      0 ::1:25                  :::*                    LISTEN      2125/master
[root@localhost ~]# netstat -tuln
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN
tcp        0      0 127.0.0.1:25            0.0.0.0:*               LISTEN
tcp6       0      0 :::22                   :::*                    LISTEN
tcp6       0      0 ::1:25                  :::*                    LISTEN
```



#### 13.5 write命令

给指定用户发送信息。

```shell
# 查看机器当前所有的登录用户
[root@localhost ~]# w
 22:24:39 up 38 min,  2 users,  load average: 0.10, 0.05, 0.05
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
root     pts/0    192.168.1.6      21:48    7.00s  0.13s  0.03s w
root     pts/1    192.168.1.6      22:24    6.00s  0.01s  0.01s -bash
[root@localhost ~]# write user tty
[root@localhost ~]# write root pts/1
hello world
# 向pts1登录的root用户发送消息，用CTRL+D快捷键保存并发送数据
```

登录终端分类：

- 本地字符终端    tty1-6  alt+F1-6

- 本地图形终端    tty7      ctrl+alt+F7

- 远程终端            pts/0-255



#### 13.6 wall命令

给所有登录用户发送信息，包括自己。

```shell
[root@localhost ~]# wall "hello world"
```



### 14 痕迹命令

系统中有一些重要的痕迹日志文件，如`/var/log/wtmp`，`/var/run/utmp`，`/var/log/lastlog`等日志文件，如果用vim打开，会发现是二进制码，这是系统为了避免痕迹信息的准确性，只能通过对应的命令来进行查看。涉及到的命令有：`w`，`who`，`last`，`lastlog`等。

- w：显示正在登录的用户信息，对应日志文件/var/run/utmp
- who：用于查看正在登录的用户，显示的内容更加简单，对应日志文件/var/run/utmp
- last：查看系统所有登录过的用户信息，包括正在登录和之前登录的用户，对应日志文件/var/log/wtmp
- lastlog：查看系统中所有用户最后一次的登录时间，对应日志文件/var/log/lastlog
- lastb：查看错误登录信息，对应日志文件/var/log/btmp



### 15 挂载命令

linux所有设备都必须挂载使用。移动设备，如光驱、U盘等不建议自动挂载。

```shell
[root@localhost ~]# mount

# 自动挂载/etc/fstab配置文件配置的设备
[root@localhost ~]# mount -a

[root@localhost ~]# mount [options] <devicename> <mountpoint>
options:
  -t, --types <list>      limit the set of filesystem types
```



#### 15.1 光盘挂载

不同版本的Linux系统，光盘的设备文件名并不相同：

- CentOS 5.x以前的系统，与硬盘共用文件名命名方式，光盘设备文件名是/dev/hdc
- CentOS 6.x以后的系统，光盘设备文件名是/dev/sr0

不论哪个版本，设备文件都有软链接/dev/cdrom，也可以作为光盘的设备文件名

```shell
[root@localhost ~]# mount -t iso9660 /dev/cdrom /mnt/cdrom
[root@localhost ~]# mount /dev/sr0 /mnt/cdrom  # 允许不指定文件系统格式
```

用完后需要卸载，卸载前需要退出光盘挂载的目录，才可以正常卸载。

```shell
[root@localhost ~]# umount /mnt/cdrom
```



#### 15.2 U盘挂载

U盘和硬盘共用设备文件名，所以U盘的设备文件名不是固定的，需要手工查询，查询命令：

```shell
[root@localhost ~]# fdisk -l
# 关注System类型为FAT32的
```

挂载命令如下：

```shell
[root@localhost ~]# mount -t vfat -o iocharset=utf8 /dev/sdb4 /mnt/usb/
# 挂载U盘。因为是windows分区，所以是vfat文件类型
# 如果U盘中有中文，挂载的时候需要手动指定编码，同时远程终端需要支持中文显示
```

卸载命令：

```shell
[root@localhost ~]# umount /mnt/usb/
```



#### 15.3 挂载NTFS分区

需要安装对应的驱动。



### 16. vim使用

命令模式：可以使用快捷键，使用`i`进入输入模式。

输入模式：正常的文本编辑。

末行模式：ESC从输入模式退出至命令模式，然后输入冒号，进入编辑模式。



#### 16.1 命令模式操作

（1）移动光标

```shell
gg        移动到文件头
G         移动到文件尾（shfit+g）
:n        移动到第n行
```

（2）删除或剪切

```shell
dd        删除单行
ndd       删除n行
:n1,n2d   删除指定范围的行，可以使用:set nu显示行号，使用:set nonu取消显示
```

这里的删除也是剪切。删除内容放入剪切板，如果不粘贴就是删除，如果粘贴就是剪切。粘贴方法如下：

```shell
p        粘贴到光标后
P（大）   粘贴到光标前
```

（3）复制

使用p或P进行粘贴

```shell
yy       复制单行
nyy      复制n行
```

（4）撤销

```shell
u       撤销
ctrl+r  反撤销
```



#### 16.2 末行模式

```shell
set nu       显示行号
set nonu     取消显示

# 查找
/查找内容     从光标所在行向下查找
?查找内容     从光标所在行向上查找
n           下一个
N           上一个

# 替换
1,10s/old/new/g    替换1到10行所有old为new
%s/old/new/g       替换整个文件的old为new
```



### 17 软件包管理

#### 17.1 软件包分类

软件包分两类：源码包和二进制包。

##### 17.1.1 源码包

1、优点

- 源码，可以自由选择所需功能
- 本系统编译安装，更加适合自己的系统，更加稳定和高效

2、缺点

- 编译时间长，安装时间长
- 安装过程一旦报错，新手很难解决，且报错根据本地编译环境不同而不同

##### 17.1.2 二进制包

1、二进制包分类

- DPKG包：Debian Linux系列的包管理机制。主要应用于Debian和unbuntu中。

- RPM包：Red Hat系列的包管理机制。主要应用于CentOS、SuSE等。

2、RPM特点

（1）优点

- 包管理简单，通过几个统一的系统命令即可实现包的安装、升级、查询和卸载
- 安装速度比源码包快

（2）缺点

- 不可以看到源码
- 功能选择不如源码包灵活
- 依赖性

3、RPM包依赖

（1）树形依赖：软件包间的依赖

（2）环形依赖：软件包间的依赖

（3）模块依赖：函数库依赖

4、RPM包命名规则

```shell
nginx-1.18.0-2.el8.ngx.x86_64.rpm
```

- nginx：软件包名
- 1.18.0：软件版本
- 2：软件发布次数
- el8：EL是Red Hat **E**nterprise **L**inux的缩写，适合Red Hat8.x和CentOS8.x版本
- x86_64：适合的硬件平台。一般有x86_64、i386、i586、i686和noarch。
- rpm：rpm包的扩展名



#### 17.2 RPM手工管理

包管理，包括：安装、升级、卸载、查询。使用过程中，需要注意包全名和包名的使用习惯。

- 包全名：如果操作的是未安装软件包，则使用包全名，而且需要注意绝对路径
- 包名：如果操作的是已经安装的软件包，则使用包名即可，系统会产生RPM包的数据库（/var/lib/rpm/），而且可以在任意路径下操作



##### 17.2.1 RPM安装

默认安装路径

| 路径            | 作用                       |
| --------------- | -------------------------- |
| /etc/           | 配置文件安装目录           |
| /usr/bin/       | 可执行的命令安装目录       |
| /usr/lib/       | 程序所使用的函数库保存位置 |
| /usr/share/doc/ | 基本的软件使用手册保存位置 |
| /usr/share/man/ | 帮助文件保存位置           |



需要根据依赖提示，先把软件包依赖的包逐个手动安装，再安装计划安装的软件包。

```shell
[root@localhost ~]# rpm -ivh 包全名
选项：
	-i        install安装
	-v        显示更详细的信息
	-h        打印显示安装进度
	--force   强制安装。一般用于恢复重要配置文件丢失，但是不会覆盖配置文件。
```

service httpd start|restart|stop

/etc/rc.d/init.d/httpd start|restart|stop



##### 17.2.2 RPM升级

```shell
[root@localhost ~]# rpm -Uvh 包全名
选项：
	-U（大写）       升级安装，如果没有安装过，系统直接安装。如果安装过了，则升级

[root@localhost ~]# rpm -Fvh 包全名
选项：
	-F（大写）       必须安装了旧版本，才会升级。如果没有安装过，则不会安装。
```



##### 17.2.3 RPM卸载

```shell
[root@localhost ~]# rpm -e 包名
选项：
	-e       卸载，需要按依赖的反序，逐个卸载
```



##### 17.2.4 RPM查询

本地系统查询。

（1）查询安装情况

```shell
# 查询软件包是否安装
[root@localhost ~]# rpm -q 包名
选项：
	-q       查询
# 查询本地所有已经安装的软件
[root@localhost ~]# rpm -qa

# Example
[root@localhost ~]# rpm -q python
python-2.7.5-48.el7.x86_64
```



（2）查询软件包详细信息

```shell
# 查询已经安装软件包的详细信息
[root@localhost ~]# rpm -qi 包名
# 查询没有安装软件包的详细信息
[root@localhost ~]# rpm -qip 包全名

# Example
[root@localhost ~]# rpm -qi python
Name        : python
Version     : 2.7.5
Release     : 48.el7
Architecture: x86_64
Install Date: Fri 01 Oct 2021 12:52:54 PM CST
Group       : Development/Languages
Size        : 80827
License     : Python
Signature   : RSA/SHA256, Mon 21 Nov 2016 04:14:21 AM CST, Key ID 24c6a8a7f4a80eb5
Source RPM  : python-2.7.5-48.el7.src.rpm
Build Date  : Sun 06 Nov 2016 09:03:10 AM CST
Build Host  : worker1.bsys.centos.org
Relocations : (not relocatable)
Packager    : CentOS BuildSystem <http://bugs.centos.org>
Vendor      : CentOS
URL         : http://www.python.org/
Summary     : An interpreted, interactive, object-oriented programming language
Description :
Python is an interpreted, interactive, object-oriented programming
language often compared to Tcl, Perl, Scheme or Java. Python includes
modules, classes, exceptions, very high level dynamic data types and
dynamic typing. Python supports interfaces to many system calls and
libraries, as well as to various windowing systems (X11, Motif, Tk,
Mac and MFC).

Programmers can write new built-in modules for Python in C or C++.
Python can be used as an extension language for applications that need
a programmable interface.

Note that documentation for Python is provided in the python-docs
package.

This package provides the "python" executable; most of the actual
implementation is within the "python-libs" package.
[root@localhost test]# rpm -qip coreutils-8.22-18.el7.x86_64.rpm
Name        : coreutils
Version     : 8.22
Release     : 18.el7
Architecture: x86_64
Install Date: (not installed)
Group       : System Environment/Base
Size        : 14589167
License     : GPLv3+
Signature   : RSA/SHA256, Mon 21 Nov 2016 01:26:24 AM CST, Key ID 24c6a8a7f4a80eb5
Source RPM  : coreutils-8.22-18.el7.src.rpm
Build Date  : Sun 06 Nov 2016 04:49:48 AM CST
Build Host  : worker1.bsys.centos.org
Relocations : (not relocatable)
Packager    : CentOS BuildSystem <http://bugs.centos.org>
Vendor      : CentOS
URL         : http://www.gnu.org/software/coreutils/
Summary     : A set of basic GNU tools commonly used in shell scripts
Description :
These are the GNU core utilities.  This package is the combination of
the old GNU fileutils, sh-utils, and textutils packages.
```



（3）查询软件包文件列表

```shell
# 查询已经安装软件包的文件列表
[root@localhost ~]# rpm -ql 包名
# 查询没有安装软件包的文件列表
[root@localhost ~]# rpm -qlp 包全名

# Example
[root@localhost ~]# rpm -ql python
/usr/bin/pydoc
/usr/bin/python
/usr/bin/python2
/usr/bin/python2.7
/usr/share/doc/python-2.7.5
/usr/share/doc/python-2.7.5/LICENSE
/usr/share/doc/python-2.7.5/README
/usr/share/man/man1/python.1.gz
/usr/share/man/man1/python2.1.gz
/usr/share/man/man1/python2.7.1.gz
[root@localhost test]# rpm -qlp coreutils-8.22-18.el7.x86_64.rpm
/etc/DIR_COLORS
/etc/DIR_COLORS.256color
/etc/DIR_COLORS.lightbgcolor
/etc/profile.d/colorls.csh
/etc/profile.d/colorls.sh
/usr/bin/[
/usr/bin/arch
....
```



（4）查询系统文件属于哪个RPM包

```shell
[root@localhost ~]# rpm -qf 系统文件名

# Example
[root@localhost ~]# rpm -ql /usr/bin/python
python-2.7.5-48.el7.x86_64
```



（5）查询依赖性

```shell
[root@localhost ~]# rpm -qR 包名
```



##### 17.2.5 验证

（1）对已经安装的软件验证

```shell
# 验证指定RPM包中的系统文件是否被修改
[root@localhost ~]# rpm -V 已安装的包名

# 验证某个系统文件是否被修改
[root@localhost ~]# rpm -Vf 系统文件名
```



（2）数字证书验证

验证待安装RPM包的完整性和防篡改。数字证书的使用：

- 安装原厂公钥文件
- 安装RPM包时，回去提取RPM包中的证书信息，然后和本机安装的原厂证书进行验证
- 如果验证通过，则允许安装；如果验证不通过，则不允许安装并警告

```shell
# (1)证书的位置
[root@localhost ~]# ls /etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7
/etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7

# (2)数字证书导入
[root@localhost ~]# rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7

# (3)查询安装好的证书
[root@localhost ~]# rpm -qa | grep gpg-pubkey
gpg-pubkey-621e9f35-58adea78
gpg-pubkey-f4a80eb5-53a7ff4b
```



##### 17.2.6 RPM包中文件提取

```shell
# 提取RPM包中文件到当前目录中
[root@localhost ~]# rpm2cpio 包全名 | cpio -idv .包内文件绝对路径

# Example：不小心把系统命令文件修改了，想要恢复
[root@localhost ~]# rpm -qf /bin/ls
coreutils-8.22-18.el7.x86_64
[root@localhost ~]# rpm -qlp coreutils-8.22-18.el7.x86_64.rpm
[root@localhost ~]# rpm2cpio /root/test/coreutils-8.22-18.el7.x86_64 | cpio -idv ./usr/bin/ls
[root@localhost ~]# ls ./usr/bin/ls
```

注意：如果抽取到当前目录，需要加`.`。



#### 17.3 RPM在线管理

使用yum来对RPM包进行在线的、自动的管理，如果RPM包有依赖，会自动解决。



##### 17.3.1 yum源文件解析

yum源配置文件保存在/etc/yum.repos.d/目录中，文件的扩展名一定是`*.repo`。

```shell
[root@localhost test]# ls /etc/yum.repos.d/
CentOS-Base.repo  CentOS-Debuginfo.repo  CentOS-Media.repo    CentOS-Vault.repo
CentOS-CR.repo    CentOS-fasttrack.repo  CentOS-Sources.repo  docker-ce.repo
```

默认情况下，在/etc/yum.repos.d/目录中的所有`*.repo`文件都会生效。命令格式如下：

```shell
[base]
name=CentOS-$releasever - Base
mirrorlist=http://mirrorlist.centos.org/?release=$releasever&arch=$basearch&repo=os&infra=$infra
#baseurl=http://mirror.centos.org/centos/$releasever/os/$basearch/
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7
```

以`CentOS-Base.repo`中的1个yum源容器举例，解析以下每个配置的含义。

- [base]：yum源名称，放在[]中。
- name：容器说明，可以自己随便写
- mirrorlist：镜像站点，**和baseurl互斥，只能存在其中一个**。
- baseurl：yum源服务器的地址。
- enabled：标明此yum源是否生效，如果不写或者enabled=1表示生效，否则表示不生效。
- gpgcheck：如果是1则表示RPM的数字证书校验生效；如果是0则不生效。
- gpgkey：数字证书保存位置。可以是本地，也可以是http地址。



##### 17.3.2 搭建本地光盘yum源

（1）放入CentOS安装光盘，并挂载光盘到指定位置

（2）让其他几个yum源配置文件失效，有两种方法，方法一：修改扩展名，如`*.repo`变成`*.repo.bak`；方法二：将enabled设置成0。一般来说方法一更便捷。

（3）修改光盘yum源配置文件`CentOS-Media.repo`，配置baseurl为光盘的挂载地址，并将enabled设置成1。

（4）使用`yum list`命令查看rpm包对应的yum源名称，来检查配置是否生效。



##### 17.3.3 yum命令

包含查询、安装、升级、卸载等。命令的格式：

```shell
[root@localhost ~]# yum --help
Usage: yum [options] COMMAND

List of Commands:
info           Display details about a package or group of packages
erase          Remove a package or packages from your system
install        Install a package or packages on your system
list           List a package or groups of packages
reinstall      reinstall a package
repolist       Display the configured software repositories
search         Search package details for the given string
update         Update a package or packages on your system
version        Display a version for the machine and/or available repos.
....more....

Options:
  -h, --help            show this help message and exit
  -t, --tolerant        be tolerant of errors
  -q, --quiet           quiet operation
  -v, --verbose         verbose operation
  -y, --assumeyes       answer yes for all questions
....more....
```



（1）查询

```shell
# 1 查询yum源中所有可用软件包列表
[root@localhost ~]# yum list
# 2 查询yum源服务器中是否包含某个软件包
[root@localhost ~]# yum list 包名

# 3 搜索yum源服务器上所有和关键字相关的软件包
[root@localhost ~]# yum search 关键字
# 3 Example
[root@localhost ~]# yum search ifconfig
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: mirrors.aliyun.com
 * extras: mirrors.bfsu.edu.cn
 * updates: mirrors.aliyun.com
=============================== Matched: ifconfig =========================================
net-tools.x86_64 : Basic networking tools

# 4 查询包的详细信息
[root@localhost ~]# yum search 包名
# 4 Example
[root@localhost ~]# yum info python
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: mirrors.aliyun.com
 * extras: mirrors.bfsu.edu.cn
 * updates: mirrors.aliyun.com
Installed Packages
Name        : python
Arch        : x86_64
Version     : 2.7.5
Release     : 48.el7
Size        : 79 k
Repo        : installed
From repo   : anaconda
Summary     : An interpreted, interactive, object-oriented programming language
URL         : http://www.python.org/
License     : Python
Description : Python is an interpreted, interactive, object-oriented programming
            : language often compared to Tcl, Perl, Scheme or Java. Python includes
            : modules, classes, exceptions, very high level dynamic data types and
            : dynamic typing. Python supports interfaces to many system calls and
            : libraries, as well as to various windowing systems (X11, Motif, Tk,
            : Mac and MFC).
            :
            : Programmers can write new built-in modules for Python in C or C++.
            : Python can be used as an extension language for applications that need
            : a programmable interface.
            :
            : Note that documentation for Python is provided in the python-docs
            : package.
            :
            : This package provides the "python" executable; most of the actual
            : implementation is within the "python-libs" package.
```



（2）安装

```shell
[root@localhost ~]# yum -y install 包名
选项：
	-y       自动回答yes
```



（3）升级

```shell
# 如果不加包名，会升级本机所有软件包（不建议）
[root@localhost ~]# yum -y update 包名
```



（4）卸载

卸载的时候，会自动同时卸载软件包的依赖包，如果被依赖包是重要的系统文件，或者是同时被其他软件包所依赖，则有可能会导致系统不可用。除非非常确定卸载的软件的依赖包不会对系统产生影响，否则不要执行yum的卸载，同时建议不要加-y选项。

```shell
[root@localhost ~]# yum remove 包名
```



##### 17.3.4 yum组管理

以软件组的方式进行管理。

```shell
# 查询可以安装的软件组
[root@localhost ~]# yum grouplist

# 查询软件组中包含的软件
[root@localhost ~]# yum groupinfo "软件组名"

# 安装软件组
[root@localhost ~]# yum groupinstall "软件组名"

# 卸载软件组
[root@localhost ~]# yum groupremove "软件组名"
```



#### 17.4 源码包安装

源码包安装要指定安装路径，一般建议为/usr/local/ 下自建一个软件目录。



##### 17.4.1 步骤

（1）下载软件包

（2）解压缩

（3）进入解压目录

（4）执行命令./configure，进行编译器准备，这一步主要有三个作用：

- 安装前检查系统环境是否符合安装要求
- 定义需要安装的功能选项。`./configure`支持的功能选项较多，可以执行`./configure --help`查询，一般通过`./configure --prefix=安装路径`来指定安装路径。
- 把系统环境的检查结果和定义好的功能选项写入Makefile文件，后续的编译和安装需要依赖这个文件的内容。

（5）执行`make`命令，执行编译

调用gcc编译器，并读取Makefile文件中的信息进行系统软件编译。将源码编译成Linux识别的可执行文件，这些可执行文件保存在**当前目录**下。

（6）执行`make clean`命令，情况编译内容（非必需）

如果在`./configure`或`make`过程中报错，那么在重新执行命令之前，需要执行`make clean`命令，来清空Makefile文件或编译产生的o文件。

（7）执行`make install`命令，执行安装

这才是真正的安装过程，一般会写清楚程序的安装位置。如果忘记指定安装目录，可以把这个命令的执行过程保存下来，以备将来删除使用。



##### 17.4.2 打补丁

diff

patch



### 18 文件系统管理

#### 18.1 文件系统介绍

磁盘：磁道、扇区（512B）、柱面

文件系统特性：

- superblock（超级块）：记录整个文件系统的信息，包括block与inode的总量，已经使用 的inode和block的数量，未使用的inode和block的数量，block与inode的大小，文件系统的挂载时间，最近一次的写入时间，最近一次的磁盘校验时间等。
- dateblock（数据块）：用来实际保存数据的，block的大小和数量在格式化后就已经决定，不能改变，除非重新格式化。每个block只能保存一个文件的数据。
- inode：用来记录文件的权限，文件的所有者和属组，文件的大小，文件的三个变化时间，文件的数据真正保存的block编号。每个文件需要占用一个inode。



#### 18.2 文件系统常用命令

##### 18.2.1 df命令

```shell
[root@localhost ~]# df -ahT
选项：
	a	显示特殊文件系统，这些文件系统几乎都是保存在内存中的。如/proc
	h	单位不再只用KB，而是换算成习惯单位
	T	多出了文件系统类型一列
```



##### 18.2.2 du命令

```shell
[root@localhost ~]# du [选项] [文件/目录]
选项：
	a	显示每个子系统的磁盘占用量。默认只统计子目录的磁盘占用量
	h   使用习惯单位显示磁盘占用量，如KB,MB或GB等
	s	统计总占用量，而不列出子目录和子文件的占用量
```



##### 18.2.3 fsck命令

文件系统修复

```shell
# 自动修复，开机时候会自动执行此命令，不需要手动执行
[root@localhost ~]# fsck -y /dev/sda1
```



##### 18.2.4 显示磁盘分区状态dumpe2fs

只对ext文件系统生效，其他系统如xfs会显示找不到超级区。

```shell
[root@localhost ~]# dumpe2fs -h /dev/sda1
dumpe2fs 1.42.9 (28-Dec-2013)
dumpe2fs: Bad magic number in super-block while trying to open /dev/sda2
Couldn't find valid filesystem superblock.
[root@localhost ~]# dumpe2fs -h /dev/sda3
dumpe2fs 1.42.9 (28-Dec-2013)
Filesystem volume name:   <none>
Last mounted on:          <not available>
Filesystem UUID:          fcb942ea-4454-4d30-b211-763baba30016
Filesystem magic number:  0xEF53
Filesystem revision #:    1 (dynamic)
Filesystem features:      has_journal ext_attr resize_inode dir_index filetype needs_recovery extent 64bit flex_bg sparse_super large_file huge_file uninit_bg dir_nlink extra_isize
Filesystem flags:         signed_directory_hash
Default mount options:    user_xattr acl
Filesystem state:         clean
Errors behavior:          Continue
Filesystem OS type:       Linux
Inode count:              65536
Block count:              262144
Reserved block count:     13107
Free blocks:              249189
Free inodes:              65525
First block:              0
Block size:               4096
Fragment size:            4096
Group descriptor size:    64
Reserved GDT blocks:      127
Blocks per group:         32768
Fragments per group:      32768
Inodes per group:         8192
Inode blocks per group:   512
Flex block group size:    16
Filesystem created:       Sat Aug  6 17:46:54 2022
Last mount time:          Sat Aug  6 17:53:07 2022
Last write time:          Sat Aug  6 17:53:07 2022
Mount count:              1
Maximum mount count:      -1
Last checked:             Sat Aug  6 17:46:54 2022
Check interval:           0 (<none>)
Lifetime writes:          33 MB
Reserved blocks uid:      0 (user root)
Reserved blocks gid:      0 (group root)
First inode:              11
Inode size:               256
Required extra isize:     28
Desired extra isize:      28
Journal inode:            8
Default directory hash:   half_md4
Directory Hash Seed:      c33f8a71-9396-44f3-a8b7-d3c9258b400c
Journal backup:           inode blocks
Journal features:         journal_64bit
Journal size:             32M
Journal length:           8192
Journal sequence:         0x00000002
Journal start:            1
```



##### 18.2.5 查看文件的详细时间stat命令

```shell
[root@localhost ~]# stat anaconda-ks.cfg
  File: ‘anaconda-ks.cfg’
  Size: 1540            Blocks: 8          IO Block: 4096   regular file
Device: fd00h/64768d    Inode: 16797763    Links: 1
Access: (0600/-rw-------)  Uid: (    0/    root)   Gid: (    0/    root)
Context: system_u:object_r:admin_home_t:s0
Access: 2022-07-12 22:32:50.797028593 +0800
Modify: 2021-10-01 13:01:41.758951744 +0800
Change: 2021-10-01 13:01:41.758951744 +0800
 Birth: -
```



#### 18.3 磁盘分区挂载

目前磁盘分区管理有MBR和GPT两种系统格式，这两种格式所使用的分区工具不一样。下面以磁盘/dev/sda为例，演示。

##### 18.3.1 列出磁盘的分区表类型与分区信息

```shell
[root@localhost ~]# parted /dev/sda print
Model: VMware, VMware Virtual S (scsi)
Disk /dev/sda: 21.5GB
Sector size (logical/physical): 512B/512B
Partition Table: msdos
Disk Flags:

Number  Start   End     Size    Type     File system  Flags
 1      1049kB  211MB   210MB   primary  xfs          boot
 2      211MB   13.1GB  12.9GB  primary               lvm
 3      13.1GB  14.2GB  1074MB  primary  ext4
```

查看结果`Partition Table: msdos`可以知道是MBR分区格式，如果结果是`Partition Table: gpt`则表示是GPT格式。

##### 18.3.2 磁盘分区fdisk/gdisk

```shell
[root@localhost ~]# fdisk /dev/sda
Welcome to fdisk (util-linux 2.23.2).
Changes will remain in memory only, until you decide to write them.
Be careful before using the write command.
Command (m for help): n
Partition type:
   p   primary (3 primary, 0 extended, 1 free)
   e   extended
Select (default e): p
Selected partition 4
First sector (27691008-41943039, default 27691008):
Using default value 27691008
Last sector, +sectors or +size{K,M,G} (27691008-41943039, default 41943039): +1G
Partition 4 of type Linux and of size 1 GiB is set

Command (m for help):w
The partition table has been altered!

Calling ioctl() to re-read partition table.

WARNING: Re-reading the partition table failed with error 16: Device or resource busy.
The kernel still uses the old table. The new table will be used at
the next reboot or after you run partprobe(8) or kpartx(8)
```

根据磁盘分区的提示，对于不清楚的，使用`m`查一下即可，可以很容易建立一个磁盘分区。建立后，需要使用`w`保存，才会生效，如果不想保存，使用`q`退出即可。

此外，根据`w`后的提示，需要重启或者使用`partprobe`命令来刷新Linux内核的分区表信息。如果不刷新，`lsblk`是查询不到新建的分区的。

```shell
[root@localhost ~]# lsblk /dev/sda
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda           8:0    0   20G  0 disk
├─sda1        8:1    0  200M  0 part /boot
├─sda2        8:2    0   12G  0 part
│ ├─cl-root 253:0    0   10G  0 lvm  /
│ └─cl-swap 253:1    0    2G  0 lvm  [SWAP]
└─sda3        8:3    0    1G  0 part /opt/sda3
[root@localhost ~]# partprobe -s
/dev/sda: msdos partitions 1 2 3 4
[root@localhost ~]# lsblk
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda           8:0    0   20G  0 disk
├─sda1        8:1    0  200M  0 part /boot
├─sda2        8:2    0   12G  0 part
│ ├─cl-root 253:0    0   10G  0 lvm  /
│ └─cl-swap 253:1    0    2G  0 lvm  [SWAP]
├─sda3        8:3    0    1G  0 part /opt/sda3
└─sda4        8:4    0    1G  0 part
```



##### 18.3.3 磁盘格式化

对应不同的文件系统，有不同的磁盘格式化命令，实际使用的时候，可以通过Linux命令自动补全的方式，查看当前系统支持哪些文件系统格式。

```shell
[root@localhost ~]# mkfs[Tab][Tab]
mkfs         mkfs.cramfs  mkfs.ext3    mkfs.minix
mkfs.btrfs   mkfs.ext2    mkfs.ext4    mkfs.xfs
```

当前比较常用的有`mkfs.xfs`和`mkfs.ext4`，以`mkfs.ext4`为例。

```shell
[root@localhost ~]# mkfs.ext4 [-b size] /dev/sda4
选项：
   -b  设置 block 的大小，有 1K, 2K, 4K 的容量

# Example 一般情况下使用默认值即可
[root@localhost ~]# mkfs.ext4 /dev/sda4
mke2fs 1.42.9 (28-Dec-2013)
Filesystem label=
OS type: Linux
Block size=4096 (log=2)
Fragment size=4096 (log=2)
Stride=0 blocks, Stripe width=0 blocks
65536 inodes, 262144 blocks
13107 blocks (5.00%) reserved for the super user
First data block=0
Maximum filesystem blocks=268435456
8 block groups
32768 blocks per group, 32768 fragments per group
8192 inodes per group
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done
```



##### 18.3.4 磁盘挂载

```shell
# 1 新建一个空目录作为挂载点
[root@localhost ~]# mkdir /opt/sda4

# 2 将磁盘/分区挂载
[root@localhost ~]# mount /dev/sda4 /opt/sda4

# 3 设置开机自动挂载，步骤2只是临时挂载
[root@localhost ~]# echo "/dev/sdb4    /opt/sda4    ext4    defaults    0 0" >> /etc/fstab

# 4 手动使能，同时起到简单的配置校验的作用
[root@localhost ~]# mount -a
```

/etc/fstab文件配置项说明：

- 第一列，设备文件名称或者UUID
- 第二列，挂载点
- 第三列，文件系统
- 第四列，挂载选项
- 第五列，1（是否可以被备份），0（不备份），1（每天备份），2（不定期备份）
- 第六列，2（是否检测磁盘fsck），0（不检测），1（启动时检测），2（启动后检测）



#### 18.4 swap分区

swap分区是虚拟内存，swap分区使用`df`命令是看不到的，需要使用`free`命令。

```shell
[root@localhost ~]# free -h
              total        used        free      shared  buff/cache   available
Mem:           976M        183M        337M        7.3M        455M        582M
Swap:          2.0G          0B        2.0G
```

> buff：缓冲，磁盘写入加速；cache：缓存，磁盘读取加速



##### 18.4.1 分配swap分区

```shell
[root@localhost ~]# fdisk /dev/sda
Welcome to fdisk (util-linux 2.23.2).

Changes will remain in memory only, until you decide to write them.
Be careful before using the write command.


Command (m for help): t
Partition number (1-4, default 4): 4
Hex code (type L to list all codes): l

 0  Empty           24  NEC DOS         81  Minix / old Lin bf  Solaris
 1  FAT12           27  Hidden NTFS Win 82  Linux swap / So c1  DRDOS/sec (FAT-
 2  XENIX root      39  Plan 9          83  Linux           c4  DRDOS/sec (FAT-
 3  XENIX usr       3c  PartitionMagic  84  OS/2 hidden C:  c6  DRDOS/sec (FAT-
 4  FAT16 <32M      40  Venix 80286     85  Linux extended  c7  Syrinx
 5  Extended        41  PPC PReP Boot   86  NTFS volume set da  Non-FS data
 6  FAT16           42  SFS             87  NTFS volume set db  CP/M / CTOS / .
 7  HPFS/NTFS/exFAT 4d  QNX4.x          88  Linux plaintext de  Dell Utility
 8  AIX             4e  QNX4.x 2nd part 8e  Linux LVM       df  BootIt
 9  AIX bootable    4f  QNX4.x 3rd part 93  Amoeba          e1  DOS access
 a  OS/2 Boot Manag 50  OnTrack DM      94  Amoeba BBT      e3  DOS R/O
 b  W95 FAT32       51  OnTrack DM6 Aux 9f  BSD/OS          e4  SpeedStor
 c  W95 FAT32 (LBA) 52  CP/M            a0  IBM Thinkpad hi eb  BeOS fs
 e  W95 FAT16 (LBA) 53  OnTrack DM6 Aux a5  FreeBSD         ee  GPT
 f  W95 Ext'd (LBA) 54  OnTrackDM6      a6  OpenBSD         ef  EFI (FAT-12/16/
10  OPUS            55  EZ-Drive        a7  NeXTSTEP        f0  Linux/PA-RISC b
11  Hidden FAT12    56  Golden Bow      a8  Darwin UFS      f1  SpeedStor
12  Compaq diagnost 5c  Priam Edisk     a9  NetBSD          f4  SpeedStor
14  Hidden FAT16 <3 61  SpeedStor       ab  Darwin boot     f2  DOS secondary
16  Hidden FAT16    63  GNU HURD or Sys af  HFS / HFS+      fb  VMware VMFS
17  Hidden HPFS/NTF 64  Novell Netware  b7  BSDI fs         fc  VMware VMKCORE
18  AST SmartSleep  65  Novell Netware  b8  BSDI swap       fd  Linux raid auto
1b  Hidden W95 FAT3 70  DiskSecure Mult bb  Boot Wizard hid fe  LANstep
1c  Hidden W95 FAT3 75  PC/IX           be  Solaris boot    ff  BBT
1e  Hidden W95 FAT1 80  Old Minix
Hex code (type L to list all codes): 82
Changed type of partition 'Linux' to 'Linux swap / Solaris'

Command (m for help): p

Disk /dev/sda: 21.5 GB, 21474836480 bytes, 41943040 sectors
Units = sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk label type: dos
Disk identifier: 0x00020272

   Device Boot      Start         End      Blocks   Id  System
/dev/sda1   *        2048      411647      204800   83  Linux
/dev/sda2          411648    25593855    12591104   8e  Linux LVM
/dev/sda3        25593856    27691007     1048576   83  Linux
/dev/sda4        27691008    29788159     1048576   82  Linux swap / Solaris

Command (m for help): w
The partition table has been altered!

Calling ioctl() to re-read partition table.

WARNING: Re-reading the partition table failed with error 16: Device or resource busy.
The kernel still uses the old table. The new table will be used at
the next reboot or after you run partprobe(8) or kpartx(8)
Syncing disks.
```



##### 18.4.2 格式化swap分区

```shell
[root@localhost ~]# mkswap /dev/sda4
mkswap: /dev/sda4: warning: wiping old ext4 signature.
Setting up swapspace version 1, size = 1048572 KiB
no label, UUID=4c59a786-d8fe-4f9a-9fff-3a2364a1b0fe
```



##### 18.4.3 挂载swap分区

```shell
[root@localhost ~]# free -h
              total        used        free      shared  buff/cache   available
Mem:           976M        187M        332M        7.6M        456M        577M
Swap:          2.0G          0B        2.0G
[root@localhost ~]# swapon /dev/sda4
[root@localhost ~]# free -h
              total        used        free      shared  buff/cache   available
Mem:           976M        188M        331M        7.6M        456M        576M
Swap:          3.0G          0B        3.0G
```

如果需要让swap分区开机自动挂载，需要配置/etc/fstab配置文件

```shell
[root@localhost ~]# echo "/dev/sda4    swap    swap    defaults    0 0" >> /etc/fstab
```



### 19 高级文件系统管理

#### 19.1 磁盘配额

需要内核支持磁盘配额，验证方式：

```shell
[root@localhost ~]# grep CONFIG_QUOTA /boot/config-*
CONFIG_QUOTA=y
CONFIG_QUOTA_NETLINK_INTERFACE=y
# CONFIG_QUOTA_DEBUG is not set
CONFIG_QUOTA_TREE=y
CONFIG_QUOTACTL=y
CONFIG_QUOTACTL_COMPAT=y
```

磁盘配额功能，需要使用quota工具。

```shell
[root@localhost ~]# rpm -qa | grep quota
quota-4.01-19.el7.x86_64
```



##### 19.1.1 磁盘配额的功能

（1）用户配额和组配额。一般情况下组配额使用的常见比较少。

（2）磁盘容量限制和文件个数限制。

（3）软限制和硬限制。超过软限制，每次登录会提示磁盘配额不足，超过硬限制即不可写入。

（4）宽限时间。用户空间占用数处于软限制和硬限制之间的宽限时间，超过软限制升级为硬限制。



##### 19.1.2 磁盘配额规划

磁盘配额是限制**普通用户**在**分区**上使用磁盘空间和文件个数的。

（1）开启磁盘配额功能

```shell
# 1 在分区上开启磁盘配额功能
[root@localhost ~]# mount -o remount,usrquota,grpquota /opt/sda3

# 2 开启磁盘配额功能永久生效
[root@localhost ~]# vim /etc/fstab
/dev/sda3    /opt/sda3     ext4     defaults,usrquota,grpquota    0 0
```

（2）建立磁盘配额的配置文件

```shell
[root@localhost ~]# quotacheck [选项] [分区名]
选项：
	-a	扫描/etc/mtab文件所有启动磁盘配额功能的分区。如果加入此参数，命令后面不需要加入分区名
	-v	显示扫描过程
	-u	建立用户配额的配置文件，也就是生成aquota.user文件
	-g	建立组配额的配置文件，也就是生成aquota.group文件

# Example
[root@localhost ~]# quotacheck -vug /dev/sda3
quotacheck: Your kernel probably supports journaled quota but you are not using it. Consider switching to journaled quota to avoid running quotacheck after an unclean shutdown.
quotacheck: Scanning /dev/sda3 [/opt/sda3] done
quotacheck: Cannot stat old user quota file /opt/sda3/aquota.user: No such file or directory. Usage will not be subtracted.
quotacheck: Cannot stat old group quota file /opt/sda3/aquota.group: No such file or directory. Usage will not be subtracted.
quotacheck: Cannot stat old user quota file /opt/sda3/aquota.user: No such file or directory. Usage will not be subtracted.
quotacheck: Cannot stat old group quota file /opt/sda3/aquota.group: No such file or directory. Usage will not be subtracted.
```

需要关闭SELinux，否则会报错。

```shell
[root@localhost ~]# getenforce
Enforcing
[root@localhost ~]# setenforce 0   # 0 关闭；1 开启
[root@localhost ~]# getenforce
Permissive
```

（3）设置用户和组的配额限制

```shell
[root@localhost ~]# edquota [选项] [用户名/组名]
选项：
	-u 用户名：	设定用户配额
	-g 组名：	 设定组配额
	-t：		  设定宽限时间
	-p：       复制配额限制

# Example 磁盘空间软限制40M，硬限制50MB；文件个数软限制8个，硬限制10个。
[root@localhost ~]# edquota -u user1
Disk quotas for user user1 (uid 1000):
  Filesystem                   blocks       soft       hard     inodes     soft     hard
  /dev/sda3                         0       40000      50000          0       8       10
```

`edquota`需要进入到vi交互界面来操作，不适用与脚本场景，非交互设定用户磁盘配额，可以使用`setquota`指令。

```shell
[root@localhost ~]# setquota -u 用户名 容量软限制 容量硬限制 文件个数软限制 文件个数硬限制 文件系统/分区名
```



（4）启动和关闭配额

```shell
# 1 启动配额
[root@localhost ~]# quotaon [选项] [分区名]
选项：
	-a：	依据/etc/mtab文件启动所有的配额分区。如果不加，后面就一定要指定分区名
	-u： 启动用户配额
	-g： 启动组配额
	-v： 显示启动过程的信息

[root@localhost ~]# quotaon -vug /dev/sda3
/dev/sda3 [/opt/sda3]: group quotas turned on
/dev/sda3 [/opt/sda3]: user quotas turned on

# 2 关闭配额
[root@localhost ~]# quotaoff [选项] [分区名]
选项：
	-a：	依据/etc/mtab文件启动所有的配额分区。如果不加，后面就一定要指定分区名
	-u： 启动用户配额
	-g： 启动组配额
```



##### 19.1.3 磁盘配额查询

（1）查询用户或用户组配额

```shell
[root@localhost ~]# quota [选项] [用户名或组名]
选项：
	-u 用户名：	查询用户配额
	-g 组名：	 查询组配额
	-v：		  显示详细信息
	-s：       以习惯单位显示容器大小，如M和G

# Example
[root@localhost ~]# quota -uvs user1
Disk quotas for user user1 (uid 1000):
     Filesystem   space   quota   limit   grace   files   quota   limit   grace
      /dev/sda3      0K  40000K  50000K               0       8      10
```



##### 19.1.4 磁盘配额测试

```shell
# 磁盘配额测试
[user1@localhost ~]$ dd if=/dev/zero of=/opt/sda3/test bs=1M count=60
sda3: warning, user block quota exceeded.
sda3: write failed, user block limit reached.
dd: error writing ‘/opt/sda3/test’: Disk quota exceeded
49+0 records in
48+0 records out
51195904 bytes (51 MB) copied, 0.212615 s, 241 MB/s
[user1@localhost ~]$ ll -h /opt/sda3/
total 49M
-rw-------. 1 root  root  7.0K Aug  8 23:07 aquota.group
-rw-------. 1 root  root  7.0K Aug  8 23:21 aquota.user
drwx------. 2 root  root   16K Aug  6 17:46 lost+found
-rw-rw-r--. 1 user1 user1  49M Aug  8 23:40 test

# 文件个数配额测试
[user1@localhost ~]$ quota -uvs user1
Disk quotas for user user1 (uid 1000):
     Filesystem   space   quota   limit   grace   files   quota   limit   grace
      /dev/sda3  50000K* 40000K  50000K   6days       2       8      10
[user1@localhost sda3]$ touch 1; touch 2; touch 3; touch 4; touch 5
[user1@localhost sda3]$ quota -uvs user1
Disk quotas for user user1 (uid 1000):
     Filesystem   space   quota   limit   grace   files   quota   limit   grace
      /dev/sda3  50000K* 40000K  50000K   6days       7       8      10
[user1@localhost sda3]$ touch 7
sda3: warning, user file quota exceeded.
```



##### 19.1.5 磁盘配额复制

user2用户限额和user1完全一致，就可以使用user1作为模板，进行复制：

```shell
[root@localhost ~]# edquota -p user1 -u user2

# Example
[root@localhost ~]# quota -uvs user1
Disk quotas for user user1 (uid 1000):
     Filesystem   space   quota   limit   grace   files   quota   limit   grace
      /dev/sda3  50000K* 40000K  50000K   6days       9*      8      10   6days
[root@localhost ~]# quota -uvs user2
Disk quotas for user user2 (uid 1001):
     Filesystem   space   quota   limit   grace   files   quota   limit   grace
      /dev/sda3      0K      0K      0K               0       0       0
[root@localhost ~]# edquota -p user1 -u user2
[root@localhost ~]# quota -uvs user2
Disk quotas for user user2 (uid 1001):
     Filesystem   space   quota   limit   grace   files   quota   limit   grace
      /dev/sda3      0K  40000K  50000K               0       8      10
```



#### 19.2 逻辑卷管理

LVM是Logical Volume Manager的简称，中文就是逻辑卷管理。

![lvm](.\lvm.png)

- PV：物理卷，就是真实物理硬盘或分区的映射抽象
- VG：卷组，多个物理卷组成一个卷组
- LV：逻辑卷，类似于分区，逻辑卷可以格式化和挂载使用
- PE：物理扩展，存储数据的最小单位，实际的数据是写入PE的，PE的大小可以配置，默认4MB

逻辑卷的出现，是为了实现弹性扩容文件系统的大小。



##### 19.2.1 建立LVM的步骤

（1）将物理硬盘分成分区，当然也可以是整块物理硬盘

（2）将物理分区建立成物理卷（PV），也可以将整块硬盘建立为物理卷

（3）将物理卷整合称为卷组（VG）。卷组就已经可以动态调整大小了，可以把PV加入到卷组，也可以移除

（4）把卷组划分为逻辑卷（LV）。逻辑卷也可以直接调整大小，逻辑卷相当于分区，需要进行格式化和挂载后使用



##### 19.2.2 物理卷管理

（1）硬盘分区

使用fdisk/gdisk交互命令进行分区，注意分区的系统ID不再是默认的83，而要改成LVM的8e。

```shell
Command (m for help): t
Partition number (1-8, default 8): 8
Hex code (type L to list all codes): 8e
Changed type of partition 'Linux' to 'Linux LVM'

Command (m for help): p

Disk /dev/sda: 21.5 GB, 21474836480 bytes, 41943040 sectors
Units = sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk label type: dos
Disk identifier: 0x00020272

   Device Boot      Start         End      Blocks   Id  System
/dev/sda1   *        2048      411647      204800   83  Linux
/dev/sda2          411648    25593855    12591104   8e  Linux LVM
/dev/sda3        25593856    27691007     1048576   83  Linux
/dev/sda4        27691008    41943039     7126016    5  Extended
/dev/sda5        27693056    29790207     1048576   8e  Linux LVM
/dev/sda6        29792256    31889407     1048576   8e  Linux LVM
/dev/sda7        31891456    33988607     1048576   8e  Linux LVM
/dev/sda8        33990656    36087807     1048576   8e  Linux LVM
```



（2）建立物理卷

```shell
[root@localhost ~]# pvcreate 磁盘/分区

#Example
[root@localhost ~]# pvcreate /dev/sda5 /dev/sda6 /dev/sda7 /dev/sda8
  Physical volume "/dev/sda5" successfully created.
  Physical volume "/dev/sda6" successfully created.
  Physical volume "/dev/sda7" successfully created.
  Physical volume "/dev/sda8" successfully created.
```

建立物理卷，可以把整块硬盘都建立成物理卷，也可以把某个分区建立成物理卷。



（3）查看物理卷

```shell
# 方法1
[root@localhost ~]# pvs
  PV         VG Fmt  Attr PSize  PFree
  /dev/sda2  cl lvm2 a--  12.00g 4.00m
  /dev/sda5     lvm2 ---   1.00g 1.00g
  /dev/sda6     lvm2 ---   1.00g 1.00g
  /dev/sda7     lvm2 ---   1.00g 1.00g
  /dev/sda8     lvm2 ---   1.00g 1.00g

# 方法2
[root@localhost ~]# pvscan
  PV /dev/sda2   VG cl              lvm2 [12.00 GiB / 4.00 MiB free]
  PV /dev/sda8                      lvm2 [1.00 GiB]
  PV /dev/sda7                      lvm2 [1.00 GiB]
  PV /dev/sda5                      lvm2 [1.00 GiB]
  PV /dev/sda6                      lvm2 [1.00 GiB]
  Total: 5 [16.00 GiB] / in use: 1 [12.00 GiB] / in no VG: 4 [4.00 GiB]

# 方法3：信息更加详细
[root@localhost ~]# pvdisplay
  --- Physical volume ---
  PV Name               /dev/sda2
  VG Name               cl
  PV Size               12.01 GiB / not usable 4.00 MiB
  Allocatable           yes
  PE Size               4.00 MiB
  Total PE              3073
  Free PE               1
  Allocated PE          3072
  PV UUID               98o8pP-Qf9g-sspJ-ZomV-AFAK-slOR-j2Xtaw

  "/dev/sda8" is a new physical volume of "1.00 GiB"
  --- NEW Physical volume ---
  PV Name               /dev/sda8
  VG Name
  PV Size               1.00 GiB
  Allocatable           NO
  PE Size               0
  Total PE              0
  Free PE               0
  Allocated PE          0
  PV UUID               HYLL53-Z79w-hzcy-57wc-Afcu-9Ugv-VBcI90
....
```



（4）删除物理卷

一般不会用到。

```shell
[root@localhost ~]# pvremove /dev/sda8
  Labels on physical volume "/dev/sda8" successfully wiped.
[root@localhost ~]# pvs
  PV         VG Fmt  Attr PSize  PFree
  /dev/sda2  cl lvm2 a--  12.00g 4.00m
  /dev/sda5     lvm2 ---   1.00g 1.00g
  /dev/sda6     lvm2 ---   1.00g 1.00g
  /dev/sda7     lvm2 ---   1.00g 1.00g
```



##### 19.2.3 卷组管理

将PE加入到VG中。

（1）建立卷组

```shell
[root@localhost ~]# vgcreate [选项] 卷组名 物理卷名
选项：
	-s PE大小：指定PE的大小，单位可以是MB,GB,TB等。如果不屑默认PE大小是4MB

# Example
[root@localhost ~]# vgcreate -s 4MB avg /dev/sda5 /dev/sda6 /dev/sda7
```



（3）查看卷组

```shell
# 方法一，比较常用
[root@localhost ~]# vgs
  VG  #PV #LV #SN Attr   VSize  VFree
  avg   3   0   0 wz--n-  2.99g 2.99g
  cl    1   2   0 wz--n- 12.00g 4.00m

# 方法二：只能看到有没有vg，信息太少，基本不用
[root@localhost ~]# vgscan
  Reading volume groups from cache.
  Found volume group "cl" using metadata type lvm2
  Found volume group "avg" using metadata type lvm2

# 方法三：信息更加详细
[root@localhost ~]# vgdisplay
  --- Volume group ---
  VG Name               cl
  System ID
  Format                lvm2
  Metadata Areas        1
  Metadata Sequence No  3
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                2
  Open LV               2
  Max PV                0
  Cur PV                1
  Act PV                1
  VG Size               12.00 GiB
  PE Size               4.00 MiB
  Total PE              3073
  Alloc PE / Size       3072 / 12.00 GiB
  Free  PE / Size       1 / 4.00 MiB
  VG UUID               RRMWbE-7777-9xvL-FcHB-6Pn3-jI1e-zcLkXJ

  --- Volume group ---
  VG Name               avg
  System ID
  Format                lvm2
  Metadata Areas        3
  Metadata Sequence No  1
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                0
  Open LV               0
  Max PV                0
  Cur PV                3
  Act PV                3
  VG Size               2.99 GiB
  PE Size               4.00 MiB
  Total PE              765
  Alloc PE / Size       0 / 0
  Free  PE / Size       765 / 2.99 GiB
  VG UUID               HKHV8u-ONqt-RLI4-1elN-khPv-TmBq-PzplnF
```



（3）卷组扩容

```shell
[root@localhost ~]# vgextend avg /dev/sda8
  Volume group "avg" successfully extended
[root@localhost ~]# vgdisplay avg
  --- Volume group ---
  VG Name               avg
  System ID
  Format                lvm2
  Metadata Areas        4
  Metadata Sequence No  2
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                0
  Open LV               0
  Max PV                0
  Cur PV                4
  Act PV                4
  VG Size               3.98 GiB
  PE Size               4.00 MiB
  Total PE              1020
  Alloc PE / Size       0 / 0
  Free  PE / Size       1020 / 3.98 GiB
  VG UUID               HKHV8u-ONqt-RLI4-1elN-khPv-TmBq-PzplnF
```



（4）缩容卷组

一般不会用到。

```shell
[root@localhost ~]# vgreduce avg /dev/sda8
  Removed "/dev/sda8" from volume group "avg"

# 删除vg所有未使用pv，最后会剩余一个无法删除
[root@localhost ~]# vgreduce -a avg
  Removed "/dev/sda5" from volume group "avg"
  Removed "/dev/sda6" from volume group "avg"
  Can't remove final physical volume "/dev/sda7" from volume group "avg"
```



（5）删除卷组

一般不会用到。

```shell
[root@localhost ~]# vgremove avg
  Volume group "avg" successfully removed
```

卷组删除之后，才能删除物理卷。如果avg卷组有添加了逻辑卷，需要先删除逻辑卷再删除卷组。



##### 19.2.4 逻辑卷管理

（1）建立逻辑卷

```shell
[root@localhost ~]# lvcreate [选项] [-n 逻辑卷名] 卷组名
选项：
	-L 容量：指定LV的大小，单位可以是MB,GB,TB等。
	-l 个数：按照PE个数指定逻辑卷大小，这个参数需要换算容量，太麻烦，一般不用
	-n 逻辑卷名：指定逻辑卷名

# Example: create a 1.5G LV
[root@localhost ~]# lvcreate -L 1.5G -n alv avg
[root@localhost ~]# mkfs.ext4 /dev/avg/alv
[root@localhost ~]# mkdir /opt/alv
[root@localhost ~]# mount /dev/avg/alv /opt/alv/
# 永久挂载需要配置/etc/fstab文件
```



（2）查看逻辑卷

```shell
[root@localhost ~]# lvs
  LV   VG  Attr       LSize  Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  alv  avg -wi-ao----  1.50g
  root cl  -wi-ao---- 10.00g
  swap cl  -wi-ao----  2.00g
[root@localhost ~]# lvscan
  ACTIVE            '/dev/avg/alv' [1.50 GiB] inherit
  ACTIVE            '/dev/cl/swap' [2.00 GiB] inherit
  ACTIVE            '/dev/cl/root' [10.00 GiB] inherit
[root@localhost ~]# lvdisplay
  --- Logical volume ---
  LV Path                /dev/avg/alv
  LV Name                alv
  VG Name                avg
  LV UUID                TGkqLC-YwY7-dcRP-NQbu-W7UK-fJQf-Dcn4xg
  LV Write Access        read/write
  LV Creation host, time localhost, 2022-08-17 23:36:17 +0800
  LV Status              available
  # open                 1
  LV Size                1.50 GiB
  Current LE             384
  Segments               2
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:2
.....
```



（3）调整逻辑卷大小

```shell
[root@localhost ~]# lvresize [选项] 逻辑卷设备文件名
选项：
	-L 容量：指定LV的大小，单位可以是MB,GB,TB等。使用+代表增加空间，-号代表减少空间，直接写容量，代表设定逻辑卷大小为指定大小。
	-l 个数：按照PE个数指定逻辑卷大小

# Example: add 1G
[root@localhost ~]# lvresize -L +1G /dev/avg/alv
  Size of logical volume avg/alv changed from 1.50 GiB (384 extents) to 2.50 GiB (640 extents).
  Logical volume avg/alv successfully resized.
[root@localhost ~]# lvs
  LV   VG  Attr       LSize  Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  alv  avg -wi-ao----  2.50g
...more...
# 查询分区使用大小，还是只有1.5G，还需要使用resize2fs/xfs_growfs来调整分区大小
[root@localhost ~]# df -h
Filesystem           Size  Used Avail Use% Mounted on
...
/dev/mapper/avg-alv  1.5G  4.5M  1.4G   1% /opt/alv
[root@localhost ~]# resize2fs /dev/avg/alv
resize2fs 1.42.9 (28-Dec-2013)
Filesystem at /dev/avg/alv is mounted on /opt/alv; on-line resizing required
old_desc_blocks = 1, new_desc_blocks = 1
The filesystem on /dev/avg/alv is now 655360 blocks long.

# Example：将vg剩余空间全都扩容到lv上
[root@localhost ~]# lvextend -l 100%Free /dev/avg/alv
```



（4）删除逻辑卷

```shell
[root@localhost ~]# lvremove 逻辑卷设备文件名

# Example
[root@localhost ~]# umount /dev/avg/alv
[root@localhost ~]# lvremove /dev/avg/alv
```



参考

[Linux文件三种时间](https://www.cnblogs.com/poloyy/p/12586677.html)

[Linux基础](https://www.bilibili.com/video/BV1GJ411K7px?p=22&spm_id_from=pageDriver&vd_source=02ec6f8cbf71a099316cdc86dad7c2cf)

[coreutils包下载链接](http://linuxsoft.cern.ch/cern/centos/7/updates/x86_64/repoview/coreutils.html)

[磁盘分区、格式化、挂载](http://shouce.jb51.net/vbird-linux-basic-4/61.html)
