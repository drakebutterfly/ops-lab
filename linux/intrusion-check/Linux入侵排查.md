<h1 align="center">Linux入侵排查</h1>

## 一、系统排查

在对被入侵主机进行排查的时候，需要先进行系统排查，了解被入侵主机的基本情况。主要分为：系统基本信息，系统用户信息，用户登录信息，系统启动项和系统任务计划等。

### 1.1 系统基本信息

#### 1.1.1 操作系统信息

使用`lsb_release -a`命令查看操作系统的发行版本。此外，也可以通过配置文件查看操作系统的发行版本，不同的发行版的相关的配置文件不同，可能为`/etc/os-release`，`/etc/system-release`，`/etc/centos-release`等。

```shell
[root@localhost ~]# lsb_release -a
LSB Version:	:base-4.0-amd64:base-4.0-noarch:core-4.0-amd64:core-4.0-noarch:graphics-4.0-amd64:graphics-4.0-noarch:printing-4.0-amd64:printing-4.0-noarch
Distributor ID:	CentOS
Description:	CentOS release 6.5 (Final)
Release:	6.5
Codename:	Final
[root@localhost ~]# cat /etc/system-release
CentOS release 6.5 (Final)
```

使用`uname -a`命令查看操作系统的内核以及CPU架构。

```shell
[root@localhost ~]# uname -a
Linux localhost 2.6.32-431.el6.x86_64 #1 SMP Fri Nov 22 03:15:09 UTC 2013 x86_64 x86_64 x86_64 GNU/Linux
[root@localhost ~]# uname --help
用法：uname [选项]...
输出一组系统信息。如果不跟随选项，则视为只附加-s 选项。

  -a, --all			以如下次序输出所有信息。其中若-p 和
				-i 的探测结果不可知则被省略：
  -s, --kernel-name		输出内核名称
  -n, --nodename		输出网络节点上的主机名
  -r, --kernel-release		输出内核发行号
  -v, --kernel-version		输出内核版本
  -m, --machine		输出主机的硬件架构名称
  -p, --processor		输出处理器类型或"unknown"
  -i, --hardware-platform	输出硬件平台或"unknown"
  -o, --operating-system	输出操作系统名称
      --help		显示此帮助信息并退出
      --version		显示版本信息并退出
```

#### 1.1.2 CPU信息

使用`lscpu`命令查看CPU的相关信息，主要有：Hypervisor vendor，核心数，主频等。

```shell
[root@localhost ~]# lscpu
Architecture:          x86_64
CPU op-mode(s):        32-bit, 64-bit
Byte Order:            Little Endian
CPU(s):                4
On-line CPU(s) list:   0-3
Thread(s) per core:    1
Core(s) per socket:    4
Socket(s):             1
NUMA node(s):          1
Vendor ID:             GenuineIntel
CPU family:            6
Model:                 58
Stepping:              9
CPU MHz:               2394.639
BogoMIPS:              4789.27
Hypervisor vendor:     VMware
Virtualization type:   full
L1d cache:             32K
L1i cache:             32K
L2 cache:              256K
L3 cache:              3072K
NUMA node0 CPU(s):     0-3
```



### 1.2 系统用户信息

通过查找用户信息，能够初步识别出哪些用户是可疑用户，哪些用户可能被劫持。用户信息相关的配置文件有：`/etc/passwd`，`/etc/shadow`，`/etc/group`和`/etc/sudoers`。

#### 1.2.1 查看超级权限账户

超级权限账户就是UID为0的账户，一般来说只有root用户是超级用户。

```shell
[root@localhost ~]# awk -F: '$3==0{print $0}' /etc/passwd
root:x:0:0:root:/root:/bin/bash
```

#### 1.2.2 查看root组中的用户

查看GID为0的组记录，查看是否有用户被添加到root组。

```shell
[root@localhost ~]# awk -F: '$3==0{print $0}' /etc/group
root:x:0:ymh
```

#### 1.2.3 查看可登陆用户

```shell
[root@localhost ~]# cat /etc/passwd | grep "/bin/bash"
root:x:0:0:root:/root:/bin/bash
ymh:x:501:807:ymh:/home/ymh:/bin/bash
```

#### 1.2.4 查看空口令用户

```shell
[root@localhost ~]# awk -F: 'length($2)==0{print $0}' /etc/shadow
```



### 1.3 用户登陆信息

#### 1.3.1 查看当前用户登陆情况

使用命令`who`或者`w`。

```shell
[root@localhost ~]# w
 20:02:05 up  2:29,  2 users,  load average: 0.00, 0.00, 0.00
USER     TTY      FROM              LOGIN@   IDLE   JCPU   PCPU WHAT
root     tty1     -                17:35    2:26m  0.06s  0.06s -bash
root     pts/0    192.168.1.6      17:38    0.00s  1.69s  0.01s w
[root@localhost ~]# who
root     tty1         2021-08-14 17:35
root     pts/0        2021-08-14 17:38 (192.168.1.6)
```

#### 1.3.2 查看所有用户的最后登录信息

使用命令`lastlog`。

```shell
[root@localhost ~]# lastlog
Username         Port     From             Lastest
root             pts/0    192.168.1.6      Sat Aug 14 17:38:02 +0800 2021
bin                                        **Never logged in**
```

#### 1.3.3 查看所有用户最近登录信息

使用命令`last`。

```shell
[root@localhost ~]# last
root     pts/0        192.168.1.6      Sat Aug 14 17:38   still logged in   
root     tty1                          Sat Aug 14 17:35   still logged in   
reboot   system boot  2.6.32-431.el6.x Sat Aug 14 17:33 - 20:05  (02:32)    
root     tty1                          Sun Jun 13 01:22 - crash (62+16:11)  
reboot   system boot  2.6.32-431.el6.x Sun Jun 13 01:20 - 20:05 (62+18:45)  
reboot   system boot  2.6.32-431.el6.x Sun Jun 13 01:17 - 01:19  (00:02)    
reboot   system boot  2.6.32-431.el6.x Sun Jun 13 01:14 - 01:19  (00:05)    
reboot   system boot  2.6.32-431.el6.x Sun Jun 13 01:12 - 01:19  (00:07)    
reboot   system boot  2.6.32-431.el6.x Sun Jun 13 00:55 - 01:19  (00:24)    
root     tty1                          Wed Apr  3 04:36 - down   (03:18)
reboot   system boot  2.6.32-431.el6.x Wed Apr  3 04:35 - 07:55  (03:20)    
root     pts/0        192.168.199.161  Tue Jan  9 18:21 - 18:49  (00:28)
```



### 1.4 系统启动项

启动项是恶意程序常驻的一种方式。分析系统启动项，需要了解一些相关的知识，包括：系统运行级别、启动项配置文件和启动项配置。

#### 1.4.1 系统运行级别

运行级别指的是Linux的不同运行模式，通常分为7等，分别是从0到6。

- level 0：关机
- level 1：单用户模式（超级用户），无网络连接
- level 2：多用户，无网络连接
- level 3：多用户，有网络连接，一般运行模式
- level 4：用户自定义，不可用
- level 5：多用户，有网络连接，图形界面
- level 6：重启

可以通过`runlevel`命令查看系统运行级别。

```shell
[root@localhost ~]# runlevel
N 3
```

#### 1.4.2 启动项配置文件

启动项相关的配置在目录`/etc/rc.d`中。

```shell
[root@localhost rc.d]# ll
total 60
drwxr-xr-x. 2 root root  4096 Aug 15 18:46 init.d
-rwxr-xr-x. 1 root root  2617 Nov 23  2013 rc
-rwxr-xr-x. 1 root root   220 Nov 23  2013 rc.local
-rwxr-xr-x. 1 root root 19688 Nov 23  2013 rc.sysinit
drwxr-xr-x. 2 root root  4096 May 18  2017 rc0.d
drwxr-xr-x. 2 root root  4096 May 18  2017 rc1.d
drwxr-xr-x. 2 root root  4096 May 18  2017 rc2.d
drwxr-xr-x. 2 root root  4096 May 18  2017 rc3.d
drwxr-xr-x. 2 root root  4096 May 18  2017 rc4.d
drwxr-xr-x. 2 root root  4096 May 18  2017 rc5.d
drwxr-xr-x. 2 root root  4096 May 18  2017 rc6.d
```

- `/etc/rc.d/init.d`：存放了服务的启动脚本，启动脚本配合`chkconfig`命令使用，需要添加约定配置。

  ```shell
  #!/bin/sh
  #
  # chkconfig:   345 95 5
  # description: description
  # 上面两行必须添加，345代表在345运行级别中启动，95代表启动顺序，5代表关闭顺序
  ```

- `/etc/rc.d/rc.local`：可以将需要开机自启动的脚本放在此脚本中

- `/etc/rc.d/rc[0-6].d`：包含有7种运行级别对应的启动脚本，通过符号链接到`/etc/rc.d/init.d`的实际脚本文件中。符号链接的名称定义了脚本的执行方式、顺序和服务。

  ```shell
  lrwxrwxrwx. 1 root root 14 May 14  2017 K99rngd -> ../init.d/rngd
  lrwxrwxrwx. 1 root root 17 May 14  2017 S01sysstat -> ../init.d/sysstat
  # S(start) K(kill) D(disable)
  # 99和01代表脚本执行的顺序
  # rngd代表了服务的名称
  # 在运行级的切换过程中，系统会自动寻找对应运行级的目录/etc/rc.d/rc[0-6].d下的K和S开头的文件按后面的数字顺序，执行这些脚本。
  ```

#### 1.4.3 启动项配置

使用`chkconfig`命令实现更新和查询不同运行级别上的系统服务。`chkconfig`不是立即自动禁止或激活一个服务，它只是简单的改变了符号连接。

```shell
[root@localhost rc3.d]# chkconfig --help
usage:   chkconfig [--list] [--type <type>] [name]
         chkconfig --add <name>
         chkconfig --del <name>
         chkconfig --override <name>
         chkconfig [--level <levels>] [--type <type>] <name> <on|off|reset|resetpriorities>
```

1）查看服务的启动项配置

在入侵排查中，需要关注是否有可疑的服务或者**脚本**被添加到启动项中。可以通过`chkconfig --list`和查看`/etc/rc.d/rc.local`来查看。

```shell
[root@localhost rc3.d]# chkconfig --list
atd            	0:off	1:off	2:off	3:on	4:on	5:on	6:off
cpuspeed       	0:off	1:on	2:on	3:on	4:on	5:on	6:off
crond          	0:off	1:off	2:on	3:on	4:on	5:on	6:off
rsyslog        	0:off	1:off	2:on	3:on	4:on	5:on	6:off
sshd           	0:off	1:off	2:on	3:on	4:on	5:on	6:off
[root@localhost rc3.d]# chkconfig --list sshd
sshd           	0:off	1:off	2:on	3:on	4:on	5:on	6:off
[root@localhost rc.d]# cat rc.local
```

2）更新服务的运行级别

通过关闭sshd服务在24运行级别的启动项配置，可以观察到对应的符号链接的变化。

```shell
[root@localhost rc3.d]# chkconfig --level 24 sshd off
[root@localhost rc3.d]# chkconfig --list sshd
sshd           	0:off	1:off	2:off	3:on	4:off	5:on	6:off
[root@localhost rc3.d]# ll ../rc4.d/ | grep sshd
lrwxrwxrwx. 1 root root 14 Aug 16 01:01 K25sshd -> ../init.d/sshd
[root@localhost rc3.d]# chkconfig --level 4 sshd on
[root@localhost rc3.d]# ll ../rc4.d/ | grep sshd
lrwxrwxrwx. 1 root root 14 Aug 16 01:02 S55sshd -> ../init.d/sshd
```

如果需要关闭服务的自启动项，可以通过以上命令进行操作。

3）添加自启动服务

首先确保`/etc/rc.d/init.d/mysqld`存在，若用 `service mysqld start `或`systemctl mysqld start`能够正常启动，表示服务存在。

> 注意：如果是rpm形式安装，相应服务会自动在`/etc/rc.d/init.d`目录注册的。

配置命令：

```shell
chkconfig --add mysqld
chkconfig --level 345 mysqld on
```

4）添加自定义脚本/服务

 如果服务启动脚本在`/etc/rc.d/init.d`不存在，或者想添加自定义的启动脚本，可以自己制作对应的shell启动脚本。

> 注意：配合chkconfig使用，需要添加两行注释。

创建脚本并添加执行权限。

````shell
cd /etc/rc.d/init.d
touch hellod
chmod +x hellod
````

将脚本/服务添加到启动项中。

```shell
[root@localhost rc.d]# chkconfig --add hellod
[root@localhost rc.d]# chkconfig --list hellod
hellod         	0:off	1:off	2:on	3:on	4:on	5:on	6:off
```

hellod内容：

```shell
#!/bin/bash
#
# hellod        welcome after startup
#
# chkconfig: 2345 99 10
# description: the hellod service show welcome slogon after startup
case "$1" in
    start)
        echo "hello, welcome to drake linux lab!" && exit 0
        ;;
    *)
	exit 2
	;;
esac
exit $?
```

本次实现的功能，直接在`/etc/rc.d/rc.local`中添加脚本更加合理。通过`chkconfig`来实现，只是想展示一般服务的启动项配置方法而已。

### 1.5 任务计划

启动项是恶意脚本程序常驻的另外一种方式。

#### 1.5.1 任务计划查看

```shell
[root@localhost ~]# cat /etc/crontab 
[root@localhost ~]# crontab -u root -l
```



#### 1.5.2 任务计划配置

通过Linux的crond服务来实现。

```shell
[root@localhost cron.monthly]# crontab --help
usage:	crontab [-u user] file
	crontab [-u user] [ -e | -l | -r ]
		(default operation is replace, per 1003.2)
	-e	(edit user's crontab)
	-l	(list user's crontab)
	-r	(delete user's crontab)
	-i	(prompt before deleting user's crontab)
	-s	(selinux context)
```

可以通过在`/etc/crontab`配置文件中配置。

```shell
[root@localhost ~]# cat /etc/crontab 
SHELL=/bin/bash
PATH=/sbin:/bin:/usr/sbin:/usr/bin
MAILTO=root
HOME=/

# For details see man 4 crontabs

# Example of job definition:
# .---------------- minute (0 - 59)
# |  .------------- hour (0 - 23)
# |  |  .---------- day of month (1 - 31)
# |  |  |  .------- month (1 - 12) OR jan,feb,mar,apr ...
# |  |  |  |  .---- day of week (0 - 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat
# |  |  |  |  |
# *  *  *  *  * user-name command to be executed

```

也可以通过`crontab -u root -e`进行配置。配置的格式和通过`/etc/crontab`的配置格式一致。

```shell
[root@localhost ~]# crontab -u root -e
no crontab for root - using an empty one
crontab: installing new cronta
[root@localhost ~]# crontab -u root -l
*/1 * * * * ls /root >> /root/bbb
```

通过`crontab -u root -r`可以将root用户下的定时任务取消。

```shell
[root@localhost ~]# crontab -u root -r
[root@localhost ~]# crontab -u root -l
no crontab for root
```



## 二、进程排查

#### 2.1 查看网络信息

通过`netstat -pantu`命令，来查看当前系统的网络信息，包括：本地开放的端口，远程的连接地址，连接状态，已经进程IP地址。一般，通过连接数，连接状态和进程名称，初步排查出来可以的进程。

![netstat](https://gitee.com/drakebutterfly/ops-lab/raw/master/linux/intrusion-check/images/netstat.PNG)



#### 2.2 查看进程信息

通过`ps -ef`命令，可以查看到本地的服务进程，以及进程的启动命令。如果是通过脚本启动的服务，能够借此查出服务的所在路径。

![ps](https://gitee.com/drakebutterfly/ops-lab/raw/master/linux/intrusion-check/images/ps.PNG)

也可以通过`lsof -p PID`命令来，查看进程所打开的文件。

![lsof](https://gitee.com/drakebutterfly/ops-lab/raw/master/linux/intrusion-check/images/lsof.PNG)



#### 2.3 查看进程资源消耗

通过`top`命令查看进程相关资源的消耗情况。`P`根据CPU使用百分比排序，`M`根据内存使用大小排序。

![top](https://gitee.com/drakebutterfly/ops-lab/raw/master/linux/intrusion-check/images/top.PNG)

可以排查出挖矿进程，同时也能查看内存消耗比较高的进程。



## 三、日志排查

Linux系统的日志一般存放在目录`/var/log`下，如整体系统信息、定时任务的日志和登录认证的日志等。

#### 3.1 定时日志排查

`/var/log/cron`：记录与定时任务相关的日志信息

能够查看哪些用户制定并运行了哪些定时任务，已经定时任务的运行命令。

```shell
[root@localhost ~]# cat /var/log/cron
Aug 24 06:18:02 localhost run-parts(/etc/cron.daily)[1778]: finished logrotate
Aug 24 06:18:02 localhost run-parts(/etc/cron.daily)[1759]: starting makewhatis.cron
Aug 24 06:18:04 localhost run-parts(/etc/cron.daily)[1933]: finished makewhatis.cron
Aug 24 06:18:04 localhost run-parts(/etc/cron.daily)[1759]: starting mlocate.cron
Aug 24 06:18:09 localhost run-parts(/etc/cron.daily)[1944]: finished mlocate.cron
Aug 24 06:18:09 localhost run-parts(/etc/cron.daily)[1759]: starting prelink
Aug 24 06:18:18 localhost run-parts(/etc/cron.daily)[1967]: finished prelink
Aug 24 06:18:18 localhost run-parts(/etc/cron.daily)[1759]: starting readahead.cron
Aug 24 06:18:18 localhost run-parts(/etc/cron.daily)[1979]: finished readahead.cron
Aug 24 06:18:18 localhost run-parts(/etc/cron.daily)[1759]: starting tmpwatch
Aug 24 06:18:19 localhost run-parts(/etc/cron.daily)[2017]: finished tmpwatch
Aug 24 06:18:19 localhost anacron[1711]: Job `cron.daily' terminated
Aug 24 06:20:01 localhost CROND[2022]: (root) CMD (/usr/lib64/sa/sa1 1 1)
Aug 24 06:30:01 localhost CROND[2041]: (root) CMD (/usr/lib64/sa/sa1 1 1)
```



#### 3.2 登录日志排查

`/var/log/secure`：可以查看验证和授权方面的信息。如sshd会将所有信息，包括登录成功失败信息。

借助`grep`，`uniq`，`sort`和`awk`等文本文件分析命令，能够帮助我们快分析。

```shell
[root@localhost ~]# cat /var/log/secure | grep "Accepted\|Failed"
Aug 24 07:32:59 localhost sshd[2033]: Failed password for root from 192.168.190.1 port 53615 ssh2
Aug 24 07:33:09 localhost sshd[2033]: Accepted password for root from 192.168.190.1 port 53615 ssh2
Aug 24 07:43:02 localhost sshd[1565]: Failed password for root from 192.168.190.1 port 53705 ssh2
Aug 24 07:43:11 localhost sshd[1565]: Accepted password for root from 192.168.190.1 port 53705 ssh2
[root@localhost ~]# awk -F" " '$6=="Accepted"{print $0}' /var/log/secure
Aug 24 07:33:09 localhost sshd[2033]: Accepted password for root from 192.168.190.1 port 53615 ssh2
Aug 24 07:43:11 localhost sshd[1565]: Accepted password for root from 192.168.190.1 port 53705 ssh2
```



## 四、文件痕迹排查

我们可以通过`stat`和`ls`命令来查看文件的属性，`ls`一般用来查看文件/目录的大小、权限等信息。我们在排查的使用，通过`stat`命令，能够从时间方面查看到更多的信息。

```shell
[root@localhost ~]# stat abc.sh 
  File: "abc.sh"
  Size: 37        	Blocks: 8          IO Block: 4096   普通文件
Device: 805h/2053d	Inode: 131971      Links: 1
Access: (0755/-rwxr-xr-x)  Uid: (    0/    root)   Gid: (    0/    root)
Access: 2017-09-19 07:13:31.343997045 +0800
Modify: 2017-09-19 07:13:31.343997045 +0800
Change: 2017-09-19 07:13:31.347996888 +0800
```

可以发现，文件有三个不同的是时间，分别是atime、ctime和mtime。不同的操作，这三个时间会有不同的更新情况。

- 读取文件时，access改变，modify和change不会变。如：more，cat，tail等命令。
- 修改文件时，access，modify和change都会改变。如：vi和vim命令。
- 修改属性时，access和modify不会变，change会变。如：chown和chmod命令。

当我们通过一些前提分析，如登录日志排查，大概定位出了攻击者的攻击时间段，我们就可以通过`find`命令来查看这个时间段变化的文件，排查出可以文件以及受影响文件。

```shell
[root@localhost ~]# find / -type f -newermt '2021-08-05 00:00:00' -a -not -newermt '2021-08-24 00:00:00'
/home/ddkdu/.bash_history
/var/lib/prelink/full
/var/cache/yum/x86_64/6/timedhosts.txt
/var/log/messages-20210814
/var/log/btmp-20210814
/var/log/cron-20210815
/var/log/maillog-20210814
/var/log/secure-20210815
# 查看atime
[root@localhost ~]# find / -type f -newerat '2021-08-05 00:00:00' -a -not -newerat '2021-08-24 00:00:00'
# 查看ctime
[root@localhost ~]# find / -type f -newerct '2021-08-05 00:00:00' -a -not -newerct '2021-08-24 00:00:00'
```

如果通过时间排查，出来发现有命令文件有变动，那么很有可能这个命令被替换伪造了，可以找一台相同版本的Linux系统，通过命令`md5sum filename`对比MD5值。



## 五、总结

1）系统用户排查时，主要去查看是否存在UID或GID为0的非root用户，并观察此可疑用户的登录行为。

2）系统启动项排查时，一般都是通过启动项配置排查。

3）定时任务排查时，通过日志来进行定位排查。

4）网络和进程的排查，能够查找到攻击程序，最终到攻击来源。

5）通过时间来排查文件痕迹，可以说是一个绝杀手段，直接从上帝视角排查出可以文件以及影响，前提是要通过上面的排查，排查出攻击时间范围。否则范围过大，很难定位。



## 六、参考

[1] [《网络安全应急响应技术实战指南》]()

[2] [Linux开机启动chkconfig命令详解(让MySQL、Apache开机启动)](https://developer.aliyun.com/article/537443)