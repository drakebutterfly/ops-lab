<h2 align="center">Shell编程</h2>

### 1 shell概述

shell分类

通过/etc/shells文件来查询Linux支持的shell



### 2 echo命令

```shell
[root@localhost ~]# echo [选项] [输出内容]
选项：
	-e:		支持反斜线控制的字符转换
	-n:		取消输出后行末的换行符号
```

使用`-e`选项，可以支持控制字符的输出，常见的控制字符有：

| 控制字符 | 作用                               |
| -------- | ---------------------------------- |
| \\\      | 输出\\本身                         |
| \a       | 输出警告音                         |
| \b       | 退格键，也就是向左删除键           |
| \c       | 取消输出行末的换行符。和-n选项一致 |
| \e       | ESC键                              |
| \f       | 换页符                             |
| \n       | 换行符                             |
| \r       | 回车符                             |
| \t       | 水平制表符，也即是Tab键            |
| \v       | 垂直制表符                         |
| \0nnn    | 按照八进制ASCII码表输出字符。      |
| \xhh     | 按照十六进制ASCII码表输出字符。    |



### 3 脚本执行方法

Shell脚本需要在第一行添加`#!/bin/bash`。Linux脚本执行主要有两种方法。

（1）赋予执行权限，直接执行

```shell
[root@localhost ~]# chmod 755 hello.sh
[root@localhost ~]# ./hello.sh
```

（2）通过Bash调用执行脚本

```shell
[root@localhost ~]# bash hello.sh
```



### 4 Bash基本功能

#### 4.1 历史命令

```shell
[root@localhost ~]# history [选项] [历史命令保存文件]
选项：
	-c:		清空历史命令
	-w:		把缓存中的历史命令写入历史命令保存文件。如果不手工指定历史命令保存文件，则             放入默认历史命令保存文件~/.bash_history中

# 历史命令相关的配置
[root@localhost ~]# cat /etc/profile
...
HISTSIZE=1000
...
```

我们使用history命令查看的历史命令和`~/.bash_history`文件中保存的历史命令不同。用户登录后操作的命令，是保存在缓存中的，需要等用户注销登录后，才会自动写入`~/.bash_history`文件。可以通过`-w`选项，手动将缓存写入文件。

```shell
[root@localhost ~]# history -w
```

如果需要清空历史命令，一般只在设置密码后清除，可以执行如下命令：

```sh
[root@localhost ~]# history -c
```



#### 4.2 输入输出重定向

（1）标准输入输出

| 设备   | 设备文件名  | 文件描述符 | 类型         |
| ------ | ----------- | ---------- | ------------ |
| 键盘   | /dev/stdin  | 0          | 标准输入     |
| 显示器 | /dev/stdout | 1          | 标准输出     |
| 显示器 | /dev/stderr | 2          | 标准错误输出 |

（2）输出重定向

| 类型                       | 符号                   | 作用                                                         |
| -------------------------- | ---------------------- | ------------------------------------------------------------ |
| 标准输出重定向             | 命令 > 文件            | 以覆盖的方式，把命令的正确输出输出到指定的文件或者设备当中。 |
| 标准输出重定向             | 命令 >> 文件           | 以追加的方式，把命令的正确输出输出到指定的文件或者设备当中。 |
| 标准错误输出重定向         | 命令 2>文件            | 以覆盖的方式，把命令的错误输出输出到指定的文件或者设备当中。 |
| 标准错误输出重定向         | 命令 2>>文件           | 以追加的方式，把命令的错误输出输出到指定的文件或者设备当中。 |
| 正确输出和错误输出同时保存 | 命令 > 文件 2>&1       | 以覆盖的方式，把正确输出和错误输出都保存在同一个文件当中。   |
| 正确输出和错误输出同时保存 | 命令 >> 文件 2>&1      | 以追加的方式，把正确输出和错误输出都保存在同一个文件当中。   |
| 正确输出和错误输出同时保存 | 命令 >> 文件1 2>>文件2 | 把正确输出追加到文件1中，把错误输出追加到文件2中。           |

> 注意：正确的输出，重定向符和文件之间加空格；错误输出，文件描述符、重定向符和文件三者之间没有空格。



#### 4.3 多命令顺序执行

| 多命令执行符 | 格式             | 作用                                        |
| ------------ | ---------------- | ------------------------------------------- |
| ;            | 命令1 ; 命令2    | 多命令顺序执行，命令之间没有任何逻辑联系    |
| &&           | 命令1 && 命令2   | 当命令1正确执行（$?==0），则命令2才会执行   |
| \|\|         | 命令1 \|\| 命令2 | 当命令1执行不正确（$?!=0），则命令2才会执行 |



### 5 特殊符号

| 符号 | 作用                                                         |
| ---- | ------------------------------------------------------------ |
| ''   | 单引号。在单引号中所有的特殊符号，如“$”和“`”（反引号）都没有特殊含义。 |
| ""   | 双引号。在双引号中特殊符号都没有特殊含义，但是“$”、“`”和“\”是例外，拥有“调用变量的值”、“引用命令”和“转义符”的特殊含义。 |
| “``” | 反引号。反引号括起来的内容是系统命令，在Bash中会先执行它，和`$()`作用意义，不过推荐使用`$()`，因为反引号非常容易看错。 |
| $()  | 和反引号作用一样，用来引用系统命令。                         |
| ()   | 用于一串命令执行时，()中的命令会在子Shell中运行              |
| {}   | 用于一串命令执行时，{}中的命令会在当前Shell中执行，最后一个命令后面需要加;号。也可以用于变量变形与替换。 |
| []   | 用于变量的测试。                                             |
| #    | 在Shell脚本中，#开头的行代表注释。                           |
| $    | 用于调用变量的值，如需要调用变量name的值时，需要用$name的方式得到变量的值。 |
| \    | 转义符。跟在\之后的特殊符号将失去特殊含义，变为普通字符。如\\$将输出`$`符号，而不当做是变量引用。 |



### 6 变量和运算符

#### 6.1 变量规则

在定义变量时，需要遵守一些规则：

- 变量名称可以由字母、数字和下划线组成，但是不能以数字开头。
- 变量的默认类型都是字符串，如果要进行数值运算，则必须指定变量类型位数值型
- 变量用等号连接值，等号左右两侧不能有空格
- 变量的值如果有空格，需要使用单引号或双引号包括。
- 变量的值中，可以使用`\`转义符
- 变量的值是可以拼接，如`name=${name}_new`
- 如果是把命令的结果作为变量值赋予变量，则需要使用反引号或者`$()`包含命令
- 环境变量建议大写，便于区分



#### 6.2 变量分类

按应用范围分类：

- 用户自定义变量：最常见变量，一般用于Shell编程，由用户自由定义。一般建议**小写**。
- 环境变量：保存的是和系统操作环境相关的数据，比如当前登录用户，用户的家目录等。一般对系统起作用的环境变量的变量名是系统预先设定好的，不允许修改变量名，只能修改值。一般建议**大写**。
- 位置参数变量：主要用来向脚本传递参数或数据，名称和作用固定，值可以修改。
- 预定义变量：是Bash中定义好的变量，名称和作用固定，值可以修改。

##### 6.2.1 用户自定义变量

只在定义的shell中起作用，在子shell中是不起作用的。

```shell
# 1 变量定义
[root@localhost ~]# name=kla
[root@localhost ~]# echo ${name}
kla
[root@localhost ~]# comment="hello world"
[root@localhost ~]# echo $comment
hello world

# 2 如果不加选项，set的作用是查询所有变量
[root@localhost ~]# set [选项]
选项：
	-u		如果设定此项，调用未声明变量时会报错
	-x      如果设定此项，在命令执行之前，会把命令先输出一次
# 2.1 Example
[root@localhost ~]# echo $age

[root@localhost ~]# set -u
[root@localhost ~]# echo $age
-bash: age: unbound variable

# 3 删除变量
[root@localhost ~]# unset name
```



##### 6.2.2 环境变量

父shell和子shell都有效。

```shell
# 1 增加环境变量
[root@localhost ~]# export age=18

# 2 删除环境变量
[root@localhost ~]# unset age

# 3 查看环境变量
[root@localhost ~]# env | grep age
```

系统有一些常用的环境变量，我们是需要了解其用途的。

（1）PATH变量：系统查找命令的路径

```shell
[root@localhost ~]# echo $PATH
/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/root/bin

# 可以通过变量拼接添加新的路径
[root@localhost ~]# PATH="$PATH":/root/sh
```

变量的值是用`:`分割的路径，这些路径就是系统查找命令的路径。这些路径下的命令，在执行的时候只需要敲对应的文件名称即可，并不需要全路径的文件名称。



（2）PS1变量：命令提示符设置

```shell
[root@localhost ~]# echo $PS1
[\u@\h \W]\$
```



（3）LANG变量：语系

```shell
[root@localhost ~]# echo $LANG
en_US.UTF-8

# 查询Linux系统支持多少种语系
[root@localhost ~]# localectl list-locales | wc -l
785

# 查询当前系统使用的语系
[root@localhost ~]# locale
LANG=en_US.UTF-8
LC_CTYPE="en_US.UTF-8"
LC_NUMERIC="en_US.UTF-8"
LC_TIME="en_US.UTF-8"
LC_COLLATE="en_US.UTF-8"
LC_MONETARY="en_US.UTF-8"
LC_MESSAGES="en_US.UTF-8"
LC_PAPER="en_US.UTF-8"
LC_NAME="en_US.UTF-8"
LC_ADDRESS="en_US.UTF-8"
LC_TELEPHONE="en_US.UTF-8"
LC_MEASUREMENT="en_US.UTF-8"
LC_IDENTIFICATION="en_US.UTF-8"
LC_ALL=
```



##### 6.2.3 位置参数变量

| 位置参数变量 | 作用                                                         |
| ------------ | ------------------------------------------------------------ |
| $n           | n为数字，`$0`代表命令本身，`$1-$9`代表第一到第九个参数，十以上的参数需要用大括号包含，如`${10}`。 |
| $*           | 代表命令行中所有的参数，把所有参数看成一个整体               |
| $@           | 代表命令行中所有的参数，把每个参数区分对待                   |
| $#           | 代码命令行中所有参数的个数                                   |

```shell
[root@localhost ~]# cat test.sh
#!/bin/bash

a=$1
b=$2

sum=$(( $a + $b ))

echo "\$sum is $sum"

echo "\$* is $*"
echo "\$@ is $@"
echo "\$# is $#"

echo "==============\$@================="
for i in "$@"
   do
        echo "$i"
   done

echo "==============\$*================="
for i in "$*"
   do
        echo "$i"
   done

[root@localhost ~]# ./test.sh 22 33 44
$sum is 55
$* is 22 33 44
$@ is 22 33 44
$# is 3
==============$@=================
22
33
44
==============$*=================
22 33 44
```



##### 6.2.4 预定义变量

| 预定义变量 | 作用                                                         |
| ---------- | ------------------------------------------------------------ |
| $?         | 最后一次执行的命令的返回状态。如果这个值为0，证明上一个命令正确执行；如果这个变量的值非0，则证明上一个命令执行不正确。 |
| $$         | 当前进程的PID                                                |
| $!         | 后台运行的最后一个进程的PID                                  |

```shell
[root@localhost ~]# echo $?
0
[root@localhost ~]# lssd
-bash: lssd: command not found
[root@localhost ~]# echo $?
127
```



### 7 read接收键盘输入

```shell
[root@localhost ~]# read [选项] [变量名]
选项：
	-p "提示信息"		在等待read输入时，输出提示信息
	-t 秒数            read命令会一直等待用户输入，使用此选项可以指定等待时间
	-n 字符数          read命令只接受指定的字符数，就会执行
	-s                隐藏输入的数据，适用于机密信息的输入
变量名：
	变量名可以自定义，如果不指定变量名，会把输入保存入默认变量REPLY
	如果只提供一个变量名，则整个输入行赋予该变量
	如果提供了一个以上的变量名，则输入行分为若干字，一个接一个地赋予各个变量，而命令行上
	的最后一个变量取得剩余的所有字
```

脚本样例：

```shell
[root@localhost test]# cat count.sh
#!/bin/bash

read -t 30 -p "please input num1: " num1
read -t 30 -p "please input num2: " num2

sum=$(( $num1 + $num2 ))

echo $sum
[root@localhost test]# ./count.sh
please input num1: 22
please input num2: 33
55
```



### 8 declare声明变量类型

shell中所有变量的默认类型时字符串，如果我们把变量声明为整数型，需要使用declare来声明。

```shell
[root@localhost ~]# declare [+/-] [选项] 变量名
选项：
	-		给变量设定类型属性
	+		取消变量的类型属性
	-a		将变量声明为数组型
	-i		将变量声明为整数型
	-r		将变量声明为只读变量。一旦设置，无法取消和修改
	-x		将变量声明为环境变量
	-p		显示指定变量的被声明的类型
```

例子1：数值运算

```shell
[root@localhost ~]# a=1 ; b=2 ; c=$a+$b ; echo $c
1+2

# Example1
[root@localhost ~]# declare -i c=$a+$b ; echo $c
3

# Example2
[root@localhost ~]# d=$(expr $a + $b) # 注意运算符+左右一定要有空格
[root@localhost ~]# echo $d
3

# Example3
[root@localhost ~]# let e=$a+$b 
[root@localhost ~]# echo $e
3

# Example4,推荐此用法
[root@localhost ~]# f=$(($a+$b))  # 括号中加号两边的空格不严格要求
[root@localhost ~]# echo $f
3
```

例子2：数组变量类型

```shell
[root@localhost ~]# name[0]="n0" ; name[1]="n1"
[root@localhost ~]# echo ${name[*]}
n0 n1
```

例子3：环境变量

```shell
[root@localhost ~]# declare -x test=123
[root@localhost ~]# declare -p          # 查询所有变量
....
declare -x test=123
```

例子4：只读属性

```shell
[root@localhost ~]# declare -r test=123
```

这个变量赋予只读属性后，无法通过+r取消，也无法unset，但是只要重新登录或者重启，这个变量就会消失。



### 9 变量的测试与内容置换

| 变量置换方式 | 变量y没有设置 | 变量y为空值 | 变量y设置值 |
| ------------ | ------------- | ----------- | ----------- |
| x=${y-新值}  | x=新值        | x为空       | x=$y        |

主要就是用来让程序能够判断变量是否有赋值。



### 10 环境变量配置文件

#### 10.1 source命令

```shell
[root@localhost ~]# source 配置文件
或
[root@localhost ~]# . 配置文件
```

修改配置文件后，可以通过以上两种方式，让修改立即生效。



#### 10.2 环境变量配置文件

在Linux系统登录时主要生效的环境变量配置文件有以下五个：

- /etc/profile
- /etc/profile.d/*.sh
- /etc/bashrc
- ~/.bash_profile
- ~/.bashrc

环境变量配置文件调用过程

![env_path](D:\docs\developer\ops-lab\linux\env_path.png)

（1）用户登录过程，环境变量的加载时通过虚线路径进行加载

（2）su切换用户或者子bash常见，环境变量的加载通过实线路径进行加载



### 11 正则表达式

在Shell中，用来在文件当中搜索字符串的命令，如grep、awk、sed等命令可以支持正则表达式。

#### 11.1 基础正则表达式

| 元字符    | 作用                                                         |
| --------- | ------------------------------------------------------------ |
| *         | 前一个字符匹配0次或任意多次                                  |
| .         | 匹配除了换行符外任意一个字符                                 |
| ^         | 匹配行首。例如：^hello会匹配以hello开头的行。                |
| $         | 匹配行尾。例如：hello&会匹配以hello结尾的行。                |
| []        | 匹配括号中指定的任意一个字符，只匹配一个字符。例如：[aoeiu]匹配任意一个元音字母，[0-9]匹配任意一位数字。 |
| [^]       | 匹配除中括号的字符以外的任意一个字符。例如：`[^0-9]`匹配任意一位非数字字符。 |
| \         | 转义符。用于取消将特殊符号的含义取消。                       |
| `\{n\}`   | 表示其前面的字符恰好出现n次。                                |
| `\{n,\}`  | 表示前面的字符出现至少n次。                                  |
| `\{n,m\}` | 便是前面的字符至少出现n次，最多出现m次。                     |

```shell
```



### 12 字符处理

Linux有十分强大的字符处理指令，帮助我们在命令行界面快速检索并获取到我们想要的信息。

#### 12.1 字符截取cut命令

```shell
[root@localhost ~]# cut [选项] 文件名
选项：
	-f 列号		提取第几列
	-d 分割符	   按照指定分割符分割列
	-c 字符范围   通过字符范围（行首为0）来进行字符提取。“n-”表示从第n个字符到行尾；“n-m”表示从第n个字符到第m个字符；“-m”表示从第一个字符到第m个字符。
```

cut命令的默认分隔符是制表符，也就是`tab`键，对空格符支持不怎么好。

```shell
[root@localhost ~]# ll test | cut -d " " -f 1
total
-rw-r--r--.
-rwxr-xr-x.
-rwxr-x---.
-rwxr-x---.
drwxr-x---.
-rw-r--r--.
-rw-r-----.

# 截取普通用户及其ID
[root@localhost ~]# grep "/bin/bash" /etc/passwd | grep -v "root"
user1:x:1000:1000::/home/user1:/bin/bash
user2:x:1001:1001::/home/user2:/bin/bash
[root@localhost ~]# grep "/bin/bash" /etc/passwd | grep -v "root" | cut -d ":" -f 1,3
user1:1000
user2:1001
```



#### 12.2 字符截取awk命令

##### 12.2.1 基本使用

```shell
[root@localhost ~]# awk 'patten1 {action1} patten2 {action2}...' file
Patten:
	一般使用关系表达式作为条件。如:
	x > 10	判断变量x是否大于10
	x == y  判断变量x是否等于变量y
	A ~ B   判断字符串A中是否包含能匹配B表达式的子字符串
Action:
	格式化输出
	流程控制语句

# 实验用例
[root@localhost ~]# cat awk-ls.txt
-rw-------. 1 root root  1540 Oct  1  2021 anaconda-ks.cfg
-rw-r--r--. 1 root root     0 Aug 21 11:38 awk-ls.txt
-rwxrwxr-x. 1 root root  1202 Sep 28  2021 docker-entrypoint.sh
drwxr-xr-x. 3 root root   205 Jul 15 22:55 hello
-rw-r--r--. 1 root root    26 Jul  9 11:29 HELLO.gz
-rw-------. 1 root root 24064 Oct  2  2021 hello-o.tar
-rw-r--r--. 1 root root 24064 Oct  2  2021 hello.tar
-rw-r--r--. 1 root root  3536 Oct  2  2021 hello.tar.gz
drwxr-xr-x. 2 root root    52 Jul  9 18:22 hello-world
-rw-r--r--. 1 root root   162 Jul 12 22:15 hello.zip
drwxr-xr-x. 3 root root   181 Jul 31 23:13 test
-rw-r--r--. 1 root root   310 Jul 12 22:21 test.zip
```

##### 12.2.2 awk条件详解

条件(Patten)

| 条件类型   | 条件   | 说明                                                         |
| ---------- | ------ | ------------------------------------------------------------ |
| awk保留字  | BEGIN  | 在awk程序一开始时，尚未读取任何数据之前执行。BEGIN后的动作只在程序开始时执行一次。 |
| awk保留字  | END    | 在awk程序处理完所有数据，即将结束时执行。END后的动作只在程序结束时执行一次。 |
| 关系运算符 | >      | 大于                                                         |
| 关系运算符 | <      | 小于                                                         |
| 关系运算符 | >=     | 大于等于                                                     |
| 关系运算符 | <=     | 小于等于                                                     |
| 关系运算符 | ==     | 等于                                                         |
| 关系运算符 | !=     | 不等于                                                       |
| 关系运算符 | A~B    | 判断字符串A中是否包含能匹配B表达式的子字符串                 |
| 关系运算符 | A!~B   | 判断字符串A中是否不包含能匹配B表达式的子字符串               |
| 正则表达式 | /正则/ | 如果在“//”中可以写入字符，也可以支持正则表达式               |

（2）awk条件样例

```shell
# BEGIN和END
[root@localhost ~]# awk 'BEGIN{printf} END{print 222} {printf $2 "\t" $6 "\n"}' awk-ls.txt
111     Oct
1       Aug
1       Sep
3       Jul
1       Jul
1       Oct
1       Oct
1       Oct
2       Jul
1       Jul
3       Jul
1       Jul
222

# 关系运算符
[root@localhost ~]# awk '$2 >= 2 {printf $2 "\t" $6 "\n"}' awk-ls.txt
3       Jul
2       Jul
3       Jul

# 关系运算符与字符串
[root@localhost ~]# awk '$6 ~ /Jul/ {printf $2 "\t" $6 "\n"}' awk-ls.txt
3       Jul
1       Jul
2       Jul
1       Jul
3       Jul
1       Jul

# 正则表达式
[root@localhost ~]# awk '/hello/  {printf $2 "\t" $6 "\t" $9 "\n"}' awk-ls.txt
3       Jul     hello
1       Oct     hello-o.tar
1       Oct     hello.tar
1       Oct     hello.tar.gz
2       Jul     hello-world
1       Jul     hello.zip
```



##### 12.2.3 格式化输出

上面`cut`命令，发现对空白字符分割截取不太友好，`awk`命令默认支持的是空白字符（连续多个和一个等同）分割。

```shell
[root@localhost ~]# awk '{printf $2 "\t" $6 "\n"}' awk-ls.txt
1       Oct
1       Aug
1       Sep
3       Jul
1       Jul
1       Oct
1       Oct
1       Oct
2       Jul
1       Jul
3       Jul
1       Jul
```



##### 12.2.4 awk常用内置变量

| 内置变量 | 作用                                          |
| -------- | --------------------------------------------- |
| $0       | 代表目前awk所读入的整行数据                   |
| $n       | 代表目前读入行的第n个字段                     |
| NF       | 当前行拥有的字段（列）总数                    |
| NR       | 当前awk所处理的行，是总数据的第几行           |
| FS       | 用户自定义分割符。awk的默认分割符是任何空格。 |

使用样例：

```shell
# 可以使用FS自定义分割符
[root@localhost ~]# cat /etc/passwd | grep "/bin/bash" | awk 'BEGIN{FS=":"}  {print $1}'
root
user1
user2

# 使用-F选项自定义分割符更加简单，且常用
[root@localhost ~]# cat /etc/passwd | grep "/bin/bash" | awk -F ":" '{print $1}'
root
user1
user2

# NF与NR
[root@localhost ~]# cat /etc/passwd | grep "/bin/bash" | awk 'BEGIN{FS=":"}  {print $1 "\t" $3 "\t row:" NR "\t colume:" NF}'
root    0        row:1   colume:7
user1   1000     row:2   colume:7
user2   1001     row:3   colume:7
```



#### 12.3 sed命令

用来将数据进行选取、替换、删除、新增的命令。

```shell
[root@localhost ~]# sed [选项] '动作' 文件名
选项：
	-n		一般sed命令会把所有数据输出到屏幕，如果加入此选择，则只会把经过sed命令处             理的行输出到屏幕。
	-e		允许对输入数据应用多条sed命令编辑。
	-i		用sed的修改结果直接修改读取数据的文件，而不是由屏幕输出。
动作：
	a \		追加，在当前行后添加一行或多行。添加多行时，除最后一行外，每行末尾需要               用\代表数据未完结。
	c \		行替换，用c后面的字符串替换原数据行。
	i \		插入，在当前行前插入一行或多行。
	d		删除，删除指定的行。
	p		打印，输出指定的行。
	s		字符串替换，用一个字符串替换另外一个字符串。格式为“行范围s/旧字串/新字               串/g”

# 实验用例
[root@localhost ~]# cat sed-df.txt
Filesystem           Size  Used Avail Use% Mounted on
/dev/mapper/cl-root   10G  1.8G  8.3G  18% /
/dev/sda3            976M   52M  858M   6% /opt/sda3
/dev/sda1            197M  117M   81M  60% /boot
```

对于sed命令，需要注意sed所做的修改并不会直接改变文件的内容，而是把修改结果只显示到屏幕上，除非使用`-i`选项才会直接修改文件。

##### 12.3.1 打印指定行

```shell
# 打印第2行
[root@localhost ~]# sed -n '2p' sed-df.txt
/dev/mapper/cl-root   10G  1.8G  8.3G  18% /
# 打印第2到第4行
[root@localhost ~]# sed -n '2,4p' sed-df.txt
/dev/mapper/cl-root   10G  1.8G  8.3G  18% /
/dev/sda3            976M   52M  858M   6% /opt/sda3
/dev/sda1            197M  117M   81M  60% /boot
```



##### 12.3.2 删除指定行

```shell
# 将删除第2行后的内容输出到屏幕
[root@localhost ~]# sed '2d' sed-df.txt
Filesystem           Size  Used Avail Use% Mounted on
/dev/sda3            976M   52M  858M   6% /opt/sda3
/dev/sda1            197M  117M   81M  60% /boot
[root@localhost ~]# cat sed-df.txt
Filesystem           Size  Used Avail Use% Mounted on
/dev/mapper/cl-root   10G  1.8G  8.3G  18% /
/dev/sda3            976M   52M  858M   6% /opt/sda3
/dev/sda1            197M  117M   81M  60% /boot

# 将数据文件中的第2行直接删除
[root@localhost ~]# sed -i '2d' sed-df.txt
[root@localhost ~]# cat sed-df.txt
Filesystem           Size  Used Avail Use% Mounted on
/dev/sda3            976M   52M  858M   6% /opt/sda3
/dev/sda1            197M  117M   81M  60% /boot

# 同时删除第2和第4行
[root@localhost ~]# cat sed-df.txt
Filesystem           Size  Used Avail Use% Mounted on
hello world
/dev/sda3            976M   52M  858M   6% /opt/sda3
222222
/dev/sda1            197M  117M   81M  60% /boot
[root@localhost ~]# sed -i -e '2d ; 4d' sed-df.txt
[root@localhost ~]# cat sed-df.txt
Filesystem           Size  Used Avail Use% Mounted on
/dev/sda3            976M   52M  858M   6% /opt/sda3
/dev/sda1            197M  117M   81M  60% /boot
```



##### 12.3.3 追加到指定行后面

```shell
# 在第一行后面追加一行，内容为1111111，输出到屏幕
[root@localhost ~]# sed '1a 1111111' sed-df.txt
Filesystem           Size  Used Avail Use% Mounted on
1111111
/dev/sda3            976M   52M  858M   6% /opt/sda3
/dev/sda1            197M  117M   81M  60% /boot
[root@localhost ~]# cat sed-df.txt
Filesystem           Size  Used Avail Use% Mounted on
/dev/sda3            976M   52M  858M   6% /opt/sda3
/dev/sda1            197M  117M   81M  60% /boot

# 在第一行后面追加一行，内容为1111111，并写入到数据文件中
[root@localhost ~]# sed -i '1a 1111111' sed-df.txt
[root@localhost ~]# cat sed-df.txt
Filesystem           Size  Used Avail Use% Mounted on
1111111
/dev/sda3            976M   52M  858M   6% /opt/sda3
/dev/sda1            197M  117M   81M  60% /boot
```



##### 12.3.4 插入到指定行前面

```shell
# 在第4行前面插入一行，内容为222222，输出到屏幕
[root@localhost ~]# sed '4i 222222' sed-df.txt
Filesystem           Size  Used Avail Use% Mounted on
1111111
/dev/sda3            976M   52M  858M   6% /opt/sda3
222222
/dev/sda1            197M  117M   81M  60% /boot
[root@localhost ~]# cat sed-df.txt
Filesystem           Size  Used Avail Use% Mounted on
1111111
/dev/sda3            976M   52M  858M   6% /opt/sda3
/dev/sda1            197M  117M   81M  60% /boot

# 在第4行前面插入一行，内容为222222，并写入到数据文件中
[root@localhost ~]# sed -i '4i 222222' sed-df.txt
[root@localhost ~]# cat sed-df.txt
Filesystem           Size  Used Avail Use% Mounted on
1111111
/dev/sda3            976M   52M  858M   6% /opt/sda3
222222
/dev/sda1            197M  117M   81M  60% /boot
```



##### 12.3.5 替换指定行

```shell
# 将第2行内容替换为hello world，输出到屏幕
[root@localhost ~]# sed '2c hello world' sed-df.txt
Filesystem           Size  Used Avail Use% Mounted on
hello world
/dev/sda3            976M   52M  858M   6% /opt/sda3
222222
/dev/sda1            197M  117M   81M  60% /boot
[root@localhost ~]# cat sed-df.txt
Filesystem           Size  Used Avail Use% Mounted on
1111111
/dev/sda3            976M   52M  858M   6% /opt/sda3
222222
/dev/sda1            197M  117M   81M  60% /boot

# 将第2行内容替换为hello world，并写入到数据文件中
[root@localhost ~]# sed -i '2c hello world' sed-df.txt
[root@localhost ~]# cat sed-df.txt
Filesystem           Size  Used Avail Use% Mounted on
hello world
/dev/sda3            976M   52M  858M   6% /opt/sda3
222222
/dev/sda1            197M  117M   81M  60% /boot
```



##### 12.3.6 字符串替换

```shell
# 格式
[root@localhost ~]# sed 's/旧字串/新字串/g' 文件名

# 样例一：将第2行的root替换为paas，如果需要将结果写入文件，需要加-i选项
[root@localhost ~]# sed '2s/root/paas/g' sed-df.txt
Filesystem           Size  Used Avail Use% Mounted on
/dev/mapper/cl-paas   10G  1.8G  8.3G  18% /
/dev/sda3            976M   52M  858M   6% /opt/sda3
/dev/sda1            197M  117M   81M  60% /boot

# 样例二：在第4行的开头替换成#，如果需要将结果写入文件，需要加-i选项
[root@localhost ~]# sed '4s/^/#/g' sed-df.txt
Filesystem           Size  Used Avail Use% Mounted on
/dev/mapper/cl-root   10G  1.8G  8.3G  18% /
/dev/sda3            976M   52M  858M   6% /opt/sda3
#/dev/sda1            197M  117M   81M  60% /boot

# 样例三：将所有的字串#删除
[root@localhost ~]# sed 's/#//g' sed-df.txt
Filesystem           Size  Used Avail Use% Mounted on
/dev/mapper/cl-root   10G  1.8G  8.3G  18% /
/dev/sda3            976M   52M  858M   6% /opt/sda3
/dev/sda1            197M  117M   81M  60% /boot
```



#### 12.4 排序命令sort

```shell
[root@localhost ~]# sort [选项] 文件名
选项：
	-f			忽略大小写
	-r			反向排序
	-n			以数值型进行排序，默认使用字符串型排序
	-t			指定分隔符，默认的分割符是制表符
	-k n[,m]	按照指定的字段范围排序。从第n字段开始，m字段结束（默认到行尾）

# 样例一
[root@localhost ~]# sort /etc/passwd
adm:x:3:4:adm:/var/adm:/sbin/nologin
bin:x:1:1:bin:/bin:/sbin/nologin
...

# 样例二：反向排序
[root@localhost ~]# sort -r /etc/passwd
user2:x:1001:1001::/home/user2:/bin/bash
user1:x:1000:1000::/home/user1:/bin/bash
...

# 样例三：指定分割符，并将分割后的第3个字段以数值进行排序
[root@localhost ~]# sort -n -t ":" -k 3,3 /etc/passwd
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
...
```



#### 12.5 取消重复行uniq

```shell
[root@localhost ~]# uniq [选项] 文件名
选项：
	-i		忽略大小写

# Example1
[root@localhost ~]# cat uniq.txt
11111
22222
11111
11111

# Example2
[root@localhost ~]# cat uniq.txt | uniq
11111
22222
11111

# Example3
[root@localhost ~]# sort uniq.txt | uniq
11111
22222
```

需要注意，`uniq`命令取消的只是连续的重复行。



#### 12.6 统计命令wc

```shell
[root@localhost ~]# wc [选项] 文件名
选项：
	-l		只统计行数
	-w		只统计单词数
	-m		只统计字符数

[root@localhost ~]# wc /etc/passwd
  23   44 1100 /etc/passwd
[root@localhost ~]# wc -l /etc/passwd
23 /etc/passwd
[root@localhost ~]# wc -w /etc/passwd
44 /etc/passwd
[root@localhost ~]# wc -m /etc/passwd
1100 /etc/passwd
```



### 13 条件判断

Linux条件判断是脚本条件语句的重要组成部分，文件类型和字符类型等有各自的测试选项。



#### 13.1 按照文件类型判断

常用文件类型的判断如下：

| 测试选项 | 作用                                                   |
| -------- | ------------------------------------------------------ |
| -d  文件 | 判断该文件是否存在，并且是否为目录（是则为真）         |
| -e  文件 | 判断该文件是否存在                                     |
| -f  文件 | 判断该文件是否存在，并且是否为普通文件（是则为真）     |
| -L  文件 | 判断该文件是否存在，并且是否为符号链接文件（是则为真） |
| -s  文件 | 判断该文件是否存在，并且是否为非空（非空则为真）       |

样例：

```shell
[root@localhost ~]# [-d test] && echo yes || echo no
yes
[root@localhost ~]# [-f test] && echo yes || echo no
no
```



#### 13.2 按照文件权限进行判断

文件权限的判断如下：

| 测试选项 | 作用                                                       |
| -------- | ---------------------------------------------------------- |
| -r  文件 | 判断该文件是否存在，并且是否该文件拥有读权限（是则为真）   |
| -w  文件 | 判断该文件是否存在，并且是否该文件拥有写权限（是则为真）   |
| -x  文件 | 判断该文件是否存在，并且是否该文件拥有执行权限（是则为真） |
| -u  文件 | 判断该文件是否存在，并且是否该文件拥有SUID权限（是则为真） |
| -g  文件 | 判断该文件是否存在，并且是否该文件拥有SGID权限（是则为真） |
| -k  文件 | 判断该文件是否存在，并且是否该文件拥有SBit权限（是则为真） |

样例：

```shell
[root@localhost ~]# ls -l
...
-rw-r--r--. 1 root root   310 Jul 12 22:21 test.zip

# 只要所有者权限、所属组权限和其他组权限中有一个有写权限，即为真
[root@localhost ~]# [-w test.zip] && echo yes || echo no
yes
```







参考

[shell基础](https://www.bilibili.com/video/BV1GJ411K7px?p=104&vd_source=02ec6f8cbf71a099316cdc86dad7c2cf)

